// PUKIS' Array Helper
package A

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"sort"
	"strings"
)

type X []interface{}
type MSX []map[string]interface{}

/* Merge 2 A.MSX into return value
func main() {
        a := MSX{map[string]interface{}{`a`:1}}
        b :=MSX{map[string]interface{}{`b`:2,`c`:3}}
        c := a.MergeWith(b)

	fmt.Println(c)
}
*/
func (a MSX) MergeWith(b MSX) MSX {
	for _, v := range b {
		a = append(a, v)
	}
	return a
}

// convert to Gob
func (a MSX) ToGob() []byte {
	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	err := e.Encode(a)
	L.PanicIf(err, `failed A.MSX.ToGob`, a)
	return b.Bytes()
}

// convert from Gob
func (a *MSX) FromGob(by []byte) {
	b := bytes.Buffer{}
	b.Write(by)
	d := gob.NewDecoder(&b)
	err := d.Decode(&a)
	L.PanicIf(err, `failed A.MSX.FromGob`, by)
}

/* Convert map array of string to JSON string type
func main() {
    m:= []interface{}{123,`abc`}
    L.Print(A.ToJson(m)) // output [123,"abc"]
}
*/
func ToJson(arr []interface{}) string {
	str, err := json.Marshal(arr)
	L.PanicIf(err, `Slice.ToJson failed`, arr)
	return string(str)
}

/* Convert 2 map array of string to JSON string type,
   remove any identic string.
func main() {
    m1:= []string{`1234`,`abc`}
    m2:= []string{`abc`,`456`}
    L.Print(A.StrUnion(m1,m2)) // output [1234 abc 456]
}
*/
// Union 2 array of string, yg kembar dihapus
func StrUnion(arr1, arr2 []string) []string {
	unique := M.SB{}
	res := []string{}
	for _, str := range arr1 {
		res = append(res, str)
		unique[str] = true
	}
	for _, str := range arr2 {
		if !unique[str] {
			res = append(res, str)
		}
	}
	return res
}

/* Converting array of integer to map of integer
func main() {
    m:= []int64{123,124}
    L.Print(A.ToIntMap(m)) // output map[123:1 124:1]
}
*/
func ToIntMap(arr []int64) M.II {
	res := M.II{}
	for _, v := range arr {
		res[v] += 1
	}
	return res
}

/* Converting array of string to map of string
func main() {
    m:= []string{`satu`,`dua`}
    L.Print(A.ToStrMap(m))//output map[satu:0 dua:1]
}
*/
// ["a","b","c"] --> kalau json kira2 hasilnya:
// {"a":0,"b":1,"c":2}
func ToStrMap(arr []string) M.SI {
	res := M.SI{}
	for k, v := range arr {
		res[v] = int64(k)
	}
	return res
}

/* Combine strings in the array of string with the chosen string separator
func main() {
    m1:= []string{`satu`,`dua`}
    m2:=(`-`)
    L.Print(A.StrJoin(m1,m2)) // output satu-dua
}
*/
// ["a","b","c"] , "-" -->
// a-b-c
func StrJoin(arr []string, sep string) string {
	return strings.Join(arr, sep)
}

/* Combine integer in the array of int64 with the chosen string separator
func main() {
    m1:= []int64{1,2}
    m2:=(`-`)
    L.Print(A.IntJoin(m1,m2)) // output 1-2
}
*/
// [1,2,3], "-" -->
// 1-2-3
func IntJoin(arr []int64, sep string) string {
	if len(arr) == 0 {
		return ``
	}
	if len(arr) == 1 {
		return I.ToS(arr[0])
	}
	b := bytes.Buffer{}
	b.WriteString(I.ToS(arr[0]))
	for _, v := range arr[1:] {
		b.WriteString(sep)
		b.WriteString(I.ToS(v))
	}
	return b.String()
}

/* Checking whether the array of string contain a spesific string
func main() {
    m1:= []string{`satu`,`dua`}
    L.Print(A.StrContain(m1,`satu`)) // output true
    L.Print(A.StrContain(m1,``))     // output false
}
*/
// the array has needle
func StrContain(arr []string, needle string) bool {
	if len(arr) == 0 {
		return false
	}
	for _, v := range arr {
		if v == needle {
			return true
		}
	}
	return false
}

/* Checking whether the array of int64 contain a spesific int64
func main() {
    m1:= []int64{1,2}
    L.Print(A.IntContain(m1,1)) // output true
    L.Print(A.IntContain(m1,3)) // output false
}
*/
// array has needle
func IntContain(arr []int64, needle int64) bool {
	if len(arr) == 0 {
		return false
	}
	for _, v := range arr {
		if v == needle {
			return true
		}
	}
	return false
}

/* Remove any string in first array that have same value with second array
   Set A minus from set B
func main() {
    m1:= []int64{1,2}
    m2:= []int64{1}
    L.Print(A.IntRemove(m1,m2)) // output [2]

}
*/
// set A minus from set B
func IntRemove(arr_search []int64, val []int64) []int64 {
	res := []int64{}
	for _, v1 := range val {
		for _, v2 := range arr_search {
			if v1 != v2 {
				res = append(res, v2)
			}
		}
	}
	return res
}

/* Convert array of string to array of int64
func main() {
    m:= []string{`1`,`2`}
    L.Print(A.StrToInt(m))//output [1 2]
}
*/
// convert string list to integer list
func StrToInt(arr []string) []int64 {
	res := []int64{}
	for _, v := range arr {
		if v == `` {
			continue
		}
		res = append(res, S.ToI(v))
	}
	return res
}

// remove duplicate int element in array
func IntUniques(elem []int64) []int64 {
	unique_set := make(map[int64]bool, len(elem))
	for _, x := range elem {
		unique_set[x] = true
	}
	result := make([]int64, 0, len(unique_set))
	for x := range unique_set {
		result = append(result, x)
	}
	return result
}

// sorting for array int64 elements
type int64arr []int64

func (a int64arr) Len() int           { return len(a) }
func (a int64arr) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a int64arr) Less(i, j int) bool { return a[i] < a[j] }
func SortIntElem(elem []int64) []int64 {
	sort.Sort(int64arr(elem))
	return elem
}

// get smallest number for int64 array
func SmallestNumberIntArr(elem []int64) int64 {
	var minNumber int64 = elem[0]
	for i := 0; i < len(elem); i++ {
		if elem[i] < minNumber {
			minNumber = elem[i]
		}
	}
	return minNumber
}

// get biggest number for int64 array
func BiggestNumberIntArr(elem []int64) int64 {
	var maxNumber int64 = elem[0]
	for i := 0; i < len(elem); i++ {
		if elem[i] > maxNumber {
			maxNumber = elem[i]
		}
	}
	return maxNumber
}
