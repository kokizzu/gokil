// PUKIS' X-Conversion (interface{}) Helper
package X

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/kokizzu/gokil/K"
	"gitlab.com/kokizzu/gokil/L"
)

/*
func main() {
    var m interface{}
    var m1 interface{}
    var m3 interface{}
    var m4 []interface{}
    m = `123`
    m1 = true
    m3 = []interface{}{1} // tipe array
    m4 = []interface{}{1}
}
*/

/* Convert any data type to int64
func main() {
    var m interface{}
    m = `123`
    L.Describe(X.ToI(m)) // output int64(123)
}
*/
// any to int64
func ToI(any interface{}) int64 {
	if any == nil {
		return 0
	}
	if val, ok := any.(int64); ok {
		return val
	}
	switch v := any.(type) {
	case int:
		return int64(v)
	case uint:
		return int64(v)
	case int8:
		return int64(v)
	case int16:
		return int64(v)
	case int32:
		return int64(v)
	case uint8:
		return int64(v)
	case uint16:
		return int64(v)
	case uint32:
		return int64(v)
	case uint64:
		return int64(v)
	case float32:
		return int64(v)
	case float64:
		return int64(v)
	case bool:
		if v {
			return 1
		}
		return 0
	case string:
		if val, err := strconv.ParseInt(v, 10, 64); err == nil {
			return val
		}
		if val, err := strconv.ParseFloat(v, 64); err == nil {
			return int64(val)
		}
		L.Describe(`Can't convert to int64`, any)
	default:
		L.Describe(`Can't convert to int64`, any)
	}
	return 0
}

/* Convert any data type to float64
func main() {
    var m interface{}
    m = `123`
    L.Describe(X.ToF(m)) // output float64(123)
}
*/
// any to float64
func ToF(any interface{}) float64 {
	if any == nil {
		return 0
	}
	if val, ok := any.(float64); ok {
		return val
	}
	switch v := any.(type) {
	case int:
		return float64(v)
	case int8:
		return float64(v)
	case int16:
		return float64(v)
	case int32:
		return float64(v)
	case int64:
		return float64(v)
	case uint:
		return float64(v)
	case uint8:
		return float64(v)
	case uint16:
		return float64(v)
	case uint32:
		return float64(v)
	case uint64:
		return float64(v)
	case float32:
		return float64(v)
	case bool:
		if v {
			return 1
		}
		return 0
	case string:
		if val, err := strconv.ParseFloat(v, 64); err == nil {
			return val
		}
		L.Describe(`Can't convert to float64`, any)
	default:
		L.Describe(`Can't convert to float64`, any)
	}
	return 0
}

/* Convert any data type to string
func main() {
    var m interface{}
    m = `123`
    L.Describe(X.ToS(m)) // output "123"
}
*/
// any to string
func ToS(any interface{}) string {
	if any == nil {
		return ``
	}
	if val, ok := any.(string); ok {
		return val
	}
	if val, ok := any.([]uint8); ok {
		return string(val)
	}
	switch v := any.(type) {
	case int:
		return strconv.FormatInt(int64(v), 10)
	case int8:
		return strconv.FormatInt(int64(v), 10)
	case int16:
		return strconv.FormatInt(int64(v), 10)
	case int32:
		return strconv.FormatInt(int64(v), 10)
	case int64:
		return strconv.FormatInt(int64(v), 10)
	case uint:
		return strconv.FormatInt(int64(v), 10)
	case uint8:
		return strconv.FormatInt(int64(v), 10)
	case uint16:
		return strconv.FormatInt(int64(v), 10)
	case uint32:
		return strconv.FormatInt(int64(v), 10)
	case uint64:
		return strconv.FormatInt(int64(v), 10)
	case float32:
		return strconv.FormatFloat(float64(v), 'f', -1, 64)
	case float64:
		return strconv.FormatFloat(float64(v), 'f', -1, 64)
	case bool:
		if v {
			return `true`
		}
		return `false`
	case fmt.Stringer:
		return v.String()
	default:
		return K.ToJson5(v)
	}
	return ``
}

// any to time
func ToTime(any interface{}) time.Time {
	if any == nil {
		return time.Time{}
	}
	val, ok := any.(time.Time)
	if L.CheckIf(!ok, `Can't convert to time.Time`, any) {
		return time.Time{}
	}
	return val
}

/* Convert any data type to bool
func main() {
    var m interface{}
    m = `123`
    L.Describe(X.ToBool(m)) // output bool(true)
}
*/
// any to bool
func ToBool(any interface{}) bool {
	if any == nil {
		return false
	}
	if val, ok := any.(bool); ok {
		return val
	}
	switch v := any.(type) {
	case int:
		return v != 0
	case int8:
		return v != 0
	case int16:
		return v != 0
	case int32:
		return v != 0
	case int64:
		return v != 0
	case uint:
		return v != 0
	case uint8:
		return v != 0
	case uint16:
		return v != 0
	case uint32:
		return v != 0
	case uint64:
		return v != 0
	case float32:
		return v != 0
	case float64:
		return v != 0
	case fmt.Stringer:
		val := v.String()
		val = strings.TrimSpace(strings.ToLower(val))
		return !(val == `` || val == `0` || val == `f` || val == `false`)
	case string:
		val := v
		val = strings.TrimSpace(strings.ToLower(val))
		return !(val == `` || val == `0` || val == `f` || val == `false`)
	default:
		L.Describe(`Can't convert to string`, v)
	}
	return false
}

/* Convert any data type to X Arr
func main() {
    var m3 interface{}
    m3 = []interface{}{1}   // tipe array
    L.Describe(X.ToArr(m3)) // output
                            // []interface {}{
                            //    int(1),
                            // }
}
*/
// any to X Arr
func ToArr(any interface{}) []interface{} {
	if any == nil {
		return []interface{}{}
	}
	val, ok := any.([]interface{})
	if L.CheckIf(!ok, `Can't convert to []interface{}`, any) {
		return []interface{}{}
	}
	return val
}

/* Convert array of any data type to array of string
func main() {
    var m4 []interface{}
    m4 = []interface{}{1}         // tipe array
    L.Describe(X.ArrToStrArr(m4)) // output []string{"1"}
}
*/
// []any to []str
func ArrToStrArr(any_arr []interface{}) []string {
	res := []string{}
	for _, val := range any_arr {
		res = append(res, ToS(val))
	}
	return res
}

/* Convert array of any data type to array of int64
func main() {
    var m4 []interface{}
    m4 = []interface{}{1}         // tipe array
    L.Describe(X.ArrToIntArr(m4)) // output []int64{1}
}
*/
// []any to []int64
func ArrToIntArr(any_arr []interface{}) []int64 {
	res := []int64{}
	for _, val := range any_arr {
		res = append(res, ToI(val))
	}
	return res
}
