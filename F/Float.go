// PUKIS' Floating-point (real number) Helper
package F

import (
	"math"
	"strconv"
	"time"
)

/* Return float64 number (second arg), if the condition (first arg) is true,
   Return 0, if the condition (first arg) is false
func main() {
   L.Describe(F.If(true,3.12)) // output float64(3.12)
   L.Describe(F.If(false,3))   // output float64(0)
}
*/
// x = If(a>5,12.3)
func If(b bool, yes float64) float64 {
	if b {
		return yes
	}
	return 0
}

/* Return float64 number (second arg), if the condition (first arg) is true,
   Return float64 number (third arg), if the condition (first arg) is false
func main() {
   L.Describe(F.IfThen(true,3.12,3.45))  // output float64(3.12)
   L.Describe(F.IfThen(false,3.12,3.45)) // output float64(3.45)
}
*/
// x := IfThen(a>5,12.3,4.0)
func IfThen(b bool, yes, no float64) float64 {
	if b {
		return yes
	}
	return no
}

/* Convert float64 to string
func main() {
   L.Describe(F.ToS(3.1284)) // output "3.1284"
}
*/
// float64 to string
func ToS(num float64) string {
	return strconv.FormatFloat(num, 'f', -1, 64)
}

/* Convert float64 to string with 2 digits behind the comma
func main() {
   L.Describe(F.ToStr(3.1284)) // output "3.13"
}
*/
// float64 to string, rounded 2
func ToStr(num float64) string {
	return strconv.FormatFloat(num, 'f', 2, 64)
}

/* Convert float64 to float64 with 2 digits behind the comma
func main() {
   L.Describe(F.To2F(3.1285)) // output float64(3.13)
}
*/
// to 2 number of comma
func To2F(num float64) float64 {
	return math.Floor((num+.005)*100) / 100
}

// format to ISO-8601
func ToIsoDateStr(num float64) string {
	n := int64(num)
	t := time.Unix(n, 0)
	return t.UTC().Format(`2006-01-02T15:04:05Z`)
}
