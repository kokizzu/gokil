package W

import "gitlab.com/kokizzu/gokil/M"

type RequestModel struct {
	Ajax    M.SX
	Actor   int64
	DbActor int64 // to be logged as created_by, updated_by, deleted_by, restored_by
	Level   M.SX
	Posts   *Posts
	Action  string
	Id      int64
	AppId   int64
	Ok      bool
	Ctx     *Context
}

func (rm *RequestModel) IsAjax() bool {
	return rm.Ajax != nil
}

func NewRequestModel_ById_ByDbActor_ByAjax(id, db_actor int64, ajax M.SX) *RequestModel {
	if ajax == nil {
		ajax = M.SX{}
	}
	return &RequestModel{
		Id:      id,
		DbActor: db_actor,
		Ajax:    ajax,
	}
}
