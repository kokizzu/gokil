// PUKIS' WebEngine (mime) Helper
package W

var MIME2EXT map[string]string

func init() {
	MIME2EXT = map[string]string{
		`application/pdf`: `pdf`,
		`image/png`:       `png`,
		`image/jpeg`:      `jpg`,
		`application/zip`: `zip`, // any open document format are also zip
		// supported by youtube
		`video/x-flv`:     `flv`,
		`video/mp4`:       `mp4`,
		`video/quicktime`: `mov`,
		`video/x-msvideo`: `avi`,
		`video/x-ms-wmv`:  `wmv`,
		`video/webm`:      `webm`,
		`video/3gpp`:      `3gp`,
		`video/mp2p`:      `mpegps`,
		`video/mp1s`:      `mpegps`,
	}
}
