// PUKIS' WebEngine (session) Helper
package W

import (
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"github.com/OneOfOne/cmap"
	aero "github.com/aerospike/aerospike-client-go"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/K"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/X"
	"gopkg.in/redis.v5"
	"math/rand"
	"net"
	"net/http"
	"sync/atomic"
	"time"
)

const COOKIE_NAME = `SK_PUIS2`
const COOKIE_SEPARATOR = `|`
const SEC_TO_NANOSEC = 1000000000
const EXPIRE_LEVEL_SEC = 60 * 10
const RKEY_EMAIL = `_email`
const RKEY_LEVEL = `:::AccessLevel`
const RKEY_LASTPAGE = `_last`
const SESSION_PREFIX = `session:`
const GLOBAL_PREFIX = `global::`
const UNIVERSE_PREFIX = `uni::`
const CACHE_PREFIX = `cache::`
const REDIS_UNIVERSE_DB = 15
const REDIS_HOST = `127.0.0.1:6379`

var NO_IP_CHECK bool

var AERO_NAMESPACE string
var AERO_WPS cmap.CMap

const AERO_UNIVERSE_NAMESPACE = `universe`
const AERO_LEVEL_BUCKET = `session_level`
const AERO_LOGIN_BUCKET = `session`
const AERO_SESSION_UNIQUE_BUCKET = `session_unique`
const AERO_LAST_VISIT_BUCKET = `last_visit`
const AERO_CACHE_BUCKET = `user_cache`
const AERO_GLOBAL_BUCKET = `global`
const AERO_UNIVERSE_BUCKET = `universe`
const AERO_ERR_DIGEST = `Failed to create compute digest key `
const AERO_PORT = 3333
const AERO_HOST = `127.0.0.1`

var EXPIRE_NANOSEC, RENEW_NANOSEC time.Duration
var EXPIRE_LOGIN_SEC, RENEW_SEC int64

var redisConnectors *redis.Client
var universeRedisConnectors *redis.Client
var aeroConnectors *aero.Client
var universeAeroConnectors *aero.Client
var globalCount uint64

func init() {
	AERO_WPS = cmap.New()
	rand.Seed(time.Now().UTC().UnixNano())
	globalCount = uint64(time.Now().UTC().UnixNano())
}

// session struct
type Session struct {
	UserId    int64
	AppId     int64
	Key       string
	IpAddr    string
	UserAgent string
	Random    string
	Email     string
	Level     M.SX
}

//////////////////
// REDIS FUNCTIONS

func redisDel(key string) bool {
	err := redisConnectors.Del(key).Err()
	return !L.IsError(err, `failed to DEL`, key)
}

func redisPut(key, val string, ttl int64) bool {
	err := redisConnectors.Set(key, val, time.Second*time.Duration(ttl)).Err()
	return !L.IsError(err, `failed to SET/SETEX`, key, ttl, val)
}

func redisInc(key string) int64 {
	val, err := redisConnectors.Incr(key).Result()
	L.IsError(err, `failed to INCR`, key)
	return val
}

func redisGet(key string) string {
	val, err := redisConnectors.Get(key).Result()
	if err != nil && err.Error() != `redis: nil` {
		L.IsError(err, `failed to GET`, key)
	}
	return val
}

func redisTTL(key string) int64 {
	val := redisConnectors.TTL(key).Val()
	return int64(val.Seconds())
}

//////////////////////
// AEROSPIKE FUNCTIONS

func aeroKey(bucket, key string) *aero.Key {
	askey, err := aero.NewKey(AERO_NAMESPACE, bucket, key)
	if L.IsError(err, AERO_ERR_DIGEST+bucket+` `+key) {
		return nil
	}
	return askey
}

func aeroTTL(bucket, key string) int64 {
	if askey := aeroKey(bucket, key); askey != nil {
		rec, err := aeroConnectors.GetHeader(nil, askey)
		if err == nil && rec != nil {
			return int64(rec.Expiration)
		}
	}
	return 0
}

func aeroGet(bucket, key string) *aero.Record {
	if askey := aeroKey(bucket, key); askey != nil {
		rec, err := aeroConnectors.Get(nil, askey)
		if !L.IsError(err, `Error getting CACHE `+bucket+` `+key) {
			return rec
		}
	}
	return nil
}

func aeroInc(bucket, key string) int64 {
	if askey := aeroKey(bucket, key); askey != nil {
		ops := []*aero.Operation{
			aero.AddOp(aero.NewBin(`value`, 1)), // add the value of the bin to the existing value
			aero.GetOp(),                        // get the value of the record after all operations are executed
		}
		rec, err := aeroConnectors.Operate(nil, askey, ops...)
		if !L.IsError(err, `Error getting CACHE `+bucket+` `+key) {
			return X.ToI(rec.Bins[`value`])
		}
	}
	return 0
}

func aeroGet2(bucket, key string) (*aero.Record, error) {
	if askey := aeroKey(bucket, key); askey != nil {
		return aeroConnectors.Get(nil, askey)
	}
	return nil, fmt.Errorf(AERO_ERR_DIGEST)
}

func aeroDel(bucket, key string) bool {
	if askey := aeroKey(bucket, key); askey != nil {
		_, err := aeroConnectors.Delete(nil, askey)
		return !L.IsError(err, `Error deleting CACHE `+bucket+` `+key)
	}
	return false
}

// check if using redis or aerospike
func aeroActive() bool {
	return aeroConnectors != nil
}

// getting aerospike ttl policy
func aeroPolicy(ttl int64) *aero.WritePolicy {
	if ttl <= 0 {
		return nil
	}
	idx := I.ToS(ttl)
	if any := AERO_WPS.Get(idx); any != nil {
		wp, ok := any.(*aero.WritePolicy)
		if ok {
			return wp
		}
	}
	wp := aero.NewWritePolicy(0, uint32(ttl))
	AERO_WPS.Set(idx, wp)
	return wp
}

// setting aerospike with write policy (ttl)
func aeroPut(bucket, key string, bin aero.BinMap, ttl int64) bool {
	if askey := aeroKey(bucket, key); askey != nil {
		policy := aeroPolicy(ttl)
		err := aeroConnectors.Put(policy, askey, bin)
		return !L.IsError(err, `Error putting CACHE `+bucket+` `+key)
	}
	return false
}

// using aerospike to touch/set login
func (s *Session) aeroLoginTouch() bool {
	binmap := aero.BinMap{
		`app_id`:      s.AppId,
		`user_id`:     s.UserId,
		`login_email`: s.Email,
	}
	if !aeroPut(AERO_SESSION_UNIQUE_BUCKET, s.Random, binmap, EXPIRE_LOGIN_SEC) {
		return false
	}
	login_key := s.LoginKey()
	return aeroPut(AERO_LOGIN_BUCKET, login_key, binmap, EXPIRE_LOGIN_SEC)
}

///////////////////////////////
/// LOGIN AND SESSION FUNCTIONS

// this function must be called before first session started
func InitSession(expire_ns, renew_ns time.Duration, dbNum int) {
	EXPIRE_NANOSEC = expire_ns
	RENEW_NANOSEC = renew_ns
	EXPIRE_LOGIN_SEC = int64(EXPIRE_NANOSEC) / SEC_TO_NANOSEC
	RENEW_SEC = int64(RENEW_NANOSEC) / SEC_TO_NANOSEC
	if dbNum < 0 {
		AERO_NAMESPACE = `namespace` + I.ToStr(dbNum)
		L.Print(`Session Stored using AeroSpike: ` + AERO_NAMESPACE + `/` + AERO_UNIVERSE_NAMESPACE)
		aeroPolicy(EXPIRE_LEVEL_SEC)
		aeroPolicy(EXPIRE_LOGIN_SEC)
		var err error
		aeroConnectors, err = aero.NewClient(AERO_HOST, AERO_PORT)
		universeAeroConnectors, err = aero.NewClient(AERO_HOST, AERO_PORT)
		L.PanicIf(err, `Failed to connect to in-memory database`)
	} else {
		L.Print(`Session Stored using Redis: ` + I.ToStr(dbNum) + `/` + I.ToStr(REDIS_UNIVERSE_DB))
		redisConnectors = redis.NewClient(&redis.Options{
			Addr:     REDIS_HOST,
			Password: ``,
			DB:       dbNum,
		})
		universeRedisConnectors = redis.NewClient(&redis.Options{
			Addr:     REDIS_HOST,
			Password: ``,
			DB:       REDIS_UNIVERSE_DB,
		})
	}
}

// add ip address, with comma if address already there
func (s *Session) addIpAddr(addr string) {
	addr = S.Trim(addr)
	if len(addr) == 0 || S.Contains(s.IpAddr, addr) {
		return
	}
	if addr != `::1` && S.Contains(addr, `:`) {
		addr, _, _ = net.SplitHostPort(addr)
	}
	if len(s.IpAddr) > 0 {
		s.IpAddr += `,`
	}
	s.IpAddr += addr
}

// parse Ip address: `public.ip,private.ip`
func (s *Session) parseIpAddr(ctx *Context) {
	ip_addr := ctx.Request.RemoteAddr
	s.addIpAddr(ip_addr)
	for _, key := range []string{`X-Real-Ip`, `Http-Client-Ip`, `Http-X-Forwarded-For`, `X-Forwarded-For`, `Http-X-Forwarded`, `X-Forwarded`, `Http-Forwarded-For`, `Forwarded-For`, `Http-Forwarded`, `Forwarded`} {
		ip_addr = ctx.Request.Header.Get(key)
		ip_addrs := S.CompactSplit(ip_addr, `,`)
		for _, addr := range ip_addrs {
			s.addIpAddr(addr)
		}
	}
}

// get session key, Session.Key must be initialized first
func (s Session) LoginKey() string {
	return SESSION_PREFIX + `:` + I.ToS(s.AppId) + `:` + s.prefix() + s.Random
}

// get level key (refreshed every n minutes)
func (s Session) LevelKey() string {
	return I.ToS(s.AppId) + `:` + I.ToS(s.UserId) + RKEY_LEVEL
}

func RedisKey_ByKey_ByRand(app_id int64, key, rand string) string {
	return SESSION_PREFIX + `:` + I.ToS(app_id) + `:` + key + COOKIE_SEPARATOR + rand
}

// for OpenID auth
func (s Session) StateCSRF() string {
	sum := S.CRC64(s.IpAddr + COOKIE_SEPARATOR + s.UserAgent)
	return I.UToBase64(sum)
}

// create new session
func newSession(ctx *Context, app_id int64) Session {
	res := Session{}
	res.parseIpAddr(ctx)
	res.UserAgent = ctx.Request.UserAgent()
	left_key := I.ToS(app_id) + `_`
	if !NO_IP_CHECK {
		left_key += res.IpAddr
	}
	res.AppId = app_id
	res.Key = hashCookie(left_key, res.UserAgent)
	//L.Print(`newSession`,left_key, res.UserAgent,res.Key)
	res.Level = nil
	return res
}

// create hash cookie
func hashCookie(ip, agent string) string {
	key := fmt.Sprintf(`%s%s:%s`, SESSION_PREFIX, ip, agent)
	bytes := []byte(key)
	hasher := sha1.New()
	hasher.Write(bytes)
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	return sha
}

// get prefix, Session.Key must be filled first
func (s Session) prefix() string {
	return s.Key + COOKIE_SEPARATOR
}

// true if less than half
func (s Session) ShouldRenew() bool {
	L.Trace()
	login_key := s.LoginKey()
	if aeroActive() {
		return aeroTTL(AERO_LOGIN_BUCKET, login_key) < RENEW_SEC
	} else {
		return redisTTL(login_key) < RENEW_SEC
	}
}

// save level
func (s *Session) SetLevel(level M.SX) {
	L.Trace()
	lev_key := s.LevelKey()
	if aeroActive() {
		binmap := aero.BinMap(level)
		aeroPut(AERO_LEVEL_BUCKET, lev_key, binmap, EXPIRE_LEVEL_SEC)
	} else {
		redisPut(lev_key, K.ToGOB64(level), EXPIRE_LEVEL_SEC)
	}
	s.Level = level
}

// unset level
func (s *Session) UnsetLevel() bool {
	return s.UnsetLevel_ByUser(s.UserId)
}

// unset other user's level
func (s *Session) UnsetLevel_ByUser(user_id int64) bool {
	L.Trace()
	cache_key := s.LevelKey()
	if aeroActive() {
		return !aeroDel(AERO_LEVEL_BUCKET, cache_key)
	} else {
		return !redisDel(cache_key)
	}
}

// unset globally
func (s *Session) UnsetRaw_ByBucket_ByKey(bucket, key string) bool {
	L.Trace()
	if aeroActive() {
		return !aeroDel(bucket, key)
	} else {
		return !redisDel(key)
	}
}

// load session from cookie if the session key matched
func LoadSession(ctx *Context, app_id int64) {
	res := newSession(ctx, app_id)
	recv_hash, _ := ctx.Request.Cookie(COOKIE_NAME)
	prefix := res.prefix()

	if recv_hash != nil {
		//L.Print(`recv_hash`, recv_hash.Value)
		//L.Print(`prefix`, prefix)
	} else {
		// create new cookie
		res.Random = K.RandLogKey(16)
		res.Save(ctx)
		new_cookie, _ := ctx.Request.Cookie(COOKIE_NAME)
		if new_cookie != nil {
			//L.Print(`New Cookie`, new_cookie.Value)
		} else {
			//L.Print(`NEW COOKIE IS NIL`)
		}
	}

	if recv_hash != nil && !S.StartWith(recv_hash.Value, prefix) {
		//L.Print(`NOT SAME AS PREFIX`)
		ctx.DeleteCookie()
	}

	if recv_hash != nil && S.StartWith(recv_hash.Value, prefix) {
		start := len(prefix)
		res.Random = recv_hash.Value[start:]
		login_key := res.LoginKey()

		var user_id int64
		var login_email string
		if aeroActive() {
			if rec := aeroGet(AERO_LOGIN_BUCKET, login_key); rec != nil {
				user_id = X.ToI(rec.Bins[`user_id`])
				login_email = X.ToS(rec.Bins[`login_email`])
			}
		} else {
			user_id_str := redisGet(login_key)
			if user_id_str != `` {
				user_id = S.ToI(user_id_str)
			}
			login_email = redisGet(login_key + RKEY_EMAIL)
		}
		if user_id > 0 {
			res.UserId = user_id
			res.AppId = app_id
			res.Email = login_email
			lev_key := res.LevelKey()
			if aeroActive() {
				if rec := aeroGet(AERO_LEVEL_BUCKET, lev_key); rec != nil {
					res.Level = M.SX(rec.Bins)
				}
			} else {
				if access_level := redisGet(lev_key); access_level != `` {
					res.Level = K.FromGOB64(access_level)
				}
			}
		} else {
			//L.Print(`USER ID == 0`)
			ctx.Session.Logout()
		}
	} else if app_id > 0 {
		//L.Print(`app_id > 0`)
		for {
			r := K.RandLogKey(16)
			collide := false
			var err error
			if aeroActive() {
				rec, err2 := aeroGet2(AERO_SESSION_UNIQUE_BUCKET, r)
				err = err2
				collide = nil != rec
			} else {
				collisions, err2 := redisConnectors.Keys(SESSION_PREFIX + `:*|` + r).Result()
				err = err2
				collide = len(collisions) > 0
			}
			if collide {
				L.Describe(`Session Collision: ` + r)
				continue
			} else if err != nil {
				L.Describe(`Unknown Error: ` + err.Error())
				break
			}
			res.Random = r
			res.AppId = app_id
			login_key := res.LoginKey()
			if aeroActive() {
				res.aeroLoginTouch()
			} else {
				err = redisConnectors.Expire(login_key, time.Second*time.Duration(EXPIRE_LOGIN_SEC)).Err()
			}
			L.IsError(err, `Failed to init session user_id`)
			break
		}
	}
	ctx.Session = res
}

// login
func (s *Session) Login(user_id int64, login_email string) {
	L.Trace()
	s.UserId = user_id
	s.Email = login_email
	if aeroActive() {
		s.aeroLoginTouch()
	} else {
		login_key := s.LoginKey()
		if !redisPut(login_key, I.ToS(user_id), EXPIRE_LOGIN_SEC) || !redisPut(login_key+RKEY_EMAIL, login_email, EXPIRE_LOGIN_SEC) {
			return
		}
	}
	s.Inc_Global(`login_counter`)
	L.Describe(`Logged: ` + I.ToS(user_id) + ` ` + login_email)
	// TODO: log to database
}

// touch (prolong the session)
func (s *Session) Touch() {
	L.Trace()
	if !s.ShouldRenew() {
		return
	}
	if aeroActive() {
		s.aeroLoginTouch()
	} else {
		err := !redisPut(s.LoginKey(), I.ToS(s.UserId), EXPIRE_LOGIN_SEC) || !redisPut(s.LoginKey()+RKEY_EMAIL, s.Email, EXPIRE_LOGIN_SEC)
		L.CheckIf(err, `Failed to touch session user_id`)
	}
}

// log last visit
func (s *Session) Last(url, referrer string) bool {
	last_visit := url + `|` + referrer + `|` + s.IpAddr + `|` + s.UserAgent
	if aeroActive() {
		binmap := aero.BinMap{
			`last_visit`: last_visit,
		}
		return aeroPut(AERO_LAST_VISIT_BUCKET, s.LevelKey(), binmap, EXPIRE_LOGIN_SEC)
	} else {
		last_visit_key := s.LoginKey() + RKEY_LASTPAGE
		return redisPut(last_visit_key, last_visit, EXPIRE_LOGIN_SEC)
	}
}

// logout
func (s *Session) Logout() {
	L.Trace()
	login_key := s.LoginKey()
	level_key := s.LevelKey()
	if aeroActive() {
		aeroDel(AERO_LOGIN_BUCKET, login_key)
		aeroDel(AERO_LEVEL_BUCKET, level_key)
	} else {
		redisDel(login_key)
		redisDel(login_key + RKEY_EMAIL)
		redisDel(level_key)
	}
	s.UserId = 0
}

func (ctx *Context) DeleteCookie() {
	cookie := http.Cookie{Name: COOKIE_NAME, Value: "", MaxAge: -1, Path: `/`, Expires: time.Now().Add(-100 * time.Hour)}
	http.SetCookie(ctx.Response, &cookie)
	//L.Print(`DELETED COOKIE`, cookie.Value)
	ctx.Session.Logout()
}

// create cookie, Session.Key and Session.Rand must be filled first
func (s Session) CreateCookie() *http.Cookie {
	expiration := time.Now().Add(EXPIRE_NANOSEC)
	value := s.prefix() + s.Random + I.UToS(atomic.AddUint64(&globalCount, 1))
	//L.Print(`>>>>> `,s.prefix())
	cookie := http.Cookie{Name: COOKIE_NAME, Value: value, Expires: expiration, Path: `/`}
	return &cookie
}

// save session, set cookie to client
func (s *Session) Save(ctx *Context) {
	cookie, _ := ctx.Request.Cookie(COOKIE_NAME)
	//L.Print(`WANT TO CREATED NEW COOKIE`)
	if cookie == nil || cookie.Value == `` {
		L.Print(`CREATED NEW COOKIE`)
		cookie = s.CreateCookie()
	}
	http.SetCookie(ctx.Response, cookie)
}

////////////////////////////////////////
// OTHER user's session/cache: _ByUserId

// create a cache_key
func CacheKey_ByUserId_BySuffix(user_id int64, key_suffix string) string {
	return I.ToS(user_id) + `:` + key_suffix
}

// get other user session
func (s *Session) Get_ByUserId(user_id int64, key_suffix string) string {
	cache_key := CacheKey_ByUserId_BySuffix(user_id, key_suffix)
	if aeroActive() {
		if rec := aeroGet(AERO_CACHE_BUCKET, cache_key); rec != nil {
			return X.ToS(rec.Bins[`value`])
		}
		return ``
	}
	return redisGet(CACHE_PREFIX + cache_key)
}

func (s *Session) GetTTL_ByUserId(user_id int64, key_suffix string) int64 {
	cache_key := CacheKey_ByUserId_BySuffix(user_id, key_suffix)
	if aeroActive() {
		return aeroTTL(AERO_CACHE_BUCKET, cache_key)
	}
	return redisTTL(CACHE_PREFIX + cache_key)
}

// set other user session
func (s *Session) Set_ByUserId(user_id int64, key_suffix, val string) bool {
	cache_key := CacheKey_ByUserId_BySuffix(user_id, key_suffix)
	if aeroActive() {
		binmap := aero.BinMap{
			`value`: val,
		}
		return aeroPut(AERO_CACHE_BUCKET, cache_key, binmap, 0)
	}
	return redisPut(CACHE_PREFIX+cache_key, val, 0)
}

// set other user session, with TTL
func (s *Session) Set_ByUserIdTTL(user_id int64, key_suffix, val string, expired_sec int64) bool {
	cache_key := CacheKey_ByUserId_BySuffix(user_id, key_suffix)
	if aeroActive() {
		binmap := aero.BinMap{
			`value`: val,
		}
		return aeroPut(AERO_CACHE_BUCKET, cache_key, binmap, expired_sec)
	}
	return redisPut(CACHE_PREFIX+cache_key, val, expired_sec)
}

// del other user session
func (s *Session) Del_ByUserId(user_id int64, key_suffix string) bool {
	cache_key := CacheKey_ByUserId_BySuffix(user_id, key_suffix)
	if aeroActive() {
		return aeroDel(AERO_CACHE_BUCKET, cache_key)
	}
	return redisDel(CACHE_PREFIX + cache_key)
}

////////////////////////////////////////////
// CURRENT user's session/cache: (no suffix)

// get local (this user) session
func (s *Session) Get(key_suffix string) string {
	return s.Get_ByUserId(s.UserId, key_suffix)
}

// set local (this user) session
func (s *Session) Set(key_suffix, val string) bool {
	return s.Set_ByUserId(s.UserId, key_suffix, val)
}

// set local (this user) session, with TTL
func (s *Session) SetTTL(key_suffix, val string, expired_sec int64) bool {
	return s.Set_ByUserIdTTL(s.UserId, key_suffix, val, expired_sec)
}

// del local (this user) session
func (s *Session) Del(key_suffix string) bool {
	return s.Del_ByUserId(s.UserId, key_suffix)
}

////////////////////////////////
// GLOBAL session/cache: _Global

// get global session
func (s *Session) Get_GlobalTTL(key_suffix string) int64 {
	if aeroActive() {
		return aeroTTL(AERO_GLOBAL_BUCKET, key_suffix)
	}
	key := GLOBAL_PREFIX + key_suffix
	return redisTTL(key)
}

// get global session
func (s *Session) Get_Global(key_suffix string) string {
	if aeroActive() {
		if rec := aeroGet(AERO_GLOBAL_BUCKET, key_suffix); rec != nil {
			return X.ToS(rec.Bins[`value`])
		}
		return ``
	}
	key := GLOBAL_PREFIX + key_suffix
	return redisGet(key)
}

// increment global session
func (s *Session) Inc_Global(key_suffix string) int64 {
	if aeroActive() {
		return aeroInc(AERO_GLOBAL_BUCKET, key_suffix)
	}
	key := GLOBAL_PREFIX + key_suffix
	return redisInc(key)
}

// set global session
func (s *Session) Set_Global(key_suffix, val string) bool {
	if aeroActive() {
		binmap := aero.BinMap{
			`value`: val,
		}
		return aeroPut(AERO_GLOBAL_BUCKET, key_suffix, binmap, 0)
	}
	key := GLOBAL_PREFIX + key_suffix
	return redisPut(key, val, 0)
}

// delete global session with TTL
func (s *Session) Set_GlobalTTL(key_suffix, val string, expired_sec int64) bool {
	if aeroActive() {
		binmap := aero.BinMap{
			`value`: val,
		}
		return aeroPut(AERO_GLOBAL_BUCKET, key_suffix, binmap, expired_sec)
	}
	key := GLOBAL_PREFIX + key_suffix
	return redisPut(key, val, expired_sec)
}

// delete global session
func (s *Session) Del_Global(key_suffix string) bool {
	if aeroActive() {
		return aeroDel(AERO_GLOBAL_BUCKET, key_suffix)
	}
	key := GLOBAL_PREFIX + key_suffix
	return redisDel(key)
}

////////////////////////////////
// UNIVERSE session/cache: _Universe

func (s *Session) get_UniverseKey(key_suffix string) *aero.Key {
	askey, err := aero.NewKey(AERO_UNIVERSE_NAMESPACE, AERO_UNIVERSE_BUCKET, key_suffix)
	if L.IsError(err, AERO_ERR_DIGEST) {
		return nil
	}
	return askey
}

// get universe  session
func (s *Session) Get_UniverseTTL(key_suffix string) int64 {
	if aeroActive() {
		if askey := s.get_UniverseKey(key_suffix); askey != nil {
			rec, err := universeAeroConnectors.GetHeader(nil, askey)
			if err == nil && rec != nil {
				return int64(rec.Expiration)
			}
		}
		return 0
	}
	key := UNIVERSE_PREFIX + key_suffix
	re, err := universeRedisConnectors.TTL(key).Result()
	L.IsError(err, `failed to TTL`, key)
	return int64(re.Seconds())
}

// get universe  session
func (s *Session) Get_Universe(key_suffix string) string {
	if aeroActive() {
		if askey := s.get_UniverseKey(key_suffix); askey != nil {
			rec, err := universeAeroConnectors.Get(nil, askey)
			if !L.IsError(err, `Error getting CACHE `+askey.String()) && rec != nil {
				return X.ToS(rec.Bins[`value`])
			}
		}
		return ``
	}
	key := UNIVERSE_PREFIX + key_suffix
	L.Print(`universeRedisConnectors`, universeRedisConnectors)
	L.Print(`key`, key)
	re, err := universeRedisConnectors.Get(key).Result()
	L.Print(universeRedisConnectors.Get(key))
	L.IsError(err, `failed to GET`, key)
	return re
}

// increment universe  session
func (s *Session) Inc_Universe(key_suffix string) int64 {
	if aeroActive() {
		if askey := s.get_UniverseKey(key_suffix); askey != nil {
			ops := []*aero.Operation{
				aero.AddOp(aero.NewBin(`value`, 1)), // add the value of the bin to the existing value
				aero.GetOp(),                        // get the value of the record after all operations are executed
			}
			rec, err := aeroConnectors.Operate(nil, askey, ops...)
			if !L.IsError(err, `Error getting CACHE `+askey.String()) && rec != nil {
				return X.ToI(rec.Bins[`value`])
			}
		}
		return 0
	}
	key := UNIVERSE_PREFIX + key_suffix
	re, err := universeRedisConnectors.Incr(key).Result()
	L.IsError(err, `failed to INCR`, key)
	return re
}

// set universe session
func (s *Session) Set_Universe(key_suffix, val string) bool {
	if aeroActive() {
		if askey := s.get_UniverseKey(key_suffix); askey != nil {
			binmap := aero.BinMap{
				`value`: val,
			}
			err := universeAeroConnectors.Put(nil, askey, binmap)
			return !L.IsError(err, `Error putting CACHE `+askey.String())
		}
		return false
	}
	key := UNIVERSE_PREFIX + key_suffix
	err := universeRedisConnectors.Set(key, val, 0).Err()
	return !L.IsError(err, `failed to SET`, key, val)
}

// set universe  session with TTL
func (s *Session) Set_UniverseTTL(key_suffix, val string, expired_sec int64) bool {
	if aeroActive() {
		if askey := s.get_UniverseKey(key_suffix); askey != nil {
			policy := aeroPolicy(expired_sec)
			binmap := aero.BinMap{
				`value`: val,
			}
			err := universeAeroConnectors.Put(policy, askey, binmap)
			return !L.IsError(err, `Error putting CACHE `+askey.String())
		}
		return false
	}
	key := UNIVERSE_PREFIX + key_suffix
	err := universeRedisConnectors.Set(key, val, time.Second*time.Duration(expired_sec)).Err()
	return !L.IsError(err, `failed to SETEX`, key, expired_sec, val)
}

// set universe TTL
func (s *Session) Expire_Universe(key_suffix string, expired_sec int64) bool {
	if aeroActive() {
		// TODO: not yet implemented
		return false
	}
	key := UNIVERSE_PREFIX + key_suffix
	err := universeRedisConnectors.Expire(key, time.Second*time.Duration(expired_sec)).Err()
	return !L.IsError(err, `failed to EXPIRE`, key, expired_sec)
}

// delete universe session
func (s *Session) Del_Universe(key_suffix string) bool {
	if aeroActive() {
		if askey := s.get_UniverseKey(key_suffix); askey != nil {
			_, err := universeAeroConnectors.Delete(nil, askey)
			return !L.IsError(err, `Error deleting CACHE `+askey.String())
		}
		return false
	}
	key := UNIVERSE_PREFIX + key_suffix
	err := universeRedisConnectors.Del(key).Err()

	return !L.IsError(err, `failed to DEL`, key)
}
