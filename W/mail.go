package W

import (
	"github.com/jordan-wright/email"
	"gitlab.com/kokizzu/gokil/A"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"net/smtp"
)

type SmtpConfig struct {
	Name     string
	Username string
	Password string
	Hostname string
	Port     int
}

func (mc *SmtpConfig) Address() string {
	return mc.Hostname + `:` + I.ToStr(mc.Port)
}

func (mc *SmtpConfig) Auth() smtp.Auth {
	return smtp.PlainAuth(``, mc.Username, mc.Password, mc.Hostname)
}
func (mc *SmtpConfig) From() string {
	return mc.Name + ` <` + mc.Username + `>`
}

// run sendbcc on another goroutine
func (mc *SmtpConfig) SendBCC(bcc []string, subject string, message string) {
	L.Print(`SendBCC started ` + A.StrJoin(bcc, `, `) + `; subject: ` + subject)
	go mc.SendSyncBCC(bcc, subject, message)
}

// run sendAttachbcc on another goroutine
func (mc *SmtpConfig) SendAttachBCC(bcc []string, subject string, message string, files []string) {
	L.Print(`SendAttachBCC started ` + A.StrJoin(bcc, `, `) + `; subject: ` + subject)
	go mc.SendSyncAttachBCC(bcc, subject, message, files)
}

// sendbcc synchronous version, returns error message
func (mc *SmtpConfig) SendSyncBCC(bcc []string, subject string, message string) string {
	return mc.SendSyncAttachBCC(bcc, subject, message, []string{})
}

// sendbcc synchronous version, returns error message
func (mc *SmtpConfig) SendSyncAttachBCC(bcc []string, subject string, message string, files []string) string {
	e := email.NewEmail()
	e.From = mc.From()
	e.To = []string{e.From}
	e.Bcc = bcc
	e.Subject = subject
	attach := A.StrJoin(files, ` `)
	for _, file := range files {
		e.AttachFile(file)
	}
	if attach != `` {
		attach = `; attachments: ` + attach
	}
	e.HTML = []byte(message)
	L.Describe(e.Subject, e.Bcc)
	err := e.Send(mc.Address(), mc.Auth())
	if L.IsError(err, `failed to SendBCC`) {
		return err.Error()
	}
	L.Print(`SendAttachBCC completed ` + A.StrJoin(bcc, `, `) + attach + `; subject: ` + subject)
	return ``
}

// 2018-10-09 Sadewo
// run sendAttachbcc on another goroutine and callback
func (mc *SmtpConfig) SendAttachBCCWithCallback(to, bcc, reply_to []string, subject string, message string, files []string, fn func(int64, string), id int64) {
	L.Print(`SendAttachBCCWithErr started ` + A.StrJoin(bcc, `, `) + `; subject: ` + subject)
	go mc.SendSyncAttachBCCWithCallback(to, bcc, reply_to, subject, message, files, fn, id)
}

// 2018-10-09 Sadewo
// sendattach async with callback if error
func (mc *SmtpConfig) SendSyncAttachBCCWithCallback(to, bcc, reply_to []string, subject string, message string, files []string, fn func(int64, string), id int64) {
	e := email.NewEmail()
	e.From = mc.From()
	e.To = to
	e.Bcc = bcc
	e.Subject = subject
	if len(reply_to) >= 1 {
		e.ReplyTo = reply_to
	}
	attach := A.StrJoin(files, ` `)
	for _, file := range files {
		e.AttachFile(file)
	}
	if attach != `` {
		attach = `; attachments: ` + attach
	}
	e.HTML = []byte(message)
	err := e.Send(mc.Address(), mc.Auth())
	if L.IsError(err, `failed to SendBCC`) {
		fn(id, err.Error())
		return
	}
	L.Print(`SendAttachBCC completed ` + A.StrJoin(bcc, `, `) + attach + `; subject: ` + subject)
}
