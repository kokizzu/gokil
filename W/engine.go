// PUKIS' WebEngine (context, post, param, engine) Helper
package W

// modified from: https://raw.githubusercontent.com/julienschmidt/httprouter/master/router.go

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/fatih/color"
	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"github.com/streamrail/concurrent-map"
	"github.com/tdewolff/minify"
	//"github.com/tdewolff/minify/css"
	//"github.com/tdewolff/minify/js"
	"gitlab.com/kokizzu/gokil/A"
	"gitlab.com/kokizzu/gokil/F"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/K"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/T"
	"gitlab.com/kokizzu/gokil/X"
	"gitlab.com/kokizzu/gokil/Z"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var START_TIME time.Time

const SHOW_STATIC = false

func init() {
	START_TIME = time.Now()
	fmt.Println(color.GreenString(`Initializing server..`))
}

//////////////////////
/// PARAM and PARAMS

// Param is a single URL parameter, consisting of a key and a value.
type Param struct {
	Key   string
	Value string
}

// Params is a struct
// Params.Ordered[int]
// Params.ByName[string]
type Params struct {
	Ordered []Param
	ByName  M.SS
}

func (ps *Params) Count() int {
	return len(ps.Ordered)
}

// Add param, newer key will overwrite older key
func (ps *Params) Add(key, value string) {
	param := Param{key, value}
	ps.Ordered = append(ps.Ordered, param)
	if ps.ByName == nil {
		ps.ByName = make(M.SS)
	}
	ps.ByName[param.Key] = param.Value
}

// get params at index as integer
func (ps *Params) GetInt(idx int) int64 {
	if idx >= len(ps.Ordered) {
		return 0
	}
	p := ps.Ordered[idx]
	val, ok := S.ToInteger(p.Value)
	if !ok {
		return 0
	}
	return val
}

// get params at index as string
func (ps *Params) GetStr(idx int) string {
	if idx >= len(ps.Ordered) {
		return ``
	}
	p := ps.Ordered[idx]
	return p.Value
}

// get params at index as float
func (ps *Params) GetFloat(idx int) float64 {
	if idx >= len(ps.Ordered) {
		return 0
	}
	p := ps.Ordered[idx]
	return S.ToF(p.Value)
}

/////////
// POST
type Posts struct {
	url.Values
}

// create new posts
func NewPosts() *Posts {
	return &Posts{
		make(url.Values),
	}
}

// read from context (GET)
func (post *Posts) FromContext(ctx *Context) {
	err := errors.New(``)
	post.Values, err = url.ParseQuery(ctx.Request.URL.RawQuery)
	L.IsError(err, `Failed to parse from context`)
}

// first []string to int64, return 0 if empty
func (post *Posts) GetInt(key string) int64 {
	str := post.GetStr(key)
	if str == `` {
		return 0
	}
	return S.ToI(str)
}

// get int with min max value
func (post *Posts) GetIntMinMax(key string, min, max int64) int64 {
	val := post.GetInt(key)
	if val < min {
		val = min
	}
	if val > max {
		val = max
	}
	return val
}

// first []string to bool, return false if empty
func (post *Posts) GetBool(key string) bool {
	str := post.GetStr(key)
	return str == `true`
}

// get first []string, return `` if empty
func (post *Posts) GetStr(key string) string {
	t := post.Get(key)
	return t
}

// get first []string trimmed, return `` if empty
func (post *Posts) GetStrTrim(key string) string {
	t := post.Get(key)
	return S.Trim(t)
}

// is not set
func (post *Posts) IsSet(key string) bool {
	return len(post.Values[key]) > 0
}

// get first []string, return []interface{}{} if empty
func (post *Posts) GetJsonArr(key string) A.X {
	return S.JsonToArr(post.Get(key))
}

// 2015-09-03 | anton
// get first []string, return map[string]interface{}{} if empty
func (post *Posts) GetJsonMap(key string) M.SX {
	return S.JsonToMap(post.Get(key))
}

// 2016-02-05 Kiz
func (post *Posts) GetJsonStrArr(key string) []string {
	ax := S.JsonToArr(post.Get(key))
	as := []string{}
	for _, v := range ax {
		as = append(as, X.ToS(v))
	}
	return as
}

// 2015-12-28 Kiz
func (post *Posts) GetJsonIntArr(key string) []int64 {
	ax := S.JsonToArr(post.Get(key))
	ai := []int64{}
	for _, v := range ax {
		ai = append(ai, X.ToI(v))
	}
	return ai
}

// first []string to float64, return 0 if empty
func (post *Posts) GetFloat(key string) float64 {
	str := post.GetStr(key)
	if str == `` {
		return 0
	}
	return S.ToF(str)
}

func (post *Posts) String() string {
	b := bytes.Buffer{}
	for k, v := range post.Values {
		b.WriteString(` | `)
		b.WriteString(k)
		b.WriteByte(' ')
		if k == `pass` || k == `password` {
			passlen := len(v[0])
			censored := S.Repeat(`*`, passlen)
			b.WriteString(censored)
		} else {
			b.WriteString(v[0])
		}
	}
	return b.String()
}

/////////////
/// CONTEXT

// All request context and response, including session
type Context struct {
	Request            *http.Request
	Response           http.ResponseWriter
	Actions            []Action
	Params             Params
	Engine             *Engine
	Code               int
	ContentType        string
	Session            Session
	Buffer             *bytes.Buffer
	Title              string
	FromFile           string
	NoLayout           bool
	IsMultipartRequest bool
	Path               []string
}

// debug info
func (ctx *Context) RequestDebugStr() string {
	app_id := ctx.Session.AppId
	has_app := app_id > 0
	return ctx.Session.IpAddr + "\n" +
		ctx.Session.UserAgent + "\n" +
		ctx.Title + "\n" +
		T.DateTimeStr() + `<br/>` +
		S.IfElse(ctx.IsAjax(), `POST`, `GET`) + ` ` + A.StrJoin(ctx.Path, `/`) + "\n" +
		S.If(has_app, `App / `) + `Actor: ` + S.If(has_app, I.ToS(app_id)+` / `) + I.ToS(ctx.Session.UserId) + " / " + ctx.Session.Email + "\n\n" +
		ctx.Posts().String()
}

// non debug info
func (ctx *Context) RequestHtmlStr() string {
	app_id := ctx.Session.AppId
	has_app := app_id > 0
	return `IP Address: ` + ctx.Session.IpAddr + `<br/>` +
		`User Agent:  ` + ctx.Session.UserAgent + `<br/>` +
		`Request Path: ` + A.StrJoin(ctx.Path, `/`) + `<br/>` +
		`Server Time: ` + T.DateTimeStr() + `<br/>` +
		S.If(has_app, `App / `) + `Actor: ` + S.If(has_app, I.ToS(app_id)+` / `) + I.ToS(ctx.Session.UserId) + " / " + ctx.Session.Email + "<br/>"
}

// is ajax
func (ctx *Context) IsAjax() bool {
	return ctx.Request.Method == `POST`
}

// get protocol prefix
func (ctx *Context) Host() string {
	prefix := `http`
	req := ctx.Request
	if req.TLS != nil || req.Header.Get(`X-Forwarded-Proto`) == `https` {
		prefix += `s`
	}
	return prefix + `://` + req.Host
}

// 2015-11-12 Active Users Count
func (ctx *Context) ActiveUsersCount() string {
	active_users := ctx.Session.Get_Global(`active_users`)
	//L.Describe(`get`, active_users)
	if active_users == `` {
		active_users = S.Trim(string(K.RunCmd(ctx.Engine.BaseDir + `server_stat.sh`)))
		num := S.ToI(active_users) / 3
		if num < 0 {
			active_users = ``
		} else {
			active_users = I.ToS(num)
		}
		//L.Describe(`set_ttl`, active_users)
		ctx.Session.Set_GlobalTTL(`active_users`, active_users, 60)
	}
	return active_users
}

// force download, don't forget to set the content type
func (ctx *Context) ForceDownload(filename string) {
	if filename[0] == '/' {
		filename = filename[1:]
	}
	header := ctx.Response.Header()
	header.Set(`Content-Disposition`, `attachment; filename=`+filename)
}

// error check
func (ctx *Context) Error(text string, any ...interface{}) {
	L.Trace()
	detail := ``
	if len(any) == 1 {
		v, ok := any[0].(string)
		if ok {
			detail = v
		} else {
			detail = K.ToJson5(any[0])
		}
	} else if len(any) > 1 {
		detail = K.ToJson5(any)
	}
	_vals := M.Merge_Map(ctx.Engine.OptionalAssets, M.SX{
		`requested_path`: ctx.Request.URL.String(),
		`webmaster`:      ctx.Engine.WebMasterAnchor,
		`message`:        S.Z(text),
		`detail`:         detail,
	})
	ctx.Render(`error`, _vals)
}
func (ctx *Context) Check404(check bool, msg string, any ...interface{}) bool {
	L.Trace()
	if L.CheckIf2(check, msg, any...) {
		ServeError(404, ctx)
		return true
	}
	return false
}
func (ctx *Context) CheckWebMaster() bool {
	L.Trace()
	return ctx.Check403(!ctx.IsWebMaster(), `must be webmaster`)
}
func (ctx *Context) Check403(check bool, msg string, any ...interface{}) bool {
	L.Trace()
	if L.CheckIf2(check, msg, any...) {
		ServeError(403, ctx)
		return true
	}
	return false
}
func (ctx *Context) Check500(check bool, msg string, any ...interface{}) bool {
	L.Trace()
	if L.CheckIf2(check, msg, any...) {
		ServeError(500, ctx)
		return true
	}
	return false
}

// create new context
func NewContext(rw http.ResponseWriter, req *http.Request, engine *Engine) (ctx *Context) {
	ctx = &Context{
		Response: rw,
		Request:  req,
		Engine:   engine,
		Code:     200,
		Buffer:   bytes.NewBuffer(nil),
		Actions:  []Action{Logger, Recover},
		Path:     S.CompactSplit(req.URL.Path, `/`),
	}
	ctx.Response.Header().Set(`Server`, engine.Name)
	if req.Method == `GET` {
		ctx.ContentType = `text/html`
	} else {
		ctx.ContentType = `application/json`
		req_ct := ctx.Request.Header.Get(`content-type`)
		ctx.IsMultipartRequest = S.StartWith(req_ct, `multipart/form-data`)
		if ctx.IsMultipartRequest {
			ctx.Request.ParseMultipartForm(32 * 1024 * 1024) // 32 MB
		} else {
			ctx.Request.ParseForm()
		}
	}
	return
}

// get first request path's segment
func (ctx *Context) FirstPath() string {
	if len(ctx.Path) == 0 {
		return ``
	}
	return ctx.Path[0]
}

// Write everything
func (ctx *Context) Finish() {
	// TODO: add caching for static files and anything (cache by url + user_id) to Redis
	// TODO: add compression to static files and anything (or use Nginx's), see https://gist.github.com/the42/1956518
	//	canGzip := S.Contains(ctx.Request.Header.Get(`Accept-Encoding`), `gzip`)
	//	if canGzip {
	//		ctx.Response.Header().Set(`Content-Encoding`, `gzip`)
	//		gz := gzip.NewWriter(ctx.Response)
	//		defer gz.Close()
	//		gzr := gzipResponseWriter{Writer: gz, ResponseWriter: ctx.Response}
	//	}
	if ctx.FromFile != `` {
		http.ServeFile(ctx.Response, ctx.Request, ctx.FromFile)
		return
	}
	L.Trace()
	ctx.Session.Save(ctx)
	ctx.Response.Header().Set(`Content-Type`, ctx.ContentType)
	ctx.Response.WriteHeader(ctx.Code)
	if !ctx.NoLayout {
		// append a layout
		buf := bytes.NewBuffer(nil)
		ctx.Engine.LoadLayout()
		ctx.Engine.ViewLayout.Render(buf, M.SX{
			`title`:        ctx.Title,
			`project_name`: ctx.Engine.Name,
			`contents`:     string(ctx.Buffer.Bytes()),
			`resources`:    ctx.Engine.ViewResources,
			`debug_mode`:   ctx.Engine.DebugMode,
			`is_admin`:     ctx.IsWebMaster(),
		})
		ctx.Buffer = buf
	}
	ctx.Response.Write(ctx.Buffer.Bytes())
}

// Run next filter, if any, or the last action if any
func (ctx *Context) Next() Action {
	if ctx.Actions == nil || len(ctx.Actions) == 0 {
		panic(`action-chain unavailable`)
	}
	action := ctx.Actions[0]
	ctx.Actions = ctx.Actions[1:]
	return action
}

func (ctx *Context) GetView(name string) (*Z.TemplateChain, bool) {
	chain, ok := ctx.Engine.ViewCache.Get(name)
	if !ok {
		return nil, ok
	}
	return chain.(*Z.TemplateChain), ok
}

func (ctx *Context) Render(name string, values M.SX) {
	debug := ctx.Engine.DebugMode
	chain, ok := ctx.GetView(name)
	//	L.Describe(chain == nil, name)
	if !ok {
		newchain, err := Z.ParseFile(debug, debug, ctx.Engine.ViewDir+name+`.html`)
		msg := `Template file not found: ` + name
		if ctx.Check500(err != nil, msg) {
			return
		}
		chain = newchain
		ctx.Engine.ViewCache.Set(name, chain)
		//		L.Describe(`caching`, name)
	}
	// @20220623 CP: merge with optional assets
	_vals := M.Merge_Map(ctx.Engine.OptionalAssets, values)
	chain.Render(ctx.Buffer, _vals)
}

func (ctx *Context) RenderNoDebug(name string, values M.SX) {
	chain, ok := ctx.GetView(name)
	debug := ctx.Engine.DebugMode
	//	L.Describe(chain == nil, name)
	if !ok {
		newchain, err := Z.ParseFile(debug, false, ctx.Engine.ViewDir+name+`.html`)
		msg := `Template file not found: ` + name
		if ctx.Check500(err != nil, msg) {
			return
		}
		chain = newchain
		ctx.Engine.ViewCache.Set(name, chain)
		//		L.Describe(`caching`, name)
	}
	_vals := M.Merge_Map(ctx.Engine.OptionalAssets, values)
	chain.Render(ctx.Buffer, _vals)
}

// render to a bytes.Buffer
func (ctx *Context) Partial(name string, values M.SX) string {
	b := bytes.Buffer{}
	debug := ctx.Engine.DebugMode
	chain, ok := ctx.GetView(name)
	//	L.Describe(chain == nil, name)
	if !ok {
		newchain, err := Z.ParseFile(debug, debug, ctx.Engine.ViewDir+name+`.html`)
		if L.IsError(err, `Template file not found: `+name) {
			return ``
		}
		chain = newchain
		ctx.Engine.ViewCache.Set(name, chain)
		//		L.Describe(`caching`, name)
	}
	_vals := M.Merge_Map(ctx.Engine.OptionalAssets, values)
	chain.Render(&b, _vals)
	return b.String()
}

func (ctx *Context) PartialNoDebug(name string, values M.SX) string {
	b := bytes.Buffer{}
	debug := ctx.Engine.DebugMode
	chain, ok := ctx.GetView(name)
	//	L.Describe(chain == nil, name)
	if !ok {
		newchain, err := Z.ParseFile(debug, false, ctx.Engine.ViewDir+name+`.html`)
		if L.IsError(err, `Template file not found: `+name) {
			return ``
		}
		chain = newchain
		ctx.Engine.ViewCache.Set(name, chain)
		//		L.Describe(`caching`, name)
	}
	_vals := M.Merge_Map(ctx.Engine.OptionalAssets, values)
	chain.Render(&b, _vals)
	return b.String()
}

// write an error
func (ctx *Context) Abort(code int, debugMode bool, err error) {
	L.Trace()
	ctx.Code = code
	ctx.Title += ` (error)`
	//ctx.ContentType = `text/plain`
	//ctx.NoLayout = true
	msg := ``
	detail := ``
	if debugMode {
		msg = spew.Sdump(err)
		detail = string(L.CensoredStackTrace(3))
	} else {
		msg = `Internal Server Error`
		ref_code := uuid.NewV4().String()
		L.Describe(ref_code)
		fmt.Println(L.StackTrace(3))
		detail = `Detailed error message not available on production mode, error reference code for webmaster: ` + ref_code
	}
	ctx.Error(msg, detail)
}

// append json
func (ctx *Context) AppendJson(any M.SX) {
	L.Trace()
	buf, err := json.Marshal(any)
	L.IsError(err, `error converting to json`)
	ctx.Buffer.Write(buf)
}

// append bytes
func (ctx *Context) AppendBytes(buf []byte) {
	L.Trace()
	ctx.Buffer.Write(buf)
}

// append buffer
func (ctx *Context) AppendBuffer(buff bytes.Buffer) {
	ctx.Buffer.Write(buff.Bytes())
}

// append string
func (ctx *Context) AppendString(txt string) {
	L.Trace()
	ctx.Buffer.WriteString(txt)
}

// get params
func (ctx *Context) Posts() *Posts {
	if ctx.IsMultipartRequest {
		return &Posts{ctx.Request.MultipartForm.Value}
	}
	return &Posts{ctx.Request.Form}
}

func (ctx *Context) IsWebMaster() bool {
	return ctx.Engine.WebMaster[ctx.Session.Email] != ``
}

// get file, similar to $_FILES[id][0]
// returns client-side filename and extension, server-side content-type, and reader
// don't foget to close the reader.Close() when no longer used
// if name is empty, the ext would contain values
func (ctx *Context) UploadedFile(id string) (fileName, ext, contentType string, reader multipart.File) {
	file, header, err := ctx.Request.FormFile(id)
	if err == nil {
		buff := make([]byte, 512)
		_, err := file.Read(buff)
		if err != nil {
			ext = err.Error()
			file.Close()
			return
		}
		contentType = http.DetectContentType(buff) // do not trust header.Get(`content-type`)[0]
		file.Seek(0, 0)
		fileName = header.Filename
		if fileName == `` {
			fileName = `tmp`
		}
		ext = filepath.Ext(fileName)
		ext = S.ToLower(ext)
		reader = file
	} else {
		ext = err.Error()
	}
	return
}

////////////
/// ACTION

type Action func(controller *Context)

// default Action: recover from error
func Recover(ctx *Context) {
	defer func() {
		if err := recover(); err != nil {
			//L.Panic(errors.New(`Internal Server Error`), ``, err)
			err2, ok := err.(error)
			if !ok {
				err2 = errors.New(fmt.Sprintf("%# v", err))
			}
			err_str := err2.Error()
			L.LOG.Errorf(err_str)
			str := L.StackTrace(2)
			L.LOG.Criticalf("StackTrace: %s", str)
			if `pq: remaining connection slots are reserved for non-replication superuser connections` == err_str || `sorry, too many clients already` == err_str {
				ServeError(503, ctx)
			} else {
				ctx.Abort(500, ctx.Engine.DebugMode, err2)
			}
			if !ctx.Engine.DebugMode {
				ctx.Engine.SendDebugMail(
					ctx.RequestDebugStr() + "\n\n" + err_str)
			}
		}
	}()
	if SHOW_STATIC {
		L.Trace()
	}
	ctx.Next()(ctx)
}

// default Action: log all access
func Logger(ctx *Context) {
	start := time.Now()
	ctx.Next()(ctx)
	if SHOW_STATIC || ctx.FromFile == `` || ctx.Code >= 400 {
		L.Trace()
		var code string
		if ctx.Code < 400 {
			code = L.BgGreen(`%s`, color.BlueString(`%3d`, ctx.Code))
		} else {
			code = L.BgRed(`%3d`, ctx.Code)
		}
		ones := `nano`
		elapsed := float64(time.Since(start).Nanoseconds())
		if elapsed > 1000000000.0 {
			elapsed /= 1000000000.0
			ones = `sec`
		} else if elapsed > 1000000.0 {
			elapsed /= 1000000.0
			ones = `mili`
		} else if elapsed > 1000.0 {
			elapsed /= 1000.0
			ones = `micro`
		}
		referrer := ctx.Request.Referer()
		url := ctx.Request.URL.String()
		msg := fmt.Sprintf(`[%s] %7d %7.2f %5s | %4s %-40s | %-40s | %15s | %4d | %s %s`,
			code,
			ctx.Buffer.Len(),
			elapsed,
			ones,
			ctx.Request.Method,
			url,
			referrer,
			ctx.Session.IpAddr,
			ctx.Session.UserId,
			ctx.Session.Email,
			ctx.Posts().String())
		msg = S.Replace(msg, `%`, `%%`)
		L.LOG.Notice(msg)
	}
}

// default Action: serve error
func ServeError(code int, ctx *Context) {
	L.Trace()
	ctx.Code = code
	name := I.ToStr(code)
	values := M.SX{
		`requested_path`: ctx.Request.URL.String(),
		`webmaster`:      ctx.Engine.WebMasterAnchor,
	}
	if code == 503 {
		values[`cpu_usage`] = F.ToStr(K.PercentCPU())
		values[`ram_usage`] = F.ToStr(K.PercentRAM())
	}
	_vals := M.Merge_Map(ctx.Engine.OptionalAssets, values)
	ctx.Render(name, _vals)
}

// default Action: error 403,404,500
func Error404(ctx *Context) {
	L.Trace()
	ServeError(404, ctx)
}

// default Action: error 404 for ajax
func Ajax404(ctx *Context) {
	L.Trace()
	ajax := M.SX{
		`errors`:     []string{`404: no handler for this route: ` + ctx.Request.URL.String()},
		`is_success`: false,
		`info`:       ``,
	}
	ctx.AppendJson(ajax)
}

// serve static file Action
func ServeStatic(ctx *Context) {
	name := ctx.Request.URL.String()
	name = filepath.Clean(ctx.Engine.PublicDir + name)
	if !S.StartWith(name, ctx.Engine.PublicDir) {
		ctx.Next()(ctx)
		return
	}
	name = S.LeftOf(name, `?`)
	finfo, err := os.Stat(name)
	if os.IsNotExist(err) || finfo.IsDir() {
		ctx.Next()(ctx)
		return
	}
	ctx.FromFile = name
}

///////////
// ENGINE

// An engine is just a list of filter and global default action (404)
type Engine struct {
	*Router
	Filters         []Action
	pageNotFound    Action
	ajaxNotFound    Action
	ViewCache       cmap.ConcurrentMap
	ViewLayout      *Z.TemplateChain // outer layout
	ViewResources   string           // js/css on html
	BaseDir         string
	PublicDir       string
	ViewDir         string
	WebMaster       M.SS
	DebugMode       bool
	Name            string
	Include         [][2]string
	FileServer      http.Handler
	MailAccounts    map[string]*SmtpConfig
	WebMasterMail   string
	WebMasterAnchor string
	GlobalInt       M.SI
	GlobalStr       M.SS
	GlobalAny       M.SX
	BaseUrls        M.SS
	MultiApp        bool
	CreatedAt       time.Time
	DisableMinifer  bool
	MinifierSuffix  string
	Assets          [][4]string
	OptionalAssets  M.SX
}

// New returns a new initialized Router.
func NewEngine(debugMode bool, multiApp bool, projectName string, mailAccounts map[string]*SmtpConfig, webMaster M.SS, webMasterMail, baseDir string, assets [][4]string, baseUrls M.SS, staticSubdir ...string) *Engine {
	engine := &Engine{
		Router:          &Router{},
		Filters:         []Action{},
		pageNotFound:    Error404,
		ajaxNotFound:    Ajax404,
		DebugMode:       debugMode,
		BaseDir:         baseDir,
		PublicDir:       baseDir + `public/`,
		ViewDir:         baseDir + `views/`,
		ViewCache:       cmap.New(),
		Name:            projectName,
		WebMaster:       webMaster,
		MailAccounts:    mailAccounts,
		WebMasterMail:   webMasterMail,
		WebMasterAnchor: `<a href='mailto:` + webMasterMail + `'>webmasters</a>`,
		GlobalInt:       M.SI{},
		GlobalStr:       M.SS{},
		GlobalAny:       M.SX{},
		BaseUrls:        baseUrls,
		MultiApp:        multiApp,
		CreatedAt:       time.Now(),
		Assets:          assets,
	}

	engine.LoadLayout()

	// initialize engine filters and static file handling
	engine.FileServer = http.FileServer(http.Dir(engine.PublicDir))
	return engine
}

// 2015-11-23 Kis
func (engine *Engine) CreateWebLinks(suffix, separator string) string {
	res := bytes.Buffer{}
	for url, label := range engine.BaseUrls {
		res.WriteString(label)
		res.WriteString(` : `)
		res.WriteString(`<a href="`)
		res.WriteString(url)
		res.WriteString(suffix)
		res.WriteString(`" >`)
		res.WriteString(url)
		res.WriteString(suffix)
		res.WriteString(`</a>`)
		res.WriteString(separator)
	}
	return res.String()
}

func (engine *Engine) MinifyInfo(f *os.File, size int64) string {
	js_stat, _ := f.Stat()
	js_min := js_stat.Size()
	js_percent := (100 * float64(js_min) / float64(size))
	return F.ToStr(js_percent) + `% from ` + I.ToS(size) + ` to ` + I.ToS(js_min)
}

func (engine *Engine) MinifyCSS(fname string, css *bufio.Writer, min *minify.M) int64 {
	f, err := os.Open(fname)
	L.PanicIf(err, `File doesn't exists: `+fname)
	css_stat, _ := f.Stat()
	size := css_stat.Size()
	if size == 0 {
		fmt.Println(`skipping: ` + fname + ` 0`)
		return 0
	}
	fmt.Println(`minifying: ` + fname + ` ` + I.ToS(size/1024))
	r := bufio.NewReader(f)
	err = min.Minify(`text/css`, css, r)
	L.IsError(err, `Error minifying: `+fname)
	css.Flush()
	f.Close()
	return size
}

func (engine *Engine) MinifyJS(fname string, js *bufio.Writer, min *minify.M) int64 {
	f, err := os.Open(fname)
	L.PanicIf(err, `File doesn't exists: `+fname)
	js_stat, _ := f.Stat()
	size := js_stat.Size()
	if size == 0 {
		fmt.Println(`skipping: ` + fname + ` 0`)
		return 0
	}
	fmt.Println(`minifying: ` + fname + ` ` + I.ToS(size/1024))
	r := bufio.NewReader(f)
	err = min.Minify(`text/js`, js, r)
	if !L.IsError(err, `Error minifying: `+fname) {
		_, err = js.WriteRune(';')
		L.IsError(err, `Error separating: `+fname)
	} else {
		dat, err := ioutil.ReadFile(fname)
		if !L.IsError(err, `Error opening: `+fname) {
			_, err = js.Write(dat)
			L.IsError(err, `Error merging: `+fname)
		}
	}
	js.Flush()
	f.Close()
	return size
}

func (engine *Engine) CreateCacheFile(name string) (*os.File, *bufio.Writer) {
	fname := engine.PublicDir + name
	jsf, err := os.Create(fname)
	L.PanicIf(err, `Failed to create: `+fname)
	js := bufio.NewWriter(jsf)
	return jsf, js
}

func (engine *Engine) LoadLayout() *Z.TemplateChain {
	if engine.ViewLayout == nil || engine.DebugMode {
		layout, _ := Z.ParseFile(engine.DebugMode, engine.DebugMode, engine.ViewDir+`layout.html`)
		engine.ViewLayout = layout
	}
	return engine.ViewLayout
}

// register post and get
func (engine *Engine) REGISTER(url string, get_action Action, post_action Action) {
	// url += `/*all`
	engine.GET(url, get_action)
	engine.POST(url, post_action)
}

// Router is a http.Handler which can be used to dispatch requests to different
// handler functions via configurable routes
type Router struct {
	trees map[string]*node

	// Function to handle panics recovered from http handlers.
	// It should be used to generate a error page and return the http error code
	// 500 (Internal Server Error).
	// The handler can be used to keep your server from crashing because of
	// unrecovered panics.
	PanicHandler func(http.ResponseWriter, *http.Request, interface{})
}

// GET is a shortcut for router.Handle(`GET`, path, handle)
func (r *Router) GET(path string, action Action) {
	r.Handle(`GET`, path, action)
}

// POST is a shortcut for router.Handle(`POST`, path, handle)
func (r *Router) POST(path string, action Action) {
	r.Handle(`POST`, path, action)
}

// Handle registers a new request handle with the given path and method.
//
// For GET, POST requests the respective shortcut
// functions can be used.
//
// This function is intended for bulk loading and to allow the usage of less
// frequently used, non-standardized or custom methods (e.g. for internal
// communication with a proxy).
func (r *Router) Handle(method, path string, action Action) {
	if path[0] != '/' {
		panic(`path must begin with '/'`)
	}

	if r.trees == nil {
		r.trees = make(map[string]*node)
	}

	root := r.trees[method]
	if root == nil {
		root = new(node)
		r.trees[method] = root
	}

	root.addRoute(path, action)
}

// attach a middleware on non-static files
func (engine *Engine) Use(m Action) {
	engine.Filters = append(engine.Filters, m)
}

// ServeHTTP makes the router implement the http.Handler interface.
func (engine *Engine) ServeHTTP(rw http.ResponseWriter, req *http.Request) {

	//if engine.DebugMode {
	//	fmt.Print(`.`)
	//}
	ctx := NewContext(rw, req, engine)
	if S.Contains(ctx.Request.URL.String(), `/../../`) {
		ctx.Finish()
	}
	var handler Action

	if root := engine.trees[req.Method]; root != nil {
		path := req.URL.Path
		if action, ps, tsr := root.getValue(path); action != nil {
			ctx.Params = ps
			handler = action
		} else if req.Method != `CONNECT` && path != `/` {
			code := 301 // Permanent redirect, request with GET method
			if req.Method != `GET` {
				// Temporary redirect, request with same method
				// As of Go 1.3, Go does not support status code 308.
				code = 307
			}

			if tsr {
				if len(path) > 1 && path[len(path)-1] == '/' {
					req.URL.Path = path[:len(path)-1]
				} else {
					req.URL.Path = path + `/`
				}
				http.Redirect(rw, req, req.URL.String(), code)
				return
			}

			// Try to fix the request path
			fixedPath, found := root.findCaseInsensitivePath(httprouter.CleanPath(path))
			if found {
				req.URL.Path = string(fixedPath)
				http.Redirect(rw, req, req.URL.String(), code)
				return
			}
		}
	}

	if handler == nil {
		handler = ServeStatic
	} else {
		ctx.Actions = append(ctx.Actions, engine.Filters...)
	}
	ctx.Actions = append(ctx.Actions, handler)
	if req.Method == `GET` {
		ctx.Actions = append(ctx.Actions, engine.pageNotFound)
	} else {
		ctx.NoLayout = true // POST by default have no layout
		ctx.Actions = append(ctx.Actions, engine.ajaxNotFound)
	}

	if engine.MultiApp {
		LoadSession(ctx, ctx.Params.GetInt(0))
	} else {
		LoadSession(ctx, 0)
	}
	ctx.Next()(ctx)
	ctx.Finish()
}

// minify assets
func (engine *Engine) MinifyAssets() {
	res := bytes.Buffer{}
	res_opt := M.SX{}
	// if engine.DebugMode || engine.DisableMinifer {
	// create css-link and scripts
	for _, ext := range engine.Assets {
		// JS/CSS 		= { 0=>'type', 1=>'file_uri relative to public folder', 2=>'[optional] version', 3=>NOT USED}
		// OPT JS/CSS 	= { 0=>'type', 1=>'asset_identifier', 2=>'file_uri relative to public folder', 3=>'[optional] version'}
		asset_type := ext[0]
		asset_is_opt := (strings.HasPrefix(asset_type, `opt-`))
		asset_fileuri := ext[1]
		asset_ver := ext[2]
		if asset_is_opt {
			asset_fileuri = ext[2]
			asset_ver = ext[3]
		}
		if !strings.HasPrefix(asset_fileuri, `/`) { // add slash
			asset_fileuri = `/` + asset_fileuri
		}
		if asset_ver != `` { // append version
			asset_fileuri = asset_fileuri + `?v=` + asset_ver
		}
		switch asset_type {
		case `js`:
			str := `<script type='text/javascript' src='` + asset_fileuri + `'></script>` + "\n"
			res.WriteString(str)
		case `css`:
			str := `<link type='text/css' rel='stylesheet' href='` + asset_fileuri + `'/>` + "\n"
			res.WriteString(str)
		case `opt-js`:
			res_opt[`use_js__`+ext[1]] = `<script type='text/javascript' src='` + asset_fileuri + `'></script>`
		case `opt-css`:
			res_opt[`use_css__`+ext[1]] = `<link type='text/css' rel='stylesheet' href='` + asset_fileuri + `'/>`
		default:
			L.IsError(fmt.Errorf(`Unknown resource format %v`, ext), ``)
		}
		//res.WriteString(str)
	}
	// TODO: bugfix this *__*)
	//} else {
	//	// bundle and minify for production
	//	// TODO: only bundle those who are not easily changed
	//	ms := engine.MinifierSuffix
	//	min := minify.New()
	//	min.AddFunc(`text/css`, css.Minify)
	//	min.AddFunc(`text/js`, js.Minify)
	//	min2 := minify.New()
	//	min2.AddFunc(`text/css`, css.Minify)
	//	min2.AddFunc(`text/js`, js.Minify)
	//	jsf, js := engine.CreateCacheFile(`js/all` + ms + `.js`)
	//	defer jsf.Close()
	//	libjsf, libjs := engine.CreateCacheFile(`js/lib` + ms + `.js`)
	//	defer libjsf.Close()
	//	cssf, css := engine.CreateCacheFile(`css/all` + ms + `.css`)
	//	defer cssf.Close()
	//	libcssf, libcss := engine.CreateCacheFile(`css/lib` + ms + `.css`)
	//	defer libcssf.Close()
	//
	//	js_total, css_total, libjs_total, libcss_total := int64(0), int64(0), int64(0), int64(0)
	//	for _, ext := range engine.Assets {
	//		fname := engine.PublicDir
	//		switch ext[0] {
	//		case `js`:
	//			fname += `js/` + ext[1] + `.js`
	//			libjs_total += engine.MinifyJS(fname, libjs, min)
	//		case `/js`:
	//			fname += ext[1] + `.js`
	//			js_total += engine.MinifyJS(fname, js, min2)
	//		case `css`:
	//			fname += `css/` + ext[1] + `.css`
	//			libcss_total += engine.MinifyCSS(fname, libcss, min)
	//		case `/css`:
	//			fname += ext[1] + `.css`
	//			css_total += engine.MinifyCSS(fname, css, min2)
	//		default:
	//			L.IsError(fmt.Errorf(`Unknown resource format %s: %s`, ext[0]), ext[1])
	//		}
	//	}
	//	js.Flush()
	//	css.Flush()
	//	libjs.Flush()
	//	libcss.Flush()
	//	L.Describe(`Minification result:`,
	//		`LIBJS  : `+engine.MinifyInfo(libjsf, libjs_total),
	//		`JS     : `+engine.MinifyInfo(jsf, js_total),
	//		`LIBCSS : `+engine.MinifyInfo(libcssf, libcss_total),
	//		`CSS    : `+engine.MinifyInfo(cssf, css_total),
	//	)
	//	str := `<link type='text/css' rel='stylesheet' href='/css/lib` + ms + `.css' />` + "\n	" +
	//		`<link type='text/css' rel='stylesheet' href='/css/all` + ms + `.css' />` + "\n	" +
	//		`<script type='text/javascript' src='/js/lib` + ms + `.js' ></script>` + "\n	" +
	//		`<script type='text/javascript' src='/js/all` + ms + `.js' ></script>` + "\n	"
	//	res.WriteString(str)
	//}
	engine.ViewResources = string(res.Bytes())
	engine.OptionalAssets = res_opt
}

// start the server
func (engine *Engine) StartServer(addressPort string) {
	engine.MinifyAssets()
	msg := `[DEVELOPMENT]`
	if !engine.DebugMode {
		msg = `[PRODUCTION]`
	}
	msg = `Starting ` + engine.Name + ` ` + msg + ` server on ` + addressPort
	L.TimeTrack(START_TIME, msg)
	err := http.ListenAndServe(addressPort, engine)
	L.IsError(err, `Failed to listen on `+addressPort)
}

// SendBCC Wrapper
func (engine *Engine) SyncSendMailAttach(mail_id string, bcc []string, subject string, message string, file []string) string {
	mailer := engine.MailAccounts[mail_id]
	if mailer == nil {
		return `Internal Server Error: Invalid mailer ID: ` + mail_id
	}
	return mailer.SendSyncAttachBCC(bcc, subject, message, file)
}

// async SendBCC, ignores error
func (engine *Engine) SendMailAttach(mail_id string, bcc []string, subject string, message string, file []string) {
	mailer := engine.MailAccounts[mail_id]
	if mailer == nil {
		L.Describe(`Internal Server Error: Invalid mailer ID: ` + mail_id)
	}
	mailer.SendAttachBCC(bcc, subject, message, file)
}

// async SendBCC, with callback
func (engine *Engine) SendMailAttachWithCallback(mail_id string, to, bcc, reply_to []string, subject string, message string, file []string, fn func(int64, string), id int64) {
	mailer := engine.MailAccounts[mail_id]
	if mailer == nil {
		L.Describe(`Internal Server Error: Invalid mailer ID: ` + mail_id)
	}
	mailer.SendAttachBCCWithCallback(to, bcc, reply_to, subject, message, file, fn, id)
}

// SendBCC Wrapper
func (engine *Engine) SyncSendMail(mail_id string, bcc []string, subject string, message string) string {
	mailer := engine.MailAccounts[mail_id]
	if mailer == nil {
		return `Internal Server Error: Invalid mailer ID: ` + mail_id
	}
	return mailer.SendSyncBCC(bcc, subject, message)
}

// async SendBCC, ignores error
func (engine *Engine) SendMail(mail_id string, bcc []string, subject string, message string) {
	mailer := engine.MailAccounts[mail_id]
	if mailer == nil {
		L.Describe(`Internal Server Error: Invalid mailer ID: ` + mail_id)
	}
	mailer.SendBCC(bcc, subject, message)
}

func (engine *Engine) SendDebugMail(message string) {
	mail_id := `debug`
	mailer := engine.MailAccounts[mail_id]
	if mailer == nil {
		return
	}
	message = `<pre>` + S.Replace(message, "\n", `<br/>`) + `</pre>`
	subject := engine.Name
	if engine.DebugMode {
		subject += ` DEBUG ` + T.DateStr()
	}
	mailer.SendBCC([]string{engine.MailAccounts[mail_id].From()}, `Internal Server Error: `+subject, message)
}
