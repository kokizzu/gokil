package main

import (
	"fmt"
	"github.com/pkg/profile"
	"gitlab.com/kokizzu/gokil/D"
	"gitlab.com/kokizzu/gokil/F"
	"gitlab.com/kokizzu/gokil/K"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/W"
	"log"
	"os"
	"runtime"
	"strings"
	"time"
)

var VERSION string
var DEBUG_MODE = (VERSION == ``)
var LISTEN_ADDR = `:8089`
var ROOT_DIR string
var BASE_URLS M.SS

const PROJECT_NAME = `Benchmark`
const DOMAIN = `localhost`

/*
 * // documentation version
 * {`type`, `filename`},
 * {`js`, `jquery`}, // should be on public/js/jquery.js
 * {`js`, `bootstrap`}, // should be on public/js/bootstrap.js
 * {`css`, `bootstrap`}, // should be on public/css/bootstrap.css
 */
var ASSETS = [][2]string{}

func Test(ctx *W.Context) {
	ctx.NoLayout = true
	ctx.Render(`test2`, M.SX{
		`name`:       ctx.Params.GetStr(0),
		`surti`:      `tejo`,
		`nilai_awal`: {a: 1, b: 2, c: 3},
	})

	//id := ctx.Params.GetInt(0) // $_POST['test']
	//nama := ctx.Params.GetStr(1)
	//ctx.Posts().GetStr(`tejo`) // whatever
	///*
	//<form method='post'>
	//  <input name='a' value='b' />
	//  <button type='submit' name='tejo' value='whatever' />
	//</form>
	// */
	// $.post('',{a:'c',tejo:'apapun'},function(res){
	// });
}

func HelloWorld_Test(ctx *W.Context) {

}

var HANDLERS = map[string]W.Action{
	`test/:id/:nama`:   Test, // /test/123/kiswono
	`hello_world/test`: HelloWorld_Test,
}

// filter the page that may or may may not be accessed
func AuthFilter(ctx *W.Context) {
	L.Trace()
	handled := false
	//Log.Describe(ctx.Session)
	app_id := ctx.Params.GetInt(0)
	sess_app_id := ctx.Session.AppId
	//L.Describe(app_id, sess_app_id)
	//L.Describe(ctx.Session.RedisKey())
	if ctx.Session.UserId > 0 {
		// logged in
		if sess_app_id > 0 && sess_app_id != app_id {
			ctx.AppendJson(M.SX{`is_success`: false, `errors`: []string{`INVALID SESSION`}})
			handled = true
		}
	}
	ctx.Session.Touch()
	if !handled {
		cpu := K.PercentCPU()
		if cpu > 95.0 {
			ctx.Session.Inc_Global(`throttle_counter`)
			if ctx.Request.Method == `GET` {
				W.ServeError(503, ctx)
			} else {
				ctx.AppendString(`{"errors":["error 503: server is overloaded, please wait for a moment.."]}`)
			}
			fmt.Println(`Throttled: ` + F.ToS(cpu) + ` %`)
			return
		}
		if ctx.Session.UserId > 0 && ctx.Session.Level == nil {
			//ctx.Session.SetLevel(mSubscribers.LoginLevelRaw(ctx.Session.UserId, ctx.IsWebMaster()))
			//L.Describe(`ResetLevel`, ctx.Session.Level, ctx.Session.UserId)
		}
		ctx.Next()(ctx)
	}
}

// initialize loading time
func init() {
	BASE_URLS = M.SS{}
	var err error
	ROOT_DIR, err = os.Getwd() // filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		_, path, _, _ := runtime.Caller(0)
		slash_pos := strings.LastIndex(path, `/`) + 1
		ROOT_DIR = path[:slash_pos]
	} else {
		ROOT_DIR += `/`
	}
	//L.Print(PROJECT_NAME)
	log.Print(`ROOT_DIR : ` + ROOT_DIR)
}

func main() {
	defer profile.Start(profile.CPUProfile).Stop()
	W.NO_IP_CHECK = true
	W.InitSession(2*24*time.Hour, 1*24*time.Hour, 12) // REDIS
	D.InitOfficeMail(`@` + DOMAIN)
	L.Trace()
	runtime.GOMAXPROCS(int(K.NUM_CPU))
	// new mailer
	mailer := map[string]*W.SmtpConfig{}
	// web engine
	D.DEBUG = DEBUG_MODE
	server := W.NewEngine(DEBUG_MODE, true, PROJECT_NAME+VERSION, mailer, mUsers.WEBMASTER_EMAILS, ``, ROOT_DIR, ASSETS, BASE_URLS)

	server.Use(AuthFilter)
	for url, handler := range HANDLERS {
		server.REGISTER(`/`+url, handler, handler)
	}
	L.Describe(server.GlobalInt, server.GlobalStr, `Version: `+S.IfElse(DEBUG_MODE, `DEBUG`, VERSION), ROOT_DIR)

	server.StartServer(LISTEN_ADDR)
}
