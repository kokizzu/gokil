// PUKIS' Time (and Date) Helper
package T

import (
	"encoding/gob"
	"gitlab.com/kokizzu/gokil/F"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"math/rand"
	"time"
)

const TimeFormat = `2006-01-02T15:04:05.999999`
const TimeFormat2 = `2006-01-02 15:04`
const DateTimeFormat = `2006-01-02 15:04:05`
const DateFormat = `2006-01-02`
const FileFormat = `20060102_150405`
const DMYFormat = `2-Jan-2006 15:04:05`

var EPOCH = time.Time{}
var DAY_HASH = map[int64]string{
	0: `Sunday / Minggu`,
	1: `Monday / Senin`,
	2: `Tuesday / Selasa`,
	3: `Wednesday / Rabu`,
	4: `Thursday / Kamis`,
	5: `Friday / Jumat`,
	6: `Saturday / Sabtu`,
}

func init() {
	gob.Register(time.Time{})
}

/*
func main() {
   L.Describe(T.ToS(time.Now()))//output "2016-03-17T10:04:50.6489"
}
*/
func ToS(t time.Time) string {
	if t == EPOCH {
		return ``
	}
	return t.Format(TimeFormat)
}

/*
L.Describe(T.DateToS(time.Now()))// output "2016-03-17"
*/
func DateToS(t time.Time) string {
	if t == EPOCH {
		return ``
	}
	return t.Format(DateFormat)
}

/*
L.Describe(T.NowStr())//output "2016-03-17T10:07:56.418728"
*/
func NowStr() string {
	return ToS(time.Now())
}

/*
L.Describe(T.NowStr2())//output "2016-03-17 10:08"
*/
func NowStr2() string {
	return time.Now().Format(TimeFormat2)
}

/*
L.Describe(T.DateStr())//output "2016-03-17"
*/
func DateStr() string {
	return NowStr()[0:10]
}

/*
L.Describe(T.DateTimeStr())//output "2016-03-17 10:11:53"
*/
func DateTimeStr() string {
	return time.Now().Format(DateTimeFormat)
}

//output "2016-03-17 10:11:53"
func DateTimeDMY() string {
	return time.Now().Format(DMYFormat)
}

/*
L.Describe(T.DayInt())//output int64(17)
*/
func DayInt() int64 {
	return int64(time.Now().Day())
}

/*
get current hour
*/
func HourInt() int64 {
	return int64(time.Now().Hour())
}

/*
L.Describe(T.MonthInt())//output int64(3)
*/
func MonthInt() int64 {
	return int64(time.Now().Month())
}

/*
L.Describe(T.YearInt())//output int64(2016)
*/
func YearInt() int64 {
	return int64(time.Now().Year())
}

/*
L.Describe(T.YearDayInt())//output int64(77)
*/
func YearDayInt() int64 {
	return int64(time.Now().YearDay())
}

/*
L.Describe(T.Filename())//output "20160317_102543"
*/
func Filename() string {
	return time.Now().Format(FileFormat)
}

/*
func main(){
    T.Sleep(2)
    L.Describe()
}
*/
func Sleep(ns time.Duration) {
	time.Sleep(ns)
}

/*
func main(){
    T.RandomSleep()
    L.Describe()
}
*/
// random 0.4-2 sec sleep
func RandomSleep() {
	dur := rand.Int63()%(1600*1000*1000) + (400 * 1000 * 1000)
	time.Sleep(time.Duration(dur))
}

/*
func main(){
    L.Describe(T.ToI(`02:10`))//output int64(130)
}
*/
// HH:MM to int64
func ToI(hhmm string) int64 {
	res := int64(0)
	if L.CheckIf(len(hhmm) != 5, `Invalid HH:MM length: `+hhmm) {
		return res
	}
	if L.CheckIf(hhmm[2] != ':', `Invalid HH:MM format: `+hhmm) {
		return res
	}
	res += int64(hhmm[0]-'0') * 10 * 60
	res += int64(hhmm[1]-'0') * 60
	res += int64(hhmm[3]-'0') * 10
	res += int64(hhmm[4] - '0')
	return res
}

/*
func main(){
    T.Track(func(){
        x:=0
        for i:=0 ; i>=100000000000 ; i++{ //output "done in 0.00s"
            x+=i
        }
    })
    L.Describe()
}
*/
// measure elapsed time in nanosec
func Track(fun func()) time.Duration {
	start := time.Now()
	fun()
	elapsed := time.Since(start)
	L.Describe(`done in ` + F.ToStr(elapsed.Seconds()) + `s`)
	return elapsed
}

/*
func main(){
    t1:="1992-03-23"
    t2:="2016-03-17"
    t11, _:=time.Parse(t1,T.DateFormat)
    t22, _:=time.Parse(t2,T.DateFormat)
    L.Describe(T.IsValidTimeRange(t11,t22,time.Now()))//output bool(false)
}
*/

// check time is in DateTime Range
func IsValidTimeRange(start, end, check time.Time) bool {
	res := check.After(start) && check.Before(end)
	return res
}

/*
L.Describe(T.Age(time.Now()))//output float64(0)
*/
// check Age
func Age(birthdate time.Time) float64 {
	return float64(time.Now().Sub(birthdate)/time.Hour) / 24 / 365.25
}

// get current unix nano
func UnixNano() int64 {
	return time.Now().UnixNano()
}

func UnixNanoAfter(d time.Duration) int64 {
	return time.Now().Add(d).UnixNano()
}

// get current unix
func Epoch() int64 {
	return time.Now().Unix()
}

// get current unix
func EpochStr() string {
	return I.ToS(time.Now().Unix())
}

// get current unix time added with a duration
func EpochAfter(d time.Duration) int64 {
	return time.Now().Add(d).Unix()
}
