// PUKIS' String Helper
package S

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"gitlab.com/kokizzu/gokil/B"
	"gitlab.com/kokizzu/gokil/C"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/T"
	"golang.org/x/crypto/bcrypt"
	"hash/crc64"
	"hash/fnv"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var re_non_phone *regexp.Regexp

func init() {
	re_non_phone = regexp.MustCompile(`[^-+\d,]+`)
}

/* Return "yes" string (second arg) if the condition (first arg) is true,
   Return "" (empty string) if the condition (first arg) is false

   usage example:
func main() {
    fmt.Println(S.If(true,`benar`))   // output "benar"
    fmt.Println(S.If(false,`benar`))  // output ""
}
*/
func If(b bool, yes string) string {
	if b {
		return yes
	}
	return ``
}

/* return "yes" string (second arg) if the condition (first arg) is true,
   return "no" string (third arg) if the condition (first arg) is false

   usage example:
func main() {
    fmt.Println(S.IfElse(true,`benar`,`salah`))   //output "benar"
    fmt.Println(S.IfElse(false,`benar`,`salah`))  //output "salah"
}
*/
func IfElse(b bool, yes, no string) string {
	if b {
		return yes
	}
	return no
}

/* Change string type to int64 type.
   Input : string type
   Output: int64 type

   usage example:
func main() {
    // Mengubah string menjadi int64 (angka bukan decimal), agar
    // bisa ditambahkan dengan angka bilangan bulat
    // Change string type to int64 type (not decimal number), so
    // it can be added with another integer number
    fmt.Println(S.ToI(`123`) + 50) //output 173
}
*/
// string to int64
func ToI(str string) int64 {
	val, err := strconv.ParseInt(str, 10, 64)
	L.PanicIf(err, str)
	return val
}

// Convert to integer with check
func ToInteger(str string) (int64, bool) {
	res, err := strconv.ParseInt(str, 10, 64)
	return res, err == nil
}

/* Change string type to float64 type.
   Input : string type
   Output: float64 type

   usage example:
func main() {
    // Mengubah string menjadi float (angka decimal)
    // Change string type into float type (decimal number)
    fmt.Println(S.ToF(`123.45`) + 50) // output 173.45
}
*/
// string to float64
func ToF(str string) float64 {
	val, err := strconv.ParseFloat(str, 64)
	L.PanicIf(err, str)
	return val
}

// Convert to float with check
func ToFloat(str string) (float64, bool) {
	res, err := strconv.ParseFloat(str, 64)
	return res, err == nil
}

/* Validating string from input,
   then return bool type true if the string input is following the time format
   and return bool type false if the string input is not following the time format

   usage example:
func main() {
    // Format tanggal dan jam :YYYY-MM-DDTJJ:MM:DD
    // Date and time format :YYYY-MM-DDTJJ:MM:DD
    fmt.Println(S.IsTime(`2016-03-17T17:00:34`)) // output True
}
*/
// validate time
func IsTime(str string) bool {
	_, err := time.Parse(T.TimeFormat, str)
	return err == nil
}

/* Convert JSON object or JavaScript map in string type to map in Go

   usage example:
func main() {
    // Merubah JSON object atau map JavaScript menjadi map di Go
    // Converting JSON object or JavaScript map to map in Go
    json_str := `{"test":123,"bla":[1,2,3,4]}`
    map1 := S.JsonToMap(json_str)
    fmt.Printf("%# v\n", map1 )        // output map[string]interface {}{"test": 123, "bla":[]interface {}{ 1,  2,  3,  4}}
    fmt.Printf("%# v\n", map1[`bla`] ) // output []interface {}{ 1,  2,  3,  4}
}
*/
// to json
func JsonToMap(str string) (res map[string]interface{}) {
	res = map[string]interface{}{}
	json.Unmarshal([]byte(str), &res)
	return
}

/*  Convert JSON array JavaScript in string type to array in Go

   usage example:
func main() {
    // Merubah JSON array JavaScript menjadi array di Go
    // Converting JSON array JavaScript to array in Go
    json_arr := `[1,"abc",3,{"test":123}]`
    map1 := S.JsonToArr(json_arr)
    fmt.Printf("%# v\n", map1) // output []interface {}{ 1, "abc",  3, map[string]interface {}{"test": 123}}
}
*/
// to json array
func JsonToArr(str string) (res []interface{}) {
	res = []interface{}{}
	json.Unmarshal([]byte(str), &res)
	return
}

/* Checking whether input string is empty/whitespace or not,
   if it is empty or having only whitespaces, it will return bool True,
   if it is not empty and not having only whitespaces, it will return bool False

   usage example:
func main() {
    fmt.Println(S.IsEmpty(``)) // output True
}
*/
// is empty (or just whitespace)
func IsEmpty(str string) bool {
	if str == `` {
		return true
	}
	return Trim(str) == ``
}

/* Checking whether input string  is empty or not,
   if it is not empty, it will return bool True,
   if it is empty, it will return bool False

   usage example:
func main() {
    fmt.Println(S.NotEmpty(`bisa`)) // output True
}
*/
// is not empty
func NotEmpty(str string) bool {
	return !IsEmpty(str)
}

/* Split a string (first arg) by characters (second arg)
   into array of strings (output).
   It will returns empty string if consecutive characters (second arg)
   is found in a string (first arg)

   usage example:
func main() {
    L.Describe(S.Split(`biiiissssa`,`i`)) // output []string{"b", "", "", "", "ssssa"}
}
*/
// split by characters
func Split(str, sep string) []string {
	return strings.Split(str, sep)
}

// split each N characters, utf-safe
func SplitN(str string, n int) []string {
	if len(str) < n {
		return []string{str}
	}
	sub := ``
	subs := []string{}
	runes := bytes.Runes([]byte(str))
	l := len(runes)
	for i, r := range runes {
		sub = sub + string(r)
		if (i+1)%n == 0 {
			subs = append(subs, sub)
			sub = ``
		} else if (i + 1) == l {
			subs = append(subs, sub)
		}
	}
	return subs
}

/* Split a string (first arg) by characters (second arg)
   into array of strings (output).
   It will not returns empty string, even if consecutive characters (second arg)
   is found in a string (first arg)

   usage example:
func main() {
   L.Describe(S.CompactSplit(`biiiissssa`,`i`))//output  []string{"b", "ssssa"}
}
*/
// just like strings.Split but removes empty strings
func CompactSplit(str, sep string) []string {
	res := Split(str, sep)
	res2 := []string{}
	for _, v := range res {
		if v != `` {
			res2 = append(res2, v)
		}
	}
	return res2
}

/* Sepearting sentence that contain enter (if using backslash ``)
   or "\n" (if using double apostrophe "") followed with splitter (,)
   and other splitters.
   Memisahkan kalimat dengan enter (jika menggunakan backslash ``)
   atau "\n" (jika menggunakan petik dua "") diikuti dengan spliter (,)
   dan spliter lain-lainnya.

func main() {
    lines1 := "a,b,c\n1,2,345\nx,y,z"
    L.Describe(S.ToMatrix(lines1,","))
    // output [][]string{ {"a", "b", "c"}, {"1", "2", "345"}, {"x", "y", "z"}, }

    lines2 := `a,b,c
1,2,345
x,y,z`
    L.Describe(S.ToMatrix(lines2,","))
    // output [][]string{ {"a", "b", "c"}, {"1", "2", "345"}, {"x", "y", "z"}, }
}
*/
// convert tsv to matrix of string
func ToMatrix(str, sep string) [][]string {
	res := [][]string{}
	rows := Split(str, "\n")
	for _, row := range rows {
		res = append(res, strings.Split(row, sep))
	}
	return res
}

//
var MONTHS = map[string]int64{
	`jan`:       1,
	`januari`:   1,
	`feb`:       2,
	`februari`:  2,
	`mar`:       3,
	`maret`:     3,
	`apr`:       4,
	`april`:     4,
	`may`:       5,
	`mei`:       5,
	`jun`:       6,
	`juni`:      6,
	`jul`:       7,
	`juli`:      7,
	`aug`:       8,
	`agustus`:   8,
	`sep`:       9,
	`september`: 9,
	`oct`:       10,
	`oktober`:   10,
	`nov`:       11,
	`november`:  11,
	`dec`:       12,
	`desember`:  12,
	`01`:        1,
	`02`:        2,
	`03`:        3,
	`04`:        4,
	`05`:        5,
	`06`:        6,
	`07`:        7,
	`08`:        8,
	`09`:        9,
	`10`:        10,
	`11`:        11,
	`12`:        12,
	`1`:         1,
	`2`:         2,
	`3`:         3,
	`4`:         4,
	`5`:         5,
	`6`:         6,
	`7`:         7,
	`8`:         8,
	`9`:         9,
}

/* Converting input string with month format (mmm/month/xx/x)
   to int64 that represent the number of the month (1, 2, 3,.. , 11, 12)

   usage example:
func main() {
   fmt.Println(S.MonthToI(`jan`)) // output 1
   fmt.Println(S.MonthToI(`02`))  // output 2
}
*/
func MonthToI(mm string) int64 {
	m := MONTHS[mm]
	L.CheckIf(m == 0, `invalid month string:`, mm)
	return m
}

/* Adding string "0" in front of string (first arg) until
   the length (number of character) of the string (first arg)
   reach a certain integer number (second arg)

   usage example:
func main() {
    // Untuk menambah angka 00 didepan angka tapi bertipe string
    // To add 00 number in front of number with string type
    fmt.Println(S.Zeros(`1`,5)) // output 00001
}
*/
func Zeros(str string, length int) string {
	for len(str) < length {
		str = `0` + str
	}
	return str
}

/* Basically, it is simillar to Zeros function, but using
   int64 type as first argument instead of string.

   It converts int64 (first arg) to string, then
   adding string "0" in front of the converted string until
   the length (number of character) of the converted string
   reach a certain integer number (second arg).

   usage example:
func main() {
    // Untuk menambahkan angka 00 didepan angka bertipe int64
    // To add 00 number in front of number with int64 type
    fmt.Println(S.ZerosI(123,5)) // output "00123"
    fmt.Println(S.ZerosI(123,4)) // output "0123"
}
*/
//
func ZerosI(n int64, length int) string {
	return Zeros(I.ToS(n), length)
}

/* Converting string type into time format.

   usage example:
func main() {
    // Untuk mengkonversikan dari string ke format time.
    // To convert string type into time format.
    L.Describe(S.ToDate(`2016-03-11`))
    // output
    // time.Time{
    //     sec:  63593251200,
    //     nsec: 0,
    //     loc:  &time.Location{
    //         name:       "UTC",
    //         zone:       nil,
    //         tx:         nil,
    //         cacheStart: 0,
    //         cacheEnd:   0,
    //         cacheZone:  (*time.zone)(nil),
    //     },
    // }
}
*/
// convert to date, return
func ToDate(str string) time.Time {
	res, err := time.Parse(`2006-01-02`, str)
	if err != nil {
		L.Describe(err)
		return time.Now()
	}
	return res
}

/* Converting string type date ("dd-mm-yy" or "dd mm yy") to
   another string type date ("yyyy-mm-dd")

   usage example:
func main() {
    // Mengkoversi string date ("dd-mm-yy" atau "dd mm yy") ke
    // string ("yyyy-mm-dd")
    // Converting string date ("dd-mm-yy" or "dd mm yy") to
    // string type ("yyyy-mm-dd")
    L.Describe(S.ToDateStr(`23-12-92`)) // output "1992-12-23"
}
*/
func ToDateStr(str string) string {
	str = strings.TrimSpace(str)
	ymd := strings.FieldsFunc(str, func(ch rune) bool {
		return ch == ' ' || ch == '-'
	})
	if len(ymd) != 3 {
		return str
	}
	/*if Log.CheckIf(len(ymd) != 3, `Invalid date string format`, str) {
		return str
	}*/
	dd := ToI(ymd[0])
	mo := ToLower(ymd[1])
	yy := ToI(ymd[2])
	if yy < int64(time.Now().Year()%1000+10) {
		// less than (xx+10) in 20xx
		yy += 2000
	} else {
		yy += 1900
	}
	if L.CheckIf(dd < 1 || dd > 31, `Invalid day format`, str) {
		return str
	}
	mm := MONTHS[mo]
	if L.CheckIf(mm == 0, `Invalid month format`, str) {
		return str
	}
	return ZerosI(yy, 4) + `-` + ZerosI(mm, 2) + `-` + ZerosI(dd, 2)
}

/* Checking whether the input string is empty/contain date or not.
   If empty or contain date, it return True, else it return False

   usage example:
func main() {
    L.Describe(S.IsDateOrEmpty(`1992-03-23`)) // output bool(true)
    L.Describe(S.IsDateOrEmpty(``))           // output bool(true)
}
*/
func IsDateOrEmpty(date string) bool {
	if date == `` {
		return true
	}
	return IsDate(date)
}

/* Checking whether the input string is containing a date.
   If it containing a date, it return True, else it return False

   usage example:
func main() {
    L.Describe(S.IsDate(`1992-03-23`)) // output bool(true)
    L.Describe(S.IsDate(``))           // output bool(false)
}
*/
func IsDate(date string) bool {
	if len(date) == 10 {
		return date[4] == '-' && date[7] == '-' &&
			C.IsDigit(date[0]) && C.IsDigit(date[1]) && C.IsDigit(date[2]) && C.IsDigit(date[3]) &&
			C.IsDigit(date[5]) && C.IsDigit(date[6]) &&
			C.IsDigit(date[8]) && C.IsDigit(date[9])
	}
	return false
}

func IsDateTimeHourMinute(datetime string) bool {
	if len(datetime) == 16 {
		return datetime[4] == '-' && datetime[7] == '-' && datetime[10] == ' ' && datetime[13] == ':' &&
			C.IsDigit(datetime[0]) && C.IsDigit(datetime[1]) && C.IsDigit(datetime[2]) && C.IsDigit(datetime[3]) &&
			C.IsDigit(datetime[5]) && C.IsDigit(datetime[6]) && C.IsDigit(datetime[8]) && C.IsDigit(datetime[9]) &&
			C.IsDigit(datetime[11]) && C.IsDigit(datetime[12]) && C.IsDigit(datetime[14]) && C.IsDigit(datetime[15])
	}
	return false
}

/* Checking whether the input string (first arg) starts with
   a certain character (second arg) or not.

   usage example:
func main() {
    // Untuk melihat nilai huruf paling awal pada string
    // To check the first character of a string
    L.Describe(S.StartWith(`adakah`,`a`)) // output bool(true)
    L.Describe(S.StartWith(`adakah`,`b`)) // output bool(false)
}
*/
func StartWith(str, prefix string) bool {
	return strings.HasPrefix(str, prefix)
}

/* Checking whether the input string (first arg) ends with
   a certain character (second arg) or not.

   usage example:
func main() {
    // Untuk melihat nilai huruf paling akhir pada string
    // To check the last character of a string
    L.Describe(S.EndWith(`adakah`,`h`)) // output bool(true)
    L.Describe(S.EndWith(`adakah`,`a`)) // output bool(false)
}
*/
func EndWith(str, suffix string) bool {
	return strings.HasSuffix(str, suffix)
}

/* Checking whether the input string (first arg) contains
   a certain sub string (second arg) or not.

   usage example:
func main() {
    L.Describe(S.Contains(`komputer`,`om`)) output bool(true)
}
*/
func Contains(str, substr string) bool {
	return strings.Contains(str, substr)
}

/* Counting how many specific character (first arg) that
   the string (second arg) contains

   usage example:
func main() {
    // Untuk menghitung jumlah huruf yang dipanggil.
    // Used to count the number of character that have been called.
    L.Describe(S.Count(`komputeer`,`e`))// output int(2)
}
*/
func Count(str, substr string) int {
	return strings.Count(str, substr)
}

/*
func main() {
    // Menghapus spasi di depan dan dibelakang
    // To delete spaces in front and end of a string
    line:=` withtrim:  `
    L.Describe(S.Trim(line)) // output "withtrim:"
}
*/
func Trim(str string) string {
	return strings.TrimSpace(str)
}

/*
func main() {
    // Menampilkan string yang berupa angka dan operator seperti +,-
    // Showing only string that in form of number and operator such as +,-
    L.Describe(S.PhoneOf(`123abcdef-+.`)) // output "123-+"
}
*/
func PhoneOf(str string) string {
	return re_non_phone.ReplaceAllString(str, ``)
}

/*
func main() {
    // Merubah huruf pada string sesuai dengan yang diinginkan
    // Replace character in the string based on the input
    L.Describe(S.Replace(`bisa`,`is`,`us`)) // output "busa"
}
*/
func Replace(haystack, needle, gold string) string {
	return strings.Replace(haystack, needle, gold, -1)
}

/*
func main() {
    // Merubah huruf kapital menjadi huruf kecil
    // Change the character in string to lowercase
    L.Describe(S.ToLower(`BIsa`)) // output "bisa"
}
*/
func ToLower(str string) string {
	return strings.ToLower(str)
}

/*
func main() {
    // Merubah huruf kecil menjadi huruf kapital
    // Change the character in string to uppercase
    L.Describe(S.ToUpper(`bisa`)) // output "BISA"
}
*/
func ToUpper(str string) string {
	return strings.ToUpper(str)
}

/*
func main() {
    // Merubah huruf pertama pada setiap kata menjadi kapital
    // Change first letter for every word to uppercase
    L.Describe(S.ToTitle(`Disa dasi`)) // output "Disa Dasi"
}
*/
func ToTitle(str string) string {
	return strings.Title(str)
}

/*
func main() {
    // Menampilkan string sebanyak jumlah angka yang dimasukkan
    // To show the string as much as the inputted number
    L.Describe(S.Repeat(`bisa`,3)) // output "bisabisabisa"
}
*/
func Repeat(str string, num int) string {
	return strings.Repeat(str, num)
}

/*
func main() {
    // Memanggil string sesuai dengan jumlah huruf ke-berapa
    // yang diinginkan mulai dari depan.
    // To return string correspond to the inputted number
    // started from the first character.
    L.Describe(S.Left(`Disa dasi`,7)) // output "Disa da"
}
*/
// take at best first n characters, not for unicode
func Left(str string, num int) string {
	if num <= 0 {
		return ``
	}
	if num > len(str) {
		num = len(str)
	}
	return str[:num]
}

/*
func main() {
    // Memanggil string sesuai dengan jumlah huruf ke-berapa
    // yang diinginkan mulai dari akhir.
    // To return string correspond to the inputted number
    // started from the last character.
    L.Describe(S.Right(`Disa dasi`,7)) // output "a dasi"
}
*/
// take at most last n characters, not for unicode
func Right(str string, num int) string {
	if num <= 0 {
		return ``
	}
	max := len(str)
	if num > max {
		num = max
	}
	num = max - num
	return str[num:]
}

/*
func main() {
    // Mengubah beberapa karakter cross-site scripting (XSS) dari input string
    // Convert some cross-site scripting (XSS) character from the inputted string
    L.Describe(S.XSS(`<Disa> `)) // output "&lt;Disa&gt;"
}
*/
func XSS(str string) string {
	str = Trim(str)
	str = strings.Replace(str, `<`, `&lt;`, -1)
	str = strings.Replace(str, `>`, `&gt;`, -1)
	str = strings.Replace(str, `'`, `&apos;`, -1)
	str = strings.Replace(str, `"`, `&quot;`, -1)
	str = strings.Replace(str, `\`, `/`, -1)
	//str = strings.Replace(str, "\n", `/n`, -1)
	//str = strings.Replace(str, "\t", `/t`, -1)
	return str
}

/*
func main() {
    // Mengubah karakter apapun menjadi uint64
    // Change any character into unit64
    L.Describe(S.CRC64(`KOOOMMMMMiiiiiiikkkk`)) // output uint64(0xe83f93d1dfca5b38)
}
*/
func CRC64(str string) uint64 {
	table := crc64.MakeTable(crc64.ECMA)
	return crc64.Checksum([]byte(str), table)
}

/*
func main() {
    // Mengubah karakter apapun menjadi base64
    // Change any character into base64
    L.Describe(S.ToBase64(`admin`)) // output "YWRtaW4="
}
*/
// encode string to base64
func ToBase64(val string) string {
	buf := []byte(val)
	return base64.StdEncoding.EncodeToString(buf)
}

/*
func main() {
    // Mengganti spasi menjadi uderscore (_)
    // Replace spaces into underscore (_)
    L.Describe(S.CleanFilename(`admin  buku`)) // output "admin_buku"
}
*/
// replace invalid character with space, remove multiple space for file name
func CleanFilename(name string) string {
	val := []byte(name)
	res := []byte{}
	last_space := true
	for _, ch := range val {
		if C.IsValidFilename(ch) {
			if ch == ' ' && last_space {
				continue
			}
			nch := ch
			if ch == ' ' {
				nch = '_'
			}
			res = append(res, nch)
			last_space = (ch == ' ')
		} else if ch == '/' {
			res = append(res, '_')
		} else {
			last_space = true
		}
	}
	return Trim(string(res))
}

// replace everything to underscore except number string
func CleanIndexName(name string) string {
	val := []byte(name)
	res := []byte{}
	underscore := 0
	for _, ch := range val {
		if C.IsIdent(ch) {
			res = append(res, ch)
			underscore = 0
		} else if underscore < 2 && ch != ')' {
			res = append(res, '_')
			underscore += 1
		}
	}
	for len(res) > 0 && res[len(res)-1] == '_' {
		res = res[:len(res)-1]
	}
	return string(res)
}

/*
func main() {
    // Change array of string into array of int64
    line:=[]string {"12","34"}
    L.Describe(S.ArrToIntArr(line) ) // output []int64{12, 34}
}
*/
func ArrToIntArr(any_arr []string) []int64 {
	res := []int64{}
	for _, val := range any_arr {
		res = append(res, ToI(val))
	}
	return res
}

/*
func main() {
    // Untuk mengecek apakah string tersebut mengandung angka/digit atau tidak.
    // Used to check ehther a string contains number/digit or not.
    L.Describe(S.ContainsDigit(`123`))  // output bool(true)
    L.Describe(S.ContainsDigit(`buku`)) // output bool(false)

}
*/
func ContainsDigit(str string) bool {
	for _, ch := range str {
		if C.IsDigit(byte(ch)) {
			return true
		}
	}
	return false
}

/*
func main()
    // Untuk memvalidasi suatu string yang berisi alamat email.
    // Menghasilkan output berupa alamat email yang benar, jika ditemukan.
    // Used validate a string that contains email address.
    // Return a string of correct email address, if found.
    L.Describe(S.ValidateEmail(`bintang@poltek.com`) ) // output "bintang@poltek.com"
    L.Describe(S.ValidateEmail(`bintang.poltek.com`) ) // output ""
}
*/
func ValidateEmail(str string) string {
	res := strings.Split(str, `@`)
	if len(res) != 2 {
		return ``
	}
	if (strings.Trim(res[0], `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$%&'*+-/=?^_{|}~.`)) == `` {
		if strings.Trim(res[1], `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.`) == `` {
			return str
		}
	}
	return ``
}

/*
func main() {
    // Untuk memvalidasi apakah input string dapat dikonversi ke int atau tidak.
    // To validate whether an input string can be converted to int or not.
    L.Describe(S.IsInt(`123`) )   // output bool(true)
    L.Describe(S.IsInt(`buku`) )  // output bool(false)
    L.Describe(S.IsInt(`123.5`) ) // output bool(false)
}
*/
func IsInt(str string) bool {
	_, err := strconv.ParseInt(str, 10, 64)
	return err == nil
}

/*
func main() {
    // Untuk memvalidasi apakah input string dapat dikonversi ke float atau tidak.
    // To validate whether an input string can be converted to float or not.
    L.Describe(S.IsFloat(`buku`) )  // output bool(false)
    L.Describe(S.IsFloat(`123.5`) ) // output bool(true)
    L.Describe(S.IsFloat(`123`) )   // output bool(true)
}
*/
func IsFloat(str string) bool {
	_, err := strconv.ParseFloat(str, 64)
	return err == nil
}

/* Used to take a sub string from the input.
   Input arg 1: a string that will be processed by the function
         arg 2: an int, the position of starting character of new string
         arg 3: an int, the number of character of new string
   Output     : the new string, a sub string from the inputted string

   usage example:
func main() {
    // Untuk mengambil bagian string dari input
    // Used to take sub string from input
    L.Describe(S.Substr(`buku`,2,1)) // output "k"
    L.Describe(S.Substr(`buku`,2,3)) // output "ku"
    L.Describe(S.Substr(`buku`,4,3)) // output ""
}
*/
func Substr(s string, pos, length int) string {
	runes := []rune(s)
	l := pos + length
	if l > len(runes) {
		l = len(runes)
	}
	return string(runes[pos:l])
}

/*
func main() {
    // Untuk mengubah format waktu/time ke string type ("YYYY-MM-DD HH:SS")
    // To convert time format into string type ("YYYY-MM-DD HH:SS")
    L.Describe(S.StrUTCtoYMDHS(time.Now()) ) // output "2016-03-14 17:33"
}
*/
func StrUTCtoYMDHS(t time.Time) string {
	res := Split(T.ToS(t), `T`)[0] + ` ` + Split(T.ToS(t), `T`)[1]
	res = Substr(res, 0, 16)
	return res
}

/*
func main() {
    // Untuk menambah huruf yang dimasukkan sesuai jumlah angka yang
    // dimasukkan dari sebelah kiri.
    // To add set of character that inputted based on the inputted number
    // in front of the string (from the left).
    L.Describe(S.PadLeft(`coba`,`co`,2)) // output "cocoba"
    L.Describe(S.PadLeft(`coba`,`co`,4)) // output "cococoba"
}
*/
func PadLeft(s string, padStr string, lenStr int) string {
	var padCount int
	padCount = I.Max2(lenStr-len(s), 0)
	return strings.Repeat(padStr, padCount) + s
}

/*
func main() {
    // Untuk menambah huruf yang dimasukkan sesuai jumlah angka yang
    // dimasukan dari sebelah kanan.
    // To add set of character that inputted based on the inputted number
    // in the end of the string (from the left).
    L.Describe(S.PadRight(`coba`,`co`,2)) // output "cobaco"
    L.Describe(S.PadRight(`coba`,`co`,4)) // output "cobacoco"
}
*/
func PadRight(s string, padStr string, lenStr int) string {
	var padCount int
	padCount = I.Max2(lenStr-len(s), 0)
	return s + strings.Repeat(padStr, padCount)
}

/*
func main()
    // Untuk menaruh koma untuk setiap 3 angka dari belakang.
    // Berlaku untuk tipe float64.
    // To add comma for every 3 number/character from the back.
    // Used for float64 type.
    L.Describe(S.Comma(2345))    // output "2,345"
    L.Describe(S.Comma(2345678)) // output "2,345,678"
}
*/
// Comma produces a string form of the given number in base 10 with
// commas after every three orders of magnitude
// e.g. Comma(834142) -> 834,142
func Comma(v int64) string {
	sign := ""
	if v < 0 {
		sign = "-"
		v = 0 - v
	}
	parts := []string{"", "", "", "", "", "", ""}
	j := len(parts) - 1
	for v > 999 {
		parts[j] = strconv.FormatInt(v%1000, 10)
		switch len(parts[j]) {
		case 2:
			parts[j] = "0" + parts[j]
		case 1:
			parts[j] = "00" + parts[j]
		}
		v = v / 1000
		j--
	}
	parts[j] = strconv.Itoa(int(v))
	return sign + strings.Join(parts[j:], ",")
}

/*
func main() {
    // Untuk menaruh koma untuk setiap 3 angka dari belakang.
    // Berlaku untuk tipe float64.
    // To add comma for every 3 number/character from the back.
    // Used for float64 type.
    L.Describe(S.Commaf(23457.23843)) // output "23,457.23843
    L.Describe(S.Commaf(23457238.43)) // output "23,457,238.43"
    L.Describe(S.Commaf(23.45723843)) // output "23.45723843"
}
*/
// Commaf produces a string form of the given number in base 10 with
// commas after every three orders of magnitude
// e.g. Comma(834142.32) -> 834,142.32
func Commaf(v float64) string {
	buf := &bytes.Buffer{}
	if v < 0 {
		buf.Write([]byte{'-'})
		v = 0 - v
	}
	comma := []byte{','}
	parts := strings.Split(strconv.FormatFloat(v, 'f', -1, 64), ".")
	pos := 0
	if len(parts[0])%3 != 0 {
		pos += len(parts[0]) % 3
		buf.WriteString(parts[0][:pos])
		buf.Write(comma)
	}
	for ; pos < len(parts[0]); pos += 3 {
		buf.WriteString(parts[0][pos : pos+3])
		buf.Write(comma)
	}
	buf.Truncate(buf.Len() - 1)
	if len(parts) > 1 {
		buf.Write([]byte{'.'})
		buf.WriteString(parts[1])
	}
	return buf.String()
}

// TODO: create func Mid(str string, pos, num int)
/*
func main() {
     coba := `tes
     123  `
    L.Print(S.ToJs(coba))// output  tes\n     123
}
*/
func ToJs(str string) string {
	return Replace(str, "\n", `\n`)
}

/* Similar with XSS function, but with adding single quote.
func main() {
    L.Print(S.Z(`coba<`))  // output 'coba&lt;'
    L.Print(S.Z(`"coba"`)) // output '&quot;coba&quot;'
}
*/
// replace <, >, ', " and gives single quote
func Z(str string) string {
	str = Trim(str)
	str = Replace(str, `<`, `&lt;`)
	str = Replace(str, `>`, `&gt;`)
	str = Replace(str, `'`, `&apos;`)
	str = Replace(str, `"`, `&quot;`)
	return `'` + str + `'`
}

func ZS(str string) string {
	str = Replace(str, `<`, `&lt;`)
	str = Replace(str, `>`, `&gt;`)
	str = Replace(str, `'`, `&apos;`)
	str = Replace(str, `"`, `&quot;`)
	return `'` + str + `'`
}

/*  Similar with XSS function, but with adding single quote and percent (%).
func main() {
    L.Print(S.ZLIKE(`coba<`))  // output '%coba&lt;%'
    L.Print(S.ZLIKE(`"coba"`)) // output '%&quot;coba&quot;%'
}
*/
// replace <, >, ', " and gives single quote and %
func ZLIKE(str string) string {
	str = Trim(str)
	str = Replace(str, `<`, `&lt;`)
	str = Replace(str, `>`, `&gt;`)
	str = Replace(str, `'`, `&apos;`)
	str = Replace(str, `"`, `&quot;`)
	return `'%` + str + `%'`
}

/* Adding only single quote in the first and the end of string.
func main() {
    L.Print(S.Q(`coba`)) // output 'coba'
    L.Print(S.Q(`123`))  // output '123'
}
*/
// gives single quote only
func Q(str string) string {
	return `'` + str + `'`
}

/*
func main() {//replace ' to "
    L.Print(S.ZZ(`coba"`)) // output "coba&quot;"
}
*/
// replace ` and give double quote (for table names)
func ZZ(str string) string {
	str = Trim(str)
	str = Replace(str, `"`, `&quot;`)
	//str = Replace(str, "\n", `/n`)
	return `"` + str + `"`
}

func ZZ2(str string) string {
	str = Trim(str)
	str = Replace(str, `"`, `&quot;`)
	str = Replace(str, "\n", `/n`)
	return `"` + str + `"`
}

/*
func main() {
    // Give ' to boolean value
    L.Print(S.ZB(true))  // output 'true'
    L.Print(S.ZB(false)) // output 'false'
}
*/
// boolean
func ZB(b bool) string {
	return `'` + B.ToS(b) + `'`
}

/*
func main() {//give ' to int64 value
    L.Print(S.ZI(23))// output 	'23'
     L.Print(S.ZI(03))// output '3'
}
*/
// single quote an integer
func ZI(num int64) string {
	return `'` + I.ToS(num) + `'`
}

/*
func main() {//give ' to int64 value
hai := `{'test':123,"bla":[1,2,3,4]}`
    L.Print(S.ZJ(hai))// output "{'test':123,\"bla\":[1,2,3,4]}"
}
*/
// double quote a json string
func ZJ(str string) string {
	str = Replace(str, "\r", `\r`)
	str = Replace(str, "\n", `\n`)
	str = Replace(str, `"`, `\"`)
	return `"` + str + `"`
}

/*
func main() {
    // Menentukan panjang password
    // To determined the length of a password
    L.Print(S.RandomPassword(2)) // output vK
    L.Print(S.RandomPassword(4)) // output nF5R
}
*/
// create random string
func RandomPassword(strlen int64) string {
	rand.Seed(time.Now().UTC().UnixNano())
	const chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789" // l and I removed
	result := make([]byte, strlen)
	for i := int64(0); i < strlen; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}

// substring before first `substr`
func LeftOf(str, substr string) string {
	len := strings.Index(str, substr)
	if len < 0 {
		return str
	}
	return str[:len]
}

// substring after first `substr`
func RightOf(str, substr string) string {
	pos := strings.Index(str, substr)
	if pos < 0 {
		return str
	}
	return str[pos+len(substr):]
}

// substring before last `substr`
func LeftOfLast(str, substr string) string {
	len := strings.LastIndex(str, substr)
	if len < 0 {
		return str
	}
	return str[:len]
}

// substring after last `substr`
func RightOfLast(str, substr string) string {
	pos := strings.LastIndex(str, substr)
	if pos < 0 {
		return str
	}
	return str[pos+len(substr):]
}

// coalesce, return first not empty string
func IfEmpty(str1, str2 string) string {
	if str1 != `` {
		return str1
	}
	return str2
}

// coalesce, return first not empty string
func Coalesce(strs ...string) string {
	for _, str := range strs {
		if str != `` {
			return str
		}
	}
	return ``
}

func HashPassword(pass string) string {
	res1 := []byte(pass)
	res2 := sha256.Sum256(res1)
	res3 := res2[:]
	return base64.StdEncoding.EncodeToString(res3)
}

// convert to indonesian long date (from ISO YYYY-MM-DD)
func ToIndoDate(str string) string {
	if len(str) < 10 {
		return ``
	}
	if str[4:5] != `-` || str[7:8] != `-` {
		return ``
	}

	d := Split(str[:10], `-`)
	month := ``
	switch d[1] {
	case `01`:
		month = `Januari`
	case `02`:
		month = `Februari`
	case `03`:
		month = `Maret`
	case `04`:
		month = `April`
	case `05`:
		month = `Mei`
	case `06`:
		month = `Juni`
	case `07`:
		month = `Juli`
	case `08`:
		month = `Agustus`
	case `09`:
		month = `September`
	case `10`:
		month = `Oktober`
	case `11`:
		month = `November`
	case `12`:
		month = `Desember`
	default:
		month = `InvalidMonth:` + d[1]
	}
	return d[2] + ` ` + month + ` ` + d[0]
}

// convert to english long date (from ISO YYYY-MM-DD)
func ToEnglishDate(str string) string {
	if len(str) < 10 {
		return ``
	}
	if str[4:5] != `-` || str[7:8] != `-` {
		return ``
	}

	d := Split(str[:10], `-`)
	month := ``
	switch d[1] {
	case `01`:
		month = `January`
	case `02`:
		month = `February`
	case `03`:
		month = `March`
	case `04`:
		month = `April`
	case `05`:
		month = `May`
	case `06`:
		month = `June`
	case `07`:
		month = `July`
	case `08`:
		month = `August`
	case `09`:
		month = `September`
	case `10`:
		month = `October`
	case `11`:
		month = `November`
	case `12`:
		month = `December`
	default:
		month = `InvalidMonth:` + d[1]
	}
	return month + ` ` + d[2] + `, ` + d[0]
}

// 2016-08-22 Sofyan
func clearHashBcrypt(b []byte) {
	for i := 0; i < len(b); i++ {
		b[i] = 0
	}
}

// 2016-08-22 Sofyan
func HashBcrypt(password []byte) ([]byte, error) {
	defer clearHashBcrypt(password)
	return bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
}

// 2021-07-08 Sadewo
// Hash to number string
func HashStringToNumber(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

// 2017-01-13 Prayogo
// remove last n character, not UTF-8 friendly
func RemoveLastN(str string, n int) string {
	m := len(str)
	if n >= m {
		return ``
	}
	return str[0 : m-3]
}

// 2017-02-14 Prayogo
// remove suffix when last ch found
func BeforeCh(str string, ch byte) string {
	pos := strings.LastIndexByte(str, ch)
	if pos == -1 {
		return str
	}
	return str[:pos]
}

// 2017-02-14 Prayogo
// remove suffix when last substr found
func BeforeStr(str, substr string) string {
	pos := strings.LastIndex(str, substr)
	if pos == -1 {
		return str
	}
	return str[:pos]
}
