// PUKIS' Map (associative array) Helper
package M

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/S"
	"sort"
	"strconv"
	"strings"
)

// map
type SX map[string]interface{}
type IX map[int64]interface{}
type IAX map[int64][]interface{}
type SAX map[string][]interface{}
type SS map[string]string
type SF map[string]float64
type II map[int64]int64
type IB map[int64]bool
type SI map[string]int64
type IS map[int64]string
type SB map[string]bool

/*
func main() {
    m := M.II{1: 2, 2: 567, 3:6, 4:45}
   fmt.Println(m.KeysConcat(`tes`))// output 1tes2tes3tes4
}
*/
// concat integer keys
// example use case:
// m.KeysConcat(",")
func (hash II) KeysConcat(with string) string {
	res := bytes.Buffer{}
	first := true
	for k := range hash {
		if first {
			first = false
		} else {
			res.WriteString(with)
		}
		res.WriteString(I.ToS(k))
	}
	return res.String()
}

/*
func main() {
    m := M.SS{"tes":"tes","coba":"saja","lah":"lah"}
   fmt.Println(m.KeysConcat(","))// output coba,lah,tes
}
*/
func (hash SS) KeysConcat(with string) string {
	res := bytes.Buffer{}
	first := true
	for k := range hash {
		if first {
			first = false
		} else {
			res.WriteString(with)
		}
		res.WriteString(k)
	}
	return res.String()
}
func (hash SS) SortedKeys() []string {
	res := make([]string, len(hash))
	idx := 0
	for k := range hash {
		res[idx] = k
		idx++
	}
	sort.Strings(res)
	return res
}

/*
func main() {
    m := M.SB{"tes":true,"coba":false}
   fmt.Println(m.KeysConcat(","))// output tes,coba
}
*/
func (hash SB) KeysConcat(with string) string {
	res := bytes.Buffer{}
	first := true
	for k := range hash {
		if first {
			first = false
		} else {
			res.WriteString(with)
		}
		res.WriteString(k)
	}
	return res.String()
}

func (hash SB) SortedKeys() []string {
	res := make([]string, len(hash))
	idx := 0
	for k := range hash {
		res[idx] = k
		idx++
	}
	sort.Strings(res)
	return res
}

/*
func main() {
    m := M.IB{1:true,2:false}
   fmt.Println(m.KeysConcat(",")) // output 1,2
}
*/
func (hash IB) KeysConcat(with string) string {
	res := bytes.Buffer{}
	first := true
	for k := range hash {
		if first {
			first = false
		} else {
			res.WriteString(with)
		}
		res.WriteString(I.ToS(k))
	}
	return res.String()
}

/*
func main() {
    m := M.SX{"test": 123,"adalah" : "buku"}
   fmt.Println(m.ToJson()) // output {"adalah":"buku","test":123}
}
*/
func (hash SX) ToJson() string {
	str, err := json.Marshal(hash)
	L.PanicIf(err, `M.ToJson failed`, hash)
	return string(str)
}

func (hash SX) ToJsonPretty() string {
	str, err := json.MarshalIndent(hash, ``, `  `)
	L.PanicIf(err, `M.ToJsonPretty failed`, hash)
	return string(str)
}

// convert to Gob
func (a SX) ToGob() []byte {
	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	err := e.Encode(a)
	L.PanicIf(err, `failed M.SX.ToGob`, a)
	return b.Bytes()
}

// convert from Gob
func (a *SX) FromGob(by []byte) {
	b := bytes.Buffer{}
	b.Write(by)
	d := gob.NewDecoder(&b)
	err := d.Decode(&a)
	L.PanicIf(err, `failed M.SX.FromGob`, by)
}

/*
func main() {
    m := M.SX{}
   L.Describe(m.Error(`error`), m)
   // output "error"
	//M.SX{
	// "errors":    []string{"error"},
	// "has_error": bool(true),
	//}
}
*/
// add an error, only append when there's a content
func (json SX) Error(msg string) string {
	if msg == `` {
		return msg
	}
	if json[`errors`] == nil {
		json[`errors`] = []string{}
	}
	errors := json[`errors`].([]string)
	errors = append(errors, msg)
	json[`errors`] = errors
	json[`is_success`] = false
	L.Describe(`Ajax error`, msg)
	return msg
}

// add an error, only append when there's a content
func (json SX) ErrorIf(err error, msg string) bool {
	if !L.IsError(err, msg) {
		return false
	}
	if msg == `` {
		return true
	}
	if json[`errors`] == nil {
		json[`errors`] = []string{}
	}
	errors := json[`errors`].([]string)
	errors = append(errors, msg)
	json[`errors`] = errors
	json[`is_success`] = false
	return true
}

// add an error, only append when there's a content
func (json SX) Errorf(msg string, args ...interface{}) string {
	if msg == `` {
		return msg
	}
	if json[`errors`] == nil {
		json[`errors`] = []string{}
	}
	errors := json[`errors`].([]string)
	msg = fmt.Sprintf(msg, args...)
	errors = append(errors, msg)
	json[`errors`] = errors
	json[`is_success`] = false
	L.Describe(`Ajax error`, msg)
	return msg
}

/* Clear error
func main() {
     m := M.SX{}
     m.Error(`aaaa`)
     L.Describe(m)
    // output
    // M.SX{
    //    "errors":    []string{"aaaa"},
    //    "has_error": bool(false),
    // }
    m.ClearError()
    L.Describe(m)
    // output
    // M.SX{
    // "errors":    []string{},
    // "has_error": bool(false),
    //}
}
*/
// clear error
func (json SX) ClearError() {
	json[`errors`] = []string{}
	json[`is_success`] = true
}

/*
func main() {
     m := M.SX{}
     m.Error(`1`)
     m.Error(`2`)
    L.Describe(m.LastError())// output    "2"
}
*/
// get last error string
func (json SX) LastError() string {
	if json[`errors`] == nil {
		return ``
	}
	errors := json[`errors`].([]string)
	if len(errors) == 0 {
		return ``
	}
	return errors[len(errors)-1]
}

/*
func main() {
    m := M.SX{}
    m.Error(`1`)
    m.Error(`2`)
   L.Describe(m.AllErrors())// output []string{"1", "2"}
}
*/
// get all error string array
func (json SX) AllErrors() []string {
	if json[`errors`] == nil {
		return []string{}
	}
	return json[`errors`].([]string)
}

/*
func main() {
	m := M.SX{}
	m.Error(`2`) kalau di comment //m.Error(`2`) output bool(false)
	L.Describe(m.HasError())// output 	bool(true)
}
*/
// has errors?
func (json SX) HasError() bool {
	if json[`errors`] == nil {
		return false
	}
	errors := json[`errors`].([]string)
	return len(errors) > 0
}

/*
func main() {
	m := M.SX{}
	m.Info(`2`) // kalau dicomment, output bool(false)
	L.Describe(m.HasInfo()) // output 	bool(true)
}
*/
// has info?
func (json SX) HasInfo() bool {
	if json[`info`] == nil {
		return false
	}
	info := json[`info`].(string)
	return len(info) > 0
}

/*
func main() {
    m := M.SX{}
  m.Error(`1`)
  m.Errors([]string{`2`})
  L.Describe (m)
    // output
    // M.SX{
	//"errors":    []string{"1", "2"},
	//"has_error": bool(true),
	//}
}
*/

// add one or more errors
func (json SX) Errors(msg []string) bool {
	if len(msg) == 0 {
		return false
	}
	if json[`errors`] == nil {
		json[`errors`] = []string{}
	}
	errors := json[`errors`].([]string)
	errors = append(errors, msg...)
	json[`errors`] = errors
	json[`is_success`] = false
	L.Describe(`Ajax errors`, msg)
	return true
}

/*
func main() { //replace  1 to 2 and add \n<br/>
     m := M.SX{`info`:`1`}
    m.Info(`2`)
    L.Describe(m) // output
                  // M.SX{
                  //    "info": "1\n<br/>2",
                  // }
}
*/
func (json SX) Info(msg string) {
	str, ok := (json[`info`]).(string)
	if !ok {
		str = ``
	}
	if len(str) > 0 {
		str += "\n<br/>"
	}
	str += msg
	json[`info`] = str
}

/*
func main() {
     m := M.SX{`info`:`error`}
    m.OverwriteInfo(`errors`)
    L.Describe(m) // output
                  // M.SX{
                  //    "info": "errors",
                  // }
}
*/
func (json SX) OverwriteInfo(msg string) {
	json[`info`] = msg
}

/*
func main() {
    m := M.SX{}
m.Set(`nama`,`bintang`)
    L.Describe(m) // output
                  // M.SX{
                  //    "nama": "bintang",
                  // }
}
*/
// x := M.SX{}
// x.Set(`nama`,`kiswono`)
func (json SX) Set(key string, val interface{}) {
	json[key] = val
}

/*
func main() {
    m := M.SX{}
    m.SetStr(`nama`, `"`)
    m.SetStr(`nama` , `<`)
   L.Describe(m) // output
                 // M.SX{
                 //    "nama": "&lt;",
                 // }
}
*/
// trim and anti XSS
func (json SX) SetStr(key, val string) {
	val = S.XSS(val)
	json[key] = val
}

/*
func main() {
    m := M.SX{}
    m.SetStrAppend(`nama` , `<`)
    m.SetStrAppend(`nama` , `"`)
    L.Describe(m) // output
                  // M.SX{
                  //    "nama": "&lt;&quot;",
                  // }
}
*/
// anti XSS and append
func (json SX) SetStrAppend(key, val string) {
	val = strings.Replace(val, `<`, `&lt;`, -1)
	val = strings.Replace(val, `>`, `&gt;`, -1)
	val = strings.Replace(val, `'`, `&apos;`, -1)
	val = strings.Replace(val, `"`, `&quot;`, -1)
	val = strings.Replace(val, `\`, `/`, -1)
	json[key] = json.GetStr(key) + val
}

/*
func main() {
    m := M.SX{}
    m.Set(`nama`, `bintang`)
    m.Del(`nama`)
    L.Describe(m) // output M.SX{ }
}
*/
func (json SX) Del(key string) {
	delete(json, key)
}

/*
func main() {
    m := M.SX{}
    m.Redirect(`nama`,23)
   L.Describe(m)
   // output	M.SX{
   // "redirect": []interface {}{
   // "nama",
   // int(23),
   // },
   //}
}
*/
func (json SX) Redirect(to string, delay int) {
	json[`redirect`] = []interface{}{to, delay}
}

/* Get int64 type from the map
    m := M.SX{`test`:234.345,`coba`:`buah`,`dia`:true,`angka`:int64(23435)}
    L.Describe(m.GetInt(`test`))  // output int64(234)
    L.Describe(m.GetInt(`dia`))   // output int64(1)
    L.Describe(m.GetInt(`coba`))  // output int64(0)
    L.Describe(m.GetInt(`angka`)) // output int64(23435)
}
*/
// x.GetInt(`test`)
func (json SX) GetInt(key string) int64 {
	any := json[key]
	if any == nil {
		return 0
	}
	if val, ok := any.(int64); ok {
		return val
	}
	switch v := any.(type) {
	case int:
		return int64(v)
	case int8:
		return int64(v)
	case int16:
		return int64(v)
	case int32:
		return int64(v)
	case uint:
		return int64(v)
	case uint8:
		return int64(v)
	case uint16:
		return int64(v)
	case uint32:
		return int64(v)
	case uint64:
		return int64(v)
	case float32:
		return int64(v)
	case float64:
		return int64(v)
	case bool:
		if v {
			return 1
		}
		return 0
	case string:
		if val, err := strconv.ParseInt(v, 10, 64); err == nil {
			return val
		}
		if val, err := strconv.ParseFloat(v, 64); err == nil {
			return int64(val)
		}
		L.Describe(`Property [` + key + `] is not an int64: ` + fmt.Sprintf("%T", any))
	default:
		L.Describe(`Property [` + key + `] is not an int64: ` + fmt.Sprintf("%T", any))
	}
	return 0
}

/* Get float64 type from the map
func main() {
    m := M.SX{`test`:234.345,`coba`:`buah`,`dia`:true,`angka`:23435}
    L.Describe(m.GetFloat(`test`))  // output float64(234.345)
    L.Describe(m.GetFloat(`dia`))   // output int64(1)
    L.Describe(m.GetFloat(`coba`))  // output "Property [coba] is not a float64: string"
                                    //        "buah"
                                    //        float64(0)
    L.Describe(m.GetFloat(`angka`)) // output "Property [angka] is not a float64: int"
                                    //        int(23435)
                                    //        float64(0)
}
*/
func (json SX) GetFloat(key string) float64 {
	any := json[key]
	if any == nil {
		return 0
	}
	if val, ok := any.(float64); ok {
		return val
	}
	switch v := any.(type) {
	case int:
		return float64(v)
	case int8:
		return float64(v)
	case int16:
		return float64(v)
	case int32:
		return float64(v)
	case int64:
		return float64(v)
	case uint:
		return float64(v)
	case uint8:
		return float64(v)
	case uint16:
		return float64(v)
	case uint32:
		return float64(v)
	case uint64:
		return float64(v)
	case float32:
		return float64(v)
	case bool:
		if v {
			return 1
		}
		return 0
	case string:
		if val, err := strconv.ParseFloat(v, 64); err == nil {
			return val
		}
		L.Describe(`Property [`+key+`] is not a float64: `+fmt.Sprintf("%T", any), any)
	default:
		L.Describe(`Property [`+key+`] is not a float64: `+fmt.Sprintf("%T", any), any)
	}
	return 0
}

/* Get string type from the map
func main() {
    m := M.SX{`test`:234.345,`coba`:`buah`,`angka`:int64(123)}
    L.Describe(m.GetStr(`test`))  // output float64(234.345)
    L.Describe(m.GetStr(`coba`))  // output "buah"
    L.Describe(m.GetStr(`angka`)) // output "123"

}
*/
func (json SX) GetStr(key string) string {
	any := json[key]
	if any == nil {
		return ``
	}
	if val, ok := any.(string); ok {
		return val
	}
	switch v := any.(type) {
	case int:
		return strconv.FormatInt(int64(v), 10)
	case int8:
		return strconv.FormatInt(int64(v), 10)
	case int16:
		return strconv.FormatInt(int64(v), 10)
	case int32:
		return strconv.FormatInt(int64(v), 10)
	case int64:
		return strconv.FormatInt(int64(v), 10)
	case uint:
		return strconv.FormatInt(int64(v), 10)
	case uint8:
		return strconv.FormatInt(int64(v), 10)
	case uint16:
		return strconv.FormatInt(int64(v), 10)
	case uint32:
		return strconv.FormatInt(int64(v), 10)
	case uint64:
		return strconv.FormatInt(int64(v), 10)
	case float32:
		return strconv.FormatFloat(float64(v), 'f', -1, 64)
	case float64:
		return strconv.FormatFloat(float64(v), 'f', -1, 64)
	case bool:
		if v {
			return `true`
		}
		return `false`
	case fmt.Stringer:
		return v.String()
	default:
		L.Describe(`Property [` + key + `] is not a string: ` + fmt.Sprintf("%T", any))
	}
	return ``
}

/*
func main() {
     m := M.SX{`test`:234.345,`coba`:`buah`,`angka`:float64(123),`salah`:123}
    L.Describe(m.GetBool(`test`)) //output bool(true)
    L.Describe(m.GetBool(`coba`))// output 	bool(true)
    L.Describe(m.GetBool(`angka`))//output bool(true)
    L.Describe(m.GetBool(`salah`))//output "Property [salah] is not a bool: int"
}
*/
func (json SX) GetBool(key string) bool {
	any := json[key]
	if any == nil {
		return false
	}
	if val, ok := any.(bool); ok {
		return val
	}
	switch v := any.(type) {
	case int:
		return v != 0
	case int8:
		return v != 0
	case int16:
		return v != 0
	case int32:
		return v != 0
	case int64:
		return v != 0
	case uint:
		return v != 0
	case uint8:
		return v != 0
	case uint16:
		return v != 0
	case uint32:
		return v != 0
	case uint64:
		return v != 0
	case float32:
		return v != 0
	case float64:
		return v != 0
	case fmt.Stringer:
		val := v.String()
		val = strings.TrimSpace(strings.ToLower(val))
		return !(val == `` || val == `0` || val == `f` || val == `false`)
	case string:
		val := v
		val = strings.TrimSpace(strings.ToLower(val))
		return !(val == `` || val == `0` || val == `f` || val == `false`)
	default:
		L.Describe(`Property [` + key + `] is not a bool: ` + fmt.Sprintf("%T", any))
	}
	return false
}

/*
func main() {
     m := M.SX{`tes`:M.SB{`1`:true,`2`:false}}
    L.Describe(m.GetMSB(`tes`)) //output M.SB{"1":true, "2":false}
}
*/
func (json SX) GetMSB(key string) SB {
	any := json[key]
	if any == nil {
		return SB{}
	}
	if val, ok := any.(map[string]bool); ok {
		return val
	} else if val, ok := any.(SB); ok {
		return val
	} else if val, ok := any.(map[string]interface{}); ok {
		res := SB{}
		for k, vx := range val {
			if vb, ok := vx.(bool); ok {
				res[k] = vb
			}
		}
		return res
	} else {
		L.Describe(`Property [` + key + `] is not a M.SB: ` + fmt.Sprintf("%T", any))
		return SB{}
	}
}

/*
func main() {
     m := M.SX{`tes`:M.SF{`satu`:32.45,`2`:12}}
    L.Describe(m.GetMSF(`tes`)) //output	M.SF{"satu":32.45, "2":12}
}
*/

func (json SX) GetMSF(key string) SF {
	any := json[key]
	if any == nil {
		return SF{}
	}
	if val, ok := any.(map[string]float64); ok {
		return val
	} else if val, ok := any.(SF); ok {
		return val
	} else if val, ok := any.(map[string]interface{}); ok {
		res := SF{}
		for k, vx := range val {
			if vf, ok := vx.(float64); ok {
				res[k] = vf
			} else if vs, ok := vx.(string); ok {
				res[k] = S.ToF(vs)
			}
		}
		return res
	} else {
		L.Describe(`Property [` + key + `] is not a M.SF: ` + fmt.Sprintf("%T", any))
		return SF{}
	}
}

/*
func main() {
     m := M.SX{`tes`:M.SI{`satu`:34,`dua`:12323}}
    L.Describe(m.GetMSI(`tes`)) //output M.SI{"satu":34, "dua":12323}
}
*/
func (json SX) GetMSI(key string) SI {
	any := json[key]
	if any == nil {
		return SI{}
	}
	if val, ok := any.(map[string]int64); ok {
		return val
	} else if val, ok := any.(SI); ok {
		return val
	} else if val, ok := any.(map[string]interface{}); ok {
		res := SI{}
		for k, vx := range val {
			if vi, ok := vx.(int64); ok {
				res[k] = vi
			} else if vs, ok := vx.(string); ok {
				res[k] = S.ToI(vs)
			}
		}
		return res
	} else {
		L.Describe(`Property [` + key + `] is not a M.SF: ` + fmt.Sprintf("%T", any))
		return SI{}
	}
}

/*
func main() {
     m := M.SX{`tes`:M.IB{1:true,2:false}}
    L.Describe(m.GetMIB(`tes`)) //output M.IB{1:true, 2:false}
}
*/
func (json SX) GetMIB(key string) IB {
	any := json[key]
	if any == nil {
		return IB{}
	}
	if val, ok := any.(map[int64]bool); ok {
		return val
	} else if val, ok := any.(IB); ok {
		return val
	} else if val, ok := any.(map[int64]interface{}); ok {
		res := IB{}
		for k, vx := range val {
			if vb, ok := vx.(bool); ok {
				res[k] = vb
			}
		}
		return res
	} else {
		L.Describe(`Property [` + key + `] is not a M.SB: ` + fmt.Sprintf("%T", any))
		return IB{}
	}
}

/*
func main() {
    m :=  M.SX{`tes`:M.SX{`satu`:234.345,`dua`:`huruf`,`tiga`:123}}
    L.Describe(m.GetMSX(`tes`)) // output
                                // M.SX{
                                //    "tiga": int(123),
                                //    "satu": float64(234.345),
                                //    "dua":  "huruf",
                                // }
}
*/
func (json SX) GetMSX(key string) SX {
	any := json[key]
	if any == nil {
		return SX{}
	}
	if val, ok := any.(map[string]interface{}); ok {
		return val
	} else if val, ok := any.(SX); ok {
		return val
	} else {
		L.Describe(`Property [` + key + `] is not a M.SX: ` + fmt.Sprintf("%T", any))
		return SX{}
	}
}

/*
func main() {
    m :=  M.SX{`tes`:[]interface{}{123,`buah`}}
    L.Describe(m.GetAX(`tes`)) // output
                               // []interface {}{
                               //     int(123),
                               //    "buah",
                               // }
}
*/
func (json SX) GetAX(key string) []interface{} {
	any := json[key]
	if any == nil {
		return []interface{}{}
	}
	if val, ok := any.([]interface{}); ok {
		return val
	} else {
		L.Describe(`Property [` + key + `] is not a A.X: ` + fmt.Sprintf("%T", any))
		return []interface{}{}
	}
}

/*
func main() {
     m :=  M.SX{`tes`:[]int64{123,234}}
    L.Describe(m.GetIntArr(`tes`)) //output []int64{123, 234}
}
*/
func (json SX) GetIntArr(key string) []int64 {
	any := json[key]
	if any == nil {
		return []int64{}
	}
	if val, ok := any.([]int64); ok {
		return val
	} else if val, ok := any.([]float64); ok {
		res := []int64{}
		for _, vf := range val {
			res = append(res, int64(vf))
		}
		return res
	} else if val, ok := any.([]interface{}); ok {
		res := []int64{}
		for k, vx := range val {
			switch v := vx.(type) {
			case int:
				res = append(res, int64(v))
			case int8:
				res = append(res, int64(v))
			case int16:
				res = append(res, int64(v))
			case int32:
				res = append(res, int64(v))
			case uint:
				res = append(res, int64(v))
			case uint8:
				res = append(res, int64(v))
			case uint16:
				res = append(res, int64(v))
			case uint32:
				res = append(res, int64(v))
			case uint64:
				res = append(res, int64(v))
			case float32:
				res = append(res, int64(v))
			case float64:
				res = append(res, int64(v))
			case string:
				if val, err := strconv.ParseInt(v, 10, 64); err == nil {
					res = append(res, int64(val))
				}
				if val, err := strconv.ParseFloat(v, 64); err == nil {
					res = append(res, int64(val))
				}
				L.Describe(`Property [` + key + `][` + I.ToStr(k) + `] is not an int64: ` + fmt.Sprintf("%T", v))
			}
		}
		return res
	} else {
		L.Describe(`Property [` + key + `] is not a []int64: ` + fmt.Sprintf("%T", any))
		return []int64{}
	}
}

/*
func main() {
        m :=  map[string]interface{}{`buah`:123,`angka`:`dia`}
    L.Print(M.ToJson(m)) //output {"angka":"dia","buah":123}
}
*/
func ToJson(hash map[string]interface{}) string {
	str, err := json.Marshal(hash)
	L.PanicIf(err, `M.ToJson failed`, hash)
	return string(str)
}

/*
func main() {
    m1 := M.SX{`satu`:1}
    m2 :=M.SX{`dua`:2}
    L.Describe(M.Merge_Map(m1,m2)) // output
                                   // M.SX{
                                   //    "dua":  int(2),
                                   //    "satu": int(1),
                                   // }
}
*/
//Merge map
func Merge_Map(source, dest SX) SX {
	for k, v := range source {
		dest[k] = v
	}
	return dest
}

/*
func main() {
     m :=  M.SS{`satu`:`1`,`dua`:`2`}
    L.Describe(m.Keys()) //output []string{"satu", "dua"}
}
*/
func (hash SS) Keys() []string {
	res := []string{}
	for k := range hash {
		res = append(res, k)
	}
	return res
}

// merge 2 SS
func (hash SS) Merge(src SS) {
	for k, v := range src {
		hash[k] = v
	}
}

/*
func main() {
     m :=  M.SX{`1`:1,`dua`:`2`}
    L.Describe(m.Keys()) //output []string{"1", "dua"}
}
*/
func (hash SX) Keys() []string {
	res := []string{}
	for k := range hash {
		res = append(res, k)
	}
	return res
}

/*
func main() {
     m :=  M.IX{1:1,2:`DIA`}
    L.Describe(m.Keys()) //output []int64{1, 2}
}
*/
func (hash IX) Keys() []int64 {
	res := []int64{}
	for k := range hash {
		res = append(res, k)
	}
	return res
}

/*
func main() {
     m :=  M.II{1:1,2:3}
    L.Describe(m.Keys()) //output []int64{1, 2}
}
*/
// get all integer keys
func (hash II) Keys() []int64 {
	res := []int64{}
	for k := range hash {
		res = append(res, k)
	}
	return res
}

/*
func main() {
     m :=  M.IB{1:true,2:false}
    L.Describe(m.Keys()) //output []int64{1, 2}
}
*/
// get all integer keys
func (hash IB) Keys() []int64 {
	res := []int64{}
	for k := range hash {
		res = append(res, k)
	}
	return res
}

// convert to json
func (hash IB) MarshalJSON() ([]byte, error) {
	buff := bytes.Buffer{}
	buff.WriteRune('{')
	first := true
	for k := range hash {
		if !first {
			buff.WriteRune(',')
		} else {
			first = false
		}
		buff.WriteRune('"')
		buff.WriteString(I.ToS(k))
		buff.WriteRune('"')
		buff.WriteRune(':')
		buff.WriteString(S.IfElse(hash[k], `true`, `false`))
	}
	buff.WriteRune('}')
	return buff.Bytes(), nil
}

/*
func main() { //KEY  INT DAN VALUENYA BOOL
    m :=  M.IX{1:1,2:`DUA`}
    L.Describe(m.ToSX()) //output M.SX{
                        //"1": int(1),
                        // "2": "DUA",
                        //}
}
*/
// convert integer keys to string keys
func (hash IX) ToSX() SX {
	res := SX{}
	for k, v := range hash {
		res[I.ToS(k)] = v
	}
	return res
}
