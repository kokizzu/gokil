package D

import (
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/T"
	"gitlab.com/kokizzu/gokil/W"
	"gitlab.com/kokizzu/gokil/X"
	"time"
)

// TODO: depreceate this, wait until it's no longer used by President University SoftDev

var OFFICE_MAIL_SUFFIX string

func InitOfficeMail(suffix string) {
	OFFICE_MAIL_SUFFIX = suffix
}

// primary table model
type Data struct {
	Data     M.SX
	Posts    *W.Posts
	Table    string
	Id       int64
	Tx       *Tx
	Actor    int64
	Log      string
	UniqueId string // set when you want to update it
}

// convert Data to JSON string
func (mp *Data) ToJson() string {
	return M.ToJson(mp.Data)
}

// fetch model to be edited
func NewData(tx *Tx, table string, id int64, actor int64, posts *W.Posts) *Data {
	data := tx.DataJsonMap(table, id)
	return &Data{data, posts, table, id, tx, actor, ``, ``}
}

// fetch model to be edited from unique
func NewDataUniq(tx *Tx, table string, unique_id string, actor int64, posts *W.Posts) *Data {
	data, id := tx.DataJsonMapUniq(table, unique_id)
	new_uniq := unique_id
	if id == 0 {
		new_uniq = ``
	}
	res := &Data{data, posts, table, id, tx, actor, ``, new_uniq}
	if id == 0 {
		res.Set_UniqueId(unique_id)
	}
	return res
}

// fetch model to be edited without post data
func NewPostlessData(tx *Tx, table string, id int64, actor int64) *Data {
	data := tx.DataJsonMap(table, id)
	return &Data{data, nil, table, id, tx, actor, ``, ``}
}

// fetch model to be edited without post data, by unique_id
func NewPostlessDataUniq(tx *Tx, table string, unique_id string, actor int64) *Data {
	data, id := tx.DataJsonMapUniq(table, unique_id)
	new_uniq := unique_id
	if id == 0 {
		new_uniq = ``
	}
	res := &Data{data, nil, table, id, tx, actor, ``, new_uniq}
	if id == 0 {
		res.Set_UniqueId(unique_id)
	}
	return res
}

// insert row
func (mp *Data) InsertRow(ajax M.SX) int64 {
	if ajax.HasError() {
		// ignore saving
		ajax.Info(`no record inserted..`)
		ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	new_id := mp.Tx.DoInsert(mp.Actor, mp.Table, params)
	label := mp.Table + `'s row ID:` + I.ToS(new_id)
	if new_id < 1 {
		ajax.Error(`Failed saving ` + label)
		L.Describe(mp.Actor, mp.Data, mp.Id, mp.Log, mp.Table, mp.UniqueId)
		ajax.Set(`id`, new_id)
		return new_id
	}
	mp.Id = new_id
	msg := `Data has been saved successfully.`
	if ajax.GetBool(`debug`) {
		msg += "\n\nCreated new " + label + " with: \n" + mp.Log
	}
	ajax.Info(msg)
	ajax.Set(`id`, new_id)
	return new_id
}

// update row
func (mp *Data) UpdateRow(ajax M.SX) int64 {
	if ajax.HasError() {
		// ignore saving
		ajax.Info(`no record updated..`)
		ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	label := mp.Table + `'s row ID:` + I.ToS(mp.Id)
	if mp.Log == `` {
		ajax.Info(`No changes detected ` + label)
		//L.Describe(mp)
		ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	affected_rows := mp.Tx.DoUpdate(mp.Actor, mp.Table, mp.Id, params)
	if affected_rows < 1 {
		ajax.Error(`Failed saving ` + label)
		L.Describe(mp.Table, mp.Id, mp.UniqueId, mp.Log, mp.Data)
		ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	msg := `Data has been saved successfully.`
	if ajax.GetBool(`debug`) {
		msg += "\n\nUpdated " + label + " with: \n" + mp.Log
	}
	ajax.Info(msg)
	ajax.Set(`id`, mp.Id)
	ajax.Set(`ra`, affected_rows)
	return mp.Id
}

// insert or update row, if uniq ada
func (mp *Data) UpsertRow(ajax M.SX) int64 {
	if ajax.HasError() {
		// ignore saving
		ajax.Info(`no record upserted..`)
		ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	new_rec := mp.Id == 0
	label := mp.Table + `'s row ID:`
	if !new_rec {
		label += I.ToS(mp.Id)
		if mp.Log == `` {
			ajax.Info(`No changes detected ` + label)
			//L.Describe(mp)
			ajax.Set(`id`, mp.Id)
			return mp.Id
		}
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.Id > 0 {
		params[`id`] = mp.Id
	}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	new_id := mp.Tx.DoUpsert(mp.Actor, mp.Table, params)
	if new_rec {
		label += I.ToS(new_id)
	}
	if new_id < 1 {
		ajax.Error(`Failed saving ` + label)
		L.Describe(mp)
		ajax.Set(`id`, new_id)
		return new_id
	} else {
		mp.Id = new_id
	}
	if new_rec && mp.Log == `` {
		ajax.Info(`Saved new ` + label + ` with empty data`)
		ajax.Set(`id`, new_id)
		return new_id
	}
	msg := `Data has been saved successfully.`
	if ajax.GetBool(`debug`) {
		if new_rec {
			msg += "\n\nCreated new " + label + " with: \n<br/>" + mp.Log
		} else {
			msg += "\n\nUpdated " + label + " with: \n<br/>" + mp.Log
		}
	}
	ajax.Info(msg)
	ajax.Set(`id`, new_id)
	return new_id
}

// insert or update row, insert if not exists even when uinque_id exists (error)
func (mp *Data) IndateRow(ajax M.SX) int64 {
	if ajax.HasError() {
		// ignore saving
		ajax.Info(`no record upserted..`)
		return mp.Id
	}
	new_rec := mp.Id == 0
	label := mp.Table + `'s row ID:`
	if !new_rec {
		label += I.ToS(mp.Id)
		if mp.Log == `` {
			ajax.Info(`No changes detected ` + label)
			//L.Describe(mp)
			ajax.Set(`id`, mp.Id)
			return mp.Id
		}
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.Id > 0 {
		params[`id`] = mp.Id
	}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	new_id := int64(0)
	if mp.Id == 0 {
		new_id = mp.Tx.DoForcedInsert(mp.Actor, mp.Table, params)
	} else {
		new_id = mp.Tx.DoUpsert(mp.Actor, mp.Table, params)
	}
	if new_rec {
		label += I.ToS(new_id)
	}
	if new_id < 1 {
		ajax.Error(`Failed saving ` + label)
		L.Describe(mp)
		ajax.Set(`id`, new_id)
		return new_id
	} else {
		mp.Id = new_id
	}
	if new_rec && mp.Log == `` {
		ajax.Info(`Saved new ` + label + ` with empty data`)
		ajax.Set(`id`, new_id)
		return new_id
	}
	msg := `Data has been saved successfully.`
	if ajax.GetBool(`debug`) {
		if new_rec {
			msg += `\n\nCreated new ` + label + " with: \n<br/>" + mp.Log
		} else {
			msg += `\n\nUpdated ` + label + " with: \n<br/>" + mp.Log
		}
	}
	ajax.Info(msg)
	ajax.Set(`id`, new_id)
	return new_id
}

// log the changes
func (mp *Data) LogIt(key string, val interface{}) {
	key_label := ZZ(key)
	newv := X.ToS(val)
	new_label := ZZ(newv)
	if mp.Id == 0 {
		mp.Log += key_label + ` = ` + new_label + "\n<br/>"
	} else {
		oldv := X.ToS(mp.Data[key])
		if oldv != newv {
			mp.Log += key_label + `  from ` + ZZ(oldv) + ` to ` + new_label + "\n<br/>"
		}
	}
}

// set unique id
func (mp *Data) Set_UniqueId(val string) {
	if val != `` {
		key_label := ZZ(`unique_id`)
		new_label := ZZ(val)
		if val != mp.UniqueId {
			mp.Log += key_label + ` = ` + new_label + "\n<br/>"
			mp.UniqueId = S.Trim(val)
		}
	}
	// TODO: unset unique id?
}

// undelete
func (mp *Data) Restore() {
	if mp.Id > 0 {
		if mp.Tx.DoRestore(mp.Actor, mp.Table, mp.Id) {
			mp.Log += "record restored\n<br/>"
		}
	}
}

// delete
func (mp *Data) Delete() {
	if mp.Id > 0 {
		if mp.Tx.DoDelete(mp.Actor, mp.Table, mp.Id) {
			mp.Log += "record deleted\n<br/>"
		}
	}
}

// delete or restore
func (mp *Data) WipeUnwipe(a string) {
	mp.Tx.DoWipeUnwipe(a, mp.Actor, mp.Table, mp.Id)
}

// set string
func (mp *Data) SetStr(key string) string {
	val := mp.Posts.GetStr(key)
	if val != `` {
		val = S.Trim(val)
		val = S.XSS(val)
		mp.LogIt(key, val)
		mp.Data[key] = val
	}
	return X.ToS(mp.Data[key])
}

// set string even if the string is null or empty
func (mp *Data) SetStrN(key string) string {
	val := mp.Posts.GetStr(key)
	val = S.Trim(val)
	val = S.XSS(val)
	mp.LogIt(key, val)
	mp.Data[key] = val
	return X.ToS(mp.Data[key])
}

// set string without xss filtering
func (mp *Data) SetStrNoXSS(key string) string {
	val := mp.Posts.GetStr(key)
	if val != `` {
		val = S.Trim(val)
		mp.LogIt(key, val)
		mp.Data[key] = val
	}
	return X.ToS(mp.Data[key])
}

// set html string
func (mp *Data) SetHtml(key string) string {
	return mp.SetStrNoXSS(key)
}

// set string strip prefix and suffix from and letters
func (mp *Data) SetStrPhone(key string) string {
	val := mp.Posts.GetStr(key)
	val = S.PhoneOf(val)
	if val != `` {
		mp.LogIt(key, val)
		mp.Data[key] = val
	}
	return X.ToS(mp.Data[key])
}

// get string from Data
func (mp *Data) Get_Str(key string) string {
	return X.ToS(mp.Data[key])
}

// get boolean from Data
func (mp *Data) Get_Bool(key string) bool {
	return X.ToBool(mp.Data[key])
}

// get int64 from Data
func (mp *Data) Get_Int(key string) int64 {
	return X.ToI(mp.Data[key])
}

// get []interface{} from Data
func (mp *Data) Get_Arr(key string) []interface{} {
	return X.ToArr(mp.Data[key])
}

// get float64 from Data
func (mp *Data) Get_Float(key string) float64 {
	return X.ToF(mp.Data[key])
}

// get id
func (mp *Data) Get_Id() int64 {
	return mp.Id
}

// get unique id
func (mp *Data) Get_UniqueId() string {
	return mp.UniqueId
}

// set date from Posts to Data
// unset when string is whitespace
func (mp *Data) SetUnsetDate(key string, ajax M.SX) string {
	val := mp.Posts.GetStr(key)
	return mp.SetUnsetValDate(key, val, ajax)
}

func (mp *Data) SetUnsetValDate(key string, val string, ajax M.SX) string {
	//	L.Describe(key, val)
	if val != `` {
		val = S.Trim(val)
		if val == `` {
			mp.Unset(key)
			return ``
		}
		val = S.XSS(val)
		val = S.Replace(val, `/`, `-`)
		d := S.Split(val, `-`)
		if len(d) != 3 {
			ajax.Error(`invalid date format for ` + key + ` parameter, value: ` + Z(val))
			return ``
		}
		d1 := S.ToI(d[0])
		d2 := S.ToI(d[1])
		d3 := S.ToI(d[2])
		if d1 > 1900 && d2 <= 12 && d3 <= 31 && d2 > 0 && d3 > 0 {
			// YYYY-MM-DD
			val = S.PadLeft(I.ToS(d1), `0`, 4) + `-` + S.PadLeft(I.ToS(d2), `0`, 2) + `-` + S.PadLeft(I.ToS(d3), `0`, 2)
		} else if d1 <= 31 && d2 <= 12 && d3 > 1900 && d1 > 0 && d2 > 0 {
			// DD-MM-YYYY
			val = S.PadLeft(I.ToS(d3), `0`, 4) + `-` + S.PadLeft(I.ToS(d2), `0`, 2) + `-` + S.PadLeft(I.ToS(d1), `0`, 2)
		} else {
			ajax.Error(`invalid date format for '` + key + `': ` + val)
			return ``
		}
		_, err := time.Parse(`2006-01-02`, val)
		if err != nil {
			ajax.Error(`invalid date for '` + key + `': ` + val)
			return ``
		}
		mp.LogIt(key, val)
		mp.Data[key] = val
	}
	return X.ToS(mp.Data[key])
}

// set time from Posts to Data
// unset when string is whitespace
func (mp *Data) SetUnsetTime(key string, ajax M.SX) string {
	val := mp.Posts.GetStr(key)
	return mp.SetUnsetValTime(key, val, ajax)
}

//
func (mp *Data) SetUnsetValTime(key string, val string, ajax M.SX) string {
	//	L.Describe(key, val)
	if val != `` {
		val = S.Trim(val)
		if val == `` {
			mp.Unset(key)
			return ``
		}
		val = S.Replace(val, `.`, `:`)
		hh_mm := S.Split(val, `:`)
		if len(hh_mm) < 2 {
			ajax.Error(`invalid format for '` + key + `': ` + val + `, time format must a HH:MM`)
			return ``
		}
		// check hours
		hh := S.ToI(hh_mm[0])
		if hh < 0 || hh > 23 {
			ajax.Error(`invalid hour for '` + key + `': ` + val)
			return ``
		}
		// check minutes
		mm := S.ToI(hh_mm[1])
		if mm < 0 || mm > 59 {
			ajax.Error(`invalid minute for '` + key + `': ` + val)
			return ``
		}
		val = S.PadLeft(I.ToS(hh), `0`, 2) + `:` + S.PadLeft(I.ToS(mm), `0`, 2)
		mp.LogIt(key, val)
		mp.Data[key] = val
	}
	return X.ToS(mp.Data[key])
}

// set dates from Posts to Data
func (mp *Data) SetDates(key string) {
	// TODO: validate this
	val := mp.Posts.GetStr(key)
	if val != `` {
		val = S.Trim(val)
		val = S.XSS(val)
		mp.LogIt(key, val)
		mp.Data[key] = val
	}
}

// set int64 from Posts to Data
func (mp *Data) SetInt(key string) int64 {
	val := mp.Posts.GetStr(key)
	if val != `` {
		mp.LogIt(key, val)
		val := mp.Posts.GetInt(key)
		mp.Data[key] = val
	}
	return X.ToI(mp.Data[key])
}

func (mp *Data) SetUnsetInt(key string) int64 {
	val := mp.Posts.GetStr(key)
	if val == `` && mp.Posts.IsSet(key) {
		mp.Unset(key)
	} else {
		mp.SetInt(key)
	}
	return X.ToI(mp.Data[key])
}

// set float64 from Posts to Data
func (mp *Data) SetFloat(key string) float64 {
	val := mp.Posts.GetStr(key)
	if val != `` {
		mp.LogIt(key, val)
		val := mp.Posts.GetFloat(key)
		mp.Data[key] = val
	}
	return X.ToF(mp.Data[key])
}

// set bool from Posts to Data
func (mp *Data) SetBool(key string) bool {
	val := mp.Posts.GetStr(key)
	if val != `` {
		mp.LogIt(key, val)
		mp.Data[key] = (val == `true`)
	}
	return X.ToBool(mp.Data[key])
}

// unset Data key
func (mp *Data) Unset(key string) {
	oldv, exists := mp.Data[key]
	if exists {
		mp.Log += ZZ(key) + ` removed, previously ` + ZZ(X.ToS(oldv)) + "\n<br/>"
		delete(mp.Data, key)
	}
}

// set unset int, returns 0 when unsetted
func (mp *Data) SetUnsetIntVal(key string, val int64) int64 {
	if val <= 0 {
		mp.Unset(key)
		return 0
	}
	mp.SetVal(key, val)
	return val
}

// set user password, skip logging
func (mp *Data) Set_UserPassword(pass string) {
	if pass != `` {
		mp.Log += ZZ(`password`) + ` changed` + "\n<br/>"
		mp.Data[`password`] = S.HashPassword(pass)
		mp.Data[`last_reset_password`] = T.DateTimeStr()
	}
}

// check password
func (mp *Data) Check_UserPassword(pass string) bool {
	return mp.Data.GetStr(`password`) == S.HashPassword(pass)
}

// set Data value
func (mp *Data) SetVal(key string, val interface{}) interface{} {
	switch v := val.(type) {
	case string:
		v = S.Trim(v)
		val = S.XSS(v)
	}
	mp.LogIt(key, val)
	mp.Data[key] = val
	return val
}

// set Data value with check
func (mp *Data) SetType1(val string, ajax M.SX) string {
	// error check
	if mp.Id > 0 {
		old_type := mp.Data.GetStr(`type`)
		if !(old_type == `` || old_type == val) {
			return ajax.Error(`Invalid record type, trying to overwrite a ` + Z(old_type) + ` record to ` + Z(val) + `, this should not be happened, please contact Site Administrator`)
		}
	}
	mp.SetVal(`type`, val)
	return ``
}

// check type should be inside certain types
func (mp *Data) SetType2(val string, ajax M.SX, types ...string) string {
	if mp.Id > 0 {
		old_type := mp.Data.GetStr(`type`)
		exists := old_type == ``
		for _, allowed := range types {
			if exists {
				break
			}
			exists = exists || old_type == allowed
		}
		if !exists {
			return ajax.Error(`Invalid record type, trying to overwrite a ` + Z(old_type) + ` record to ` + Z(val) + `, this should not be happened, please contact Site Administrator`)
		}
	}
	mp.SetVal(`type`, val)
	return ``
}

func (mp *Data) IsChanged() bool {
	return mp.Log != ``
}

// set val without XSS filtering
func (mp *Data) SetValNoXSS(key string, val interface{}) interface{} {
	mp.LogIt(key, val)
	mp.Data[key] = val
	return val
}

// set Data value if ok
func (mp *Data) SetValIf(ok bool, key string, val interface{}) {
	if ok {
		mp.SetVal(key, val)
	}
}

// set Data value from string
func (mp *Data) SetValStr(key, val string) {
	if val != `` {
		val = S.Trim(val)
		val = S.XSS(val)
		mp.LogIt(key, val)
		mp.Data[key] = val
	}
}

// set Data by current date
func (mp *Data) SetValNow(key string) string {
	val := T.NowStr()
	mp.LogIt(key, val)
	mp.Data[key] = val
	return val
}

func (mp *Data) SetValNow2(key string) string {
	val := T.NowStr2()
	mp.LogIt(key, val)
	mp.Data[key] = val
	return val
}

// set Data by current date epoch as float
func (mp *Data) SetValEpoch(key string) float64 {
	val := T.Epoch()
	mp.LogIt(key, val)
	mp.Data[key] = val
	return float64(val)
}

// set Data by current date epoch as float
func (mp *Data) SetValEpochOnce(key string) float64 {
	old_val := X.ToF(mp.Data[key])
	if old_val > 0 {
		return old_val
	}
	val := T.Epoch()
	mp.LogIt(key, val)
	mp.Data[key] = val
	return float64(val)
}

// set Data office_mail, gmail, yahoo and email
func (mp *Data) Set_UserEmails(emails string, ajax M.SX) (ok bool) {
	return mp.Set_UserEmails_ByTable(emails, ajax, `users`, false)
}

// set Data office_mail, gmail, yahoo and email only for students
func (mp *Data) Set_StudentEmails(emails string, ajax M.SX) (ok bool) {
	return mp.Set_UserEmails_ByTable(emails, ajax, `users`, true)
}

func (mp *Data) Set_UserEmails_ByTable(emails string, ajax M.SX, table string, is_student bool) (ok bool) {
	ok = true
	if emails != `` {
		emails = S.XSS(emails)
		emails = S.ToLower(emails)
		orig := M.SS{}
		orig[`office_mail`] = ``
		orig[`gmail`] = ``
		orig[`yahoo`] = ``
		orig[`email`] = ``
		mails := S.Split(emails, `,`)
		for _, mail := range mails {
			orig_mail := S.Trim(mail)
			mail = S.ValidateEmail(orig_mail)
			if mail == `` {
				L.Describe(`invalid e-mail`, orig_mail)
				continue
			}
			if S.EndWith(mail, `president.ac.id`) {
				orig[`office_mail`] = mail
			} else if S.Contains(mail, `@gmail.`) {
				orig[`gmail`] = mail
			} else if S.Contains(mail, `@yahoo.`) ||
				S.Contains(mail, `@ymail.`) ||
				S.Contains(mail, `@rocketmail.`) {
				orig[`yahoo`] = mail
			} else {
				orig[`email`] = mail
			}
		}
		for _, key := range []string{`office_mail`, `gmail`, `yahoo`, `email`} {
			if key == `office_mail` && is_student {
				continue
			}
			val := orig[key]
			mp.SetVal(key, val)
			if val == `` {
				continue
			}
			lkey, rkey, id_str := Z(key), Z(val), ZI(mp.Id)
			query := `SELECT COALESCE((SELECT id FROM ` + ZZ(table) + ` WHERE data->>` + lkey + ` = ` + rkey + ` AND id <> ` + id_str + ` AND is_deleted = false LIMIT 1),0)`
			dup_id := mp.Tx.QInt(query)
			if dup_id == 0 {
				continue
			}
			msg := `This email: ` + lkey + ` is being used by another user account: ` + rkey + `, if you think this should not be happened, please contact us.` // ` = ` + ZI(dup_id) + ` <> ` + id_str
			L.Describe(msg)
			ajax.Error(msg)
			ok = false
		}
	}
	return
}

// set Data gmail, yahoo and email only for students, except for office_mail
func (mp *Data) Set_StudentEmailsNonPresident(emails string, ajax M.SX) (ok bool) {
	if S.EndWith(emails, `president.ac.id`) {
		msg := `President email cannot be used as your personal Email!`
		L.Describe(msg)
		ajax.Error(msg)
		ok = false
		return
	}
	return mp.Set_UserEmails_ByTable(emails, ajax, `users`, true)
}

// set Data office_mail, gmail, yahoo and email only 1 email
func (mp *Data) Set_OneUserEmails(emails string, ajax M.SX) (ok bool) {
	return mp.Set_OneUserEmail_ByTable(emails, ajax, `users`, false)
}

// set Data office_mail, gmail, yahoo and email only 1 email for students
func (mp *Data) Set_OneStudentEmails(emails string, ajax M.SX) (ok bool) {
	return mp.Set_OneUserEmail_ByTable(emails, ajax, `users`, true)
}

func (mp *Data) Set_OneUserEmail_ByTable(emails string, ajax M.SX, table string, is_student bool) (ok bool) {
	ok = true
	if emails != `` {
		emails = S.XSS(emails)
		emails = S.ToLower(emails)
		orig := M.SS{}
		orig[`office_mail`] = ``
		orig[`gmail`] = ``
		orig[`yahoo`] = ``
		orig[`email`] = ``
		mails := S.Split(emails, `,`)
		if len(mails) > 1 {
			msg := `Email format not valid, should be one your of your email (not student email)`
			L.Describe(msg)
			ajax.Error(msg)
			ok = false
		}
		for _, mail := range mails {
			orig_mail := S.Trim(mail)
			mail = S.ValidateEmail(orig_mail)
			if mail == `` {
				L.Describe(`invalid e-mail`, orig_mail)
				continue
			}
			if S.EndWith(mail, `president.ac.id`) {
				orig[`office_mail`] = mail
			} else if S.Contains(mail, `@gmail.`) {
				orig[`gmail`] = mail
			} else if S.Contains(mail, `@yahoo.`) ||
				S.Contains(mail, `@ymail.`) ||
				S.Contains(mail, `@rocketmail.`) {
				orig[`yahoo`] = mail
			} else {
				orig[`email`] = mail
			}
			break
		}
		for _, key := range []string{`office_mail`, `gmail`, `yahoo`, `email`} {
			//if key == `office_mail` && is_student {
			//	continue
			//}
			val := orig[key]
			mp.SetVal(key, val)
			if val == `` {
				continue
			}
			lkey, rkey, id_str := Z(key), Z(val), ZI(mp.Id)
			query := `SELECT COALESCE((SELECT id FROM ` + ZZ(table) + ` WHERE data->>` + lkey + ` = ` + rkey + ` AND id <> ` + id_str + ` AND is_deleted = false LIMIT 1),0)`
			dup_id := mp.Tx.QInt(query)
			if dup_id == 0 {
				continue
			}
			msg := `This email: ` + lkey + ` is being used by another user account: ` + rkey + `, if you think this should not be happened, please contact us.` // ` = ` + ZI(dup_id) + ` <> ` + id_str
			L.Describe(msg)
			ajax.Error(msg)
			ok = false
		}
	}
	return
}
