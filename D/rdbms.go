// PUKIS' Database Helpers
package D

import (
	"bytes"
	"database/sql"
	"fmt"
	"gitlab.com/kokizzu/gokil/A"
	"gitlab.com/kokizzu/gokil/B"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/J"
	"gitlab.com/kokizzu/gokil/K"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/T"
	"gitlab.com/kokizzu/gokil/X"
	// TODO: replace with faster one `github.com/jackc/pgx/stdlib`, see https://github.com/jackc/pgx/issues/81
	"time"
	// https://jmoiron.github.io/sqlx/
	// https://github.com/jmoiron/sqlx
	// https://sourcegraph.com/github.com/jmoiron/sqlx
	"errors"
	"github.com/OneOfOne/cmap"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	_ "github.com/mutecomm/go-sqlcipher"
)

var DEBUG bool

////////////////
// RECORD SET

type Rows struct {
	ResultSet   *sqlx.Rows
	Query       string
	QueryParams []interface{}
}

func (r *Rows) ErrorCheck(err error, msg string) {
	if len(r.QueryParams) == 0 {
		L.PanicIf(err, `failed `+msg, r.Query)
	} else {
		L.PanicIf(err, `failed `+msg, r.Query, r.QueryParams)
	}
}
func (r *Rows) Err() error {
	return r.ResultSet.Err()
}
func (r *Rows) Next() bool {
	return r.ResultSet.Next()
}
func (r *Rows) Close() {
	r.ResultSet.Close()
}
func (r *Rows) ScanSlice() []interface{} {
	arr, err := r.ResultSet.SliceScan()
	r.ErrorCheck(err, `ScanSlice`)
	for k, v := range arr {
		bs, ok := v.([]uint8)
		if ok {
			arr[k] = string(bs)
		}
	}
	return arr
}
func (r *Rows) ScanStruct(dest interface{}) bool {
	err := r.ResultSet.StructScan(dest)
	r.ErrorCheck(err, `StructScan`)
	return err == nil
}
func (r *Rows) Scan(dest ...interface{}) bool {
	err := r.ResultSet.Scan(dest...)
	r.ErrorCheck(err, `Scan`)
	return err == nil
}
func (r *Rows) ScanMap() map[string]interface{} {
	res := map[string]interface{}{}
	err := r.ResultSet.MapScan(res)
	r.ErrorCheck(err, `MapScan`)
	for k, v := range res {
		bs, ok := v.([]uint8)
		if ok {
			res[k] = string(bs)
		}
	}
	return res
}

////////////////
// GENERATORS

// generate insert, requires table name and field-value params
func GenInsert(table string, kvparams M.SX) (string, []interface{}) {
	query := bytes.Buffer{}
	params := []interface{}{}
	query.WriteString(`INSERT INTO ` + table + `( `)
	len := 0
	for key, val := range kvparams {
		if len > 0 {
			query.WriteString(`, `)
		}
		query.WriteString(key)
		params = append(params, val)
		len++
	}
	query.WriteString(` ) VALUES ( `)
	for z := 1; z <= len; z++ {
		if z > 1 {
			query.WriteString(`, `)
		}
		query.WriteString(`$` + I.ToStr(z))
	}
	query.WriteString(` ) RETURNING id`)
	return query.String(), params
}

// generate update, requires table name, id and field-value params
func GenUpdateId(table string, id int64, kvparams M.SX) (string, []interface{}) {
	return GenUpdateWhere(table, `id = `+I.ToS(id), kvparams)
}

// generate update requires table name, unique id and field-value params
func GenUpdateUniq(table, uniq string, kvparams M.SX) (string, []interface{}) {
	return GenUpdateWhere(table, `unique_id = `+Z(uniq), kvparams)
}

// generate update, requires table, id and
func GenUpdateWhere(table, where string, kvparams M.SX) (string, []interface{}) {
	query := bytes.Buffer{}
	params := []interface{}{}
	query.WriteString(`UPDATE ` + table + ` SET `)
	len := 1
	for key, val := range kvparams {
		if key == `unique_id` && val == `` {
			continue
		}
		if len > 1 {
			query.WriteString(`, `)
		}
		query.WriteString(key + ` = $` + I.ToStr(len))
		params = append(params, val)
		len++
	}
	str := ` WHERE ` + where
	query.WriteString(str)
	return query.String(), params
}

// generate update base, requires table name, id and field-value, and data json string
// http://michael.otacoo.com/postgresql-2/manipulating-jsonb-data-with-key-unique/
// http://schinckel.net/2014/09/29/adding-json%28b%29-operators-to-postgresql/
// data json string provided by: data := M.ToJson(a K.M)
func GenUpdateBase(name string, id int64, kvparams M.SX, data string) (string, []interface{}) {
	query := bytes.Buffer{}
	query.WriteString(`
UPDATE ` + name + `
SET data = (
	SELECT json_object_agg(key, value)::jsonb
	FROM (
		SELECT * FROM jsonb_each( SELECT data FROM ` + name + ` WHERE id = ` + I.ToS(id) + ` )
		UNION ALL
	   SELECT * FROM jsonb_each( $1 )
	) x1
)`)
	params := []interface{}{data}
	len := 2
	for key, val := range kvparams {
		query.WriteString(`, ` + key + ` = $` + I.ToStr(len))
		params = append(params, val)
		len++
	}
	str := ` WHERE id = ` + I.ToS(id)
	query.WriteString(str)
	return query.String(), params
}

// generate update command to unset json data by key
func GenUpdateBaseUnset(name string, id int64, unsetkeys string) (string, []interface{}) {
	query := `
UPDATE ` + name + `
SET data = (
	SELECT json_object_agg(key, value)::jsonb FROM
		( SELECT * FROM jsonb_each( SELECT data FROM ` + name + ` WHERE id = ` + I.ToS(id) + ` )
			WHERE key NOT IN ( $1 )
		) x1
) WHERE id = ` + I.ToS(id)
	params := []interface{}{unsetkeys}
	return query, params
}

/////////////////
// TRANSACTION

type Tx struct {
	Trans *sqlx.Tx
}

// query 2 colums of integer-integer as map
func (tx *Tx) QIntIntMap(query string, params ...interface{}) M.II {
	res := M.II{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := int64(0)
		val := key
		rows.Scan(&key, &val)
		res[key] = val
	}
	return res
}

// query 2 colums of integer-string as map
func (tx *Tx) QIntStrMap(query string, params ...interface{}) M.IS {
	res := M.IS{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := int64(0)
		val := ``
		rows.Scan(&key, &val)
	}
	return res
}

// query 1 colums of string as map
// SELECT data->>'puis_dosenid'
func (tx *Tx) QStrBoolMap(query string, params ...interface{}) M.SB {
	res := M.SB{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	key := ``
	for rows.Next() {
		rows.Scan(&key)
		res[key] = true
	}
	return res
}

// query single column int64, return with true value
func (tx *Tx) QIntBoolMap(query string, params ...interface{}) M.IB {
	res := M.IB{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	key := int64(0)
	for rows.Next() {
		rows.Scan(&key)
		res[key] = true
	}
	return res
}

// query 2 colums of string-string as map
func (tx *Tx) QStrStrMap(query string, params ...interface{}) M.SS {
	res := M.SS{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := ``
		val := ``
		rows.Scan(&key, &val)
		res[key] = val
	}
	return res
}

// query 1+N colums of string-[]any as map
func (tx *Tx) QStrArrMap(query string, params ...interface{}) M.SAX {
	res := M.SAX{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		row := rows.ScanSlice()
		res[X.ToS(row[0])] = row[1:]
	}
	return res
}

// query 2 colums of string-integer as map
func (tx *Tx) QStrIntMap(query string, params ...interface{}) M.SI {
	res := M.SI{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := ``
		val := int64(0)
		rows.Scan(&key, &val)
		res[key] = val
	}
	return res
}

// query 1 colums of integer
func (tx *Tx) QIntCountMap(query string, params ...interface{}) M.II {
	res := M.II{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := int64(0)
		rows.Scan(&val)
		res[val] += 1
	}
	return res
}

// query 1 colums of string
func (tx *Tx) QStrCountMap(query string, params ...interface{}) M.SI {
	res := M.SI{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := ``
		rows.Scan(&val)
		res[val] += 1
	}
	return res
}

// query 1 colums of integer
func (tx *Tx) QIntArr(query string, params ...interface{}) []int64 {
	res := []int64{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := int64(0)
		rows.Scan(&val)
		res = append(res, val)
	}
	return res
}

// query one cell [1,2,3,...] and return array of int64
func (tx *Tx) QJsonIntArr(query string, params ...interface{}) []int64 {
	str := tx.QStr(query, params...)
	ax := S.JsonToArr(str)
	ai := []int64{}
	for _, v := range ax {
		ai = append(ai, X.ToI(v))
	}
	return ai
}

// query 1 colums of string
func (tx *Tx) QStrArr(query string, params ...interface{}) []string {
	res := []string{}
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := ``
		rows.Scan(&val)
		res = append(res, val)
	}
	return res
}

// do query all, also calls DoPrepare, don't forget to close the rows
func (tx *Tx) QAll(query string, params ...interface{}) (rows Rows) {
	var start time.Time
	if DEBUG {
		start = time.Now()
	}
	var err error
	rs, err := tx.Trans.Queryx(query, params...)
	L.PanicIf(err, `failed to QAll: %s %# v`, query, params)
	rows = Rows{rs, query, params}
	if DEBUG {
		L.TimeTrack(start, query)
	}
	return
}

// execute a select single value query, convert to string
func (tx *Tx) QStr(query string, params ...interface{}) (dest string) {
	err := tx.Trans.Get(&dest, query, params...)
	L.PanicIf(err, `failed to QStr: %s %# v`, query, params)
	return
}

// check non-zero if unique exists
func (tx *Tx) QId(table, uniq string) (id int64) {
	uniq = Z(uniq)
	query := `SELECT COUNT(*) FROM ` + table + ` WHERE unique_id = ` + uniq
	id = tx.QInt(query)
	if id == 0 {
		return
	}
	query = `SELECT id FROM ` + table + ` WHERE unique_id = ` + uniq
	id = tx.QInt(query)
	return
}

// check if id exists
func (tx *Tx) QExists(table string, id int64) bool {
	id_str := I.ToS(id)
	query := `SELECT COUNT(*) FROM ` + table + ` WHERE id = ` + id_str
	count := tx.QInt(query)
	if count == 0 {
		return false
	}
	return true
}

// return non-empty string if id exists
func (tx *Tx) QUniq(table string, id int64) (uniq string) {
	id_str := I.ToS(id)
	query := `SELECT COUNT(*) FROM ` + table + ` WHERE id = ` + id_str
	count := tx.QInt(query)
	if count == 0 {
		return
	}
	query = `SELECT unique_id FROM ` + table + ` WHERE id = ` + id_str
	uniq = tx.QStr(query)
	return
}

// execute a select pair value query, convert to int64 and string
func (tx *Tx) QIntStr(query string, params ...interface{}) (i int64, s string) {
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&i, &s)
		return
	}
	return
}

// execute a select pair value query, convert to string and int
func (tx *Tx) QStrInt(query string, params ...interface{}) (s string, i int64) {
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&s, &i)
		return
	}
	return
}

// execute a select pair value query, convert to string and float64
func (tx *Tx) QStrFloat(query string, params ...interface{}) (s string, f float64) {
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&s, &f)
		return
	}
	return
}

// execute a select pair value query, convert to string and string
func (tx *Tx) QStrStr(query string, params ...interface{}) (s string, ss string) {
	rows := tx.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		rows.Scan(&s, &ss)
		return
	}
	return
}

// execute a select single value query, convert to int64
func (tx *Tx) QInt(query string, params ...interface{}) (dest int64) {
	err := tx.Trans.Get(&dest, query, params...)
	L.PanicIf(err, `failed to QInt: %s %# v`, query, params)
	return
}

func (tx *Tx) QFloat(query string, params ...interface{}) (dest float64) {
	err := tx.Trans.Get(&dest, query, params...)
	L.PanicIf(err, `failed to QFloat: %s %# v`, query, params)
	return
}

// fetch a row to as Base struct
func (tx *Tx) QBase(table string, id int64) (base Base) {
	query := `SELECT * FROM ` + S.ZZ(table) + ` WHERE id = ` + I.ToS(id)
	rows := tx.QAll(query)
	defer rows.Close()
	if rows.Next() {
		rows.ScanStruct(&base)
		base.DataToMap()
	}
	base.Table = table
	return
}

// fetch a row to Base struct by unique_id
func (tx *Tx) QBaseUniq(table, uid string) (base Base) {
	query := `SELECT * FROM ` + ZZ(table) + ` WHERE unique_id = ` + Z(uid)
	rows := tx.QAll(query)
	defer rows.Close()
	if rows.Next() {
		rows.ScanStruct(&base)
		base.DataToMap()
	}
	return
}

// execute anything that doesn't need LastInsertId or RowsAffected
func (tx *Tx) DoExec(query string, params ...interface{}) sql.Result {
	res, err := tx.Trans.Exec(query, params...)
	L.PanicIf(err, query)
	return res
}

// generate insert command and execute it
func (tx *Tx) DoInsert(actor int64, table string, kvparams M.SX) (id int64) {
	uniq, ok := kvparams[`unique_id`].(string)
	id = tx.QId(table, uniq)
	if ok && id > 0 {
		// already exists, cancel insert
		id = 0
		return
	}
	id = tx.DoForcedInsert(actor, table, kvparams)
	return
}

// do insert without checking
func (tx *Tx) DoForcedInsert(actor int64, table string, kvparams M.SX) (id int64) {
	kvparams[`created_by`] = actor
	query, params := GenInsert(table, kvparams)
	//L.Describe(query, params)
	rows := tx.QAll(query, params...)
	defer rows.Close()
	if rows.Next() {
		rows.Scan(&id)
	} else {
		L.PanicIf(rows.Err(), `failed to insert on %s: %s; %# v`, table, query, params)
	}
	return
}

// generate insert or update command and execute it
func (tx *Tx) DoUpsert(actor int64, table string, kvparams M.SX) (id int64) {
	id, ok := kvparams[`id`].(int64)
	exists := false
	if ok && id > 0 {
		exists = tx.QExists(table, id)
	} else {
		uniq, ok := kvparams[`unique_id`].(string)
		id = tx.QId(table, uniq)
		exists = (ok && id > 0)
	}
	if exists {
		// update
		tx.DoUpdate(actor, table, id, kvparams)
	} else {
		// insert
		if id == 0 {
			delete(kvparams, `id`)
		}
		id = tx.DoForcedInsert(actor, table, kvparams)
	}
	return
}

// generate update command and execute it
func (tx *Tx) DoUpdate(actor int64, table string, id int64, kvparams M.SX) (ra int64) {
	if id > 0 {
		kvparams[`updated_by`] = actor
		query, params := GenUpdateId(table, id, kvparams)
		rs := tx.DoExec(query, params...)
		ra, _ = rs.RowsAffected()
		return
	}
	// when id not given, find the id first
	uniq, ok := kvparams[`unique_id`].(string)
	if !ok {
		// doesn't exists, cancel update
		ra = 0
		return
	}
	id = tx.QId(table, uniq)
	if id == 0 {
		// doesn't exists, cancel update
		ra = 0
		return
	}
	return tx.DoUpdate(actor, table, id, kvparams) // try again with given ID
}

// execute delete (is_deleted = true)
func (tx *Tx) DoDelete(actor int64, table string, id int64) bool {
	kvparams := M.SX{}
	kvparams[`is_deleted`] = true
	kvparams[`deleted_by`] = actor
	query, params := GenUpdateId(table, id, kvparams)
	rs := tx.DoExec(query, params...)
	ra, _ := rs.RowsAffected()
	return ra > 0
}

// execute delete (is_deleted = false)
func (tx *Tx) DoRestore(actor int64, table string, id int64) bool {
	kvparams := M.SX{}
	kvparams[`is_deleted`] = false
	kvparams[`restored_by`] = actor
	query, params := GenUpdateId(table, id, kvparams)
	rs := tx.DoExec(query, params...)
	ra, _ := rs.RowsAffected()
	return ra > 0
}

// delete or restore
func (tx *Tx) DoWipeUnwipe(a string, actor int64, table string, id int64) bool {
	if a == `save` || a == `` {
		return false
	}
	switch a {
	case `restore`:
		return tx.DoRestore(actor, table, id)
	case `delete`:
		return tx.DoDelete(actor, table, id)
	default:
		if S.StartWith(a, `restore_`) {
			return tx.DoRestore(actor, table, id)
		} else if S.StartWith(a, `delete_`) {
			return tx.DoDelete(actor, table, id)
		}
	}
	return false
}

// execute delete (is_deleted = true)
func (tx *Tx) DoDelete2(actor int64, table string, id int64, lambda func(base *Base) string, ajax M.SX) bool {
	base := tx.QBase(table, id)
	err_msg := lambda(&base)
	if err_msg == `` {
		return base.Delete(actor)
	}
	ajax.Error(err_msg)
	return false
}

// execute delete (is_deleted = false)
func (tx *Tx) DoRestore2(actor int64, table string, id int64, lambda func(base *Base) string, ajax M.SX) bool {
	base := tx.QBase(table, id)
	err_msg := lambda(&base)
	if err_msg == `` {
		return base.Restore(actor)
	}
	ajax.Error(err_msg)
	return false
}

// fetch json data as map
func (tx *Tx) DataJsonMap(table string, id int64) M.SX {
	res := M.SX{}
	if id <= 0 {
		return res
	}
	query := `SELECT data FROM ` + table + ` WHERE id = ` + I.ToS(id)
	rows := tx.QAll(query)
	str := ``
	defer rows.Close()
	if rows.Next() {
		rows.Scan(&str)
		res = J.ToMap(str)
	}
	return res
}

// fecth json data as map by unique id
func (tx *Tx) DataJsonMapUniq(table, unique_id string) (res M.SX, id int64) {
	res = M.SX{}
	id = 0
	if unique_id == `` {
		return
	}
	query := `SELECT data, id FROM ` + table + ` WHERE unique_id = ` + Z(unique_id)
	rows := tx.QAll(query)
	str := ``
	defer rows.Close()
	if rows.Next() {
		rows.Scan(&str, &id)
		res = J.ToMap(str)
	}
	return
}

// query any number of columns, returns first line of line
func (db *Tx) QFirstMap(query string, params ...interface{}) M.SX {
	rows := db.QAll(query)
	defer rows.Close()
	for rows.Next() {
		return rows.ScanMap()
	}
	return M.SX{}
}

///////////
// RDBMS

// wrapper for GO's sql.DB
type RDBMS struct {
	Name    string
	Adapter *sqlx.DB
}

// create new postgresql connection to localhost
func NewPgConn(user, db string) *RDBMS {
	opt := `user=` + user + ` dbname=` + db + ` sslmode=disable`
	conn := sqlx.MustConnect(`postgres`, opt)
	//conn.DB.SetMaxIdleConns(1)  // according to http://jmoiron.github.io/sqlx/#connectionPool
	conn.DB.SetMaxOpenConns(61) // TODO: change this according to postgresql.conf -3
	name := `pg::` + user + `@/` + db
	return &RDBMS{
		Name:    name,
		Adapter: conn,
	}
}

// create new mysql connection
func NewMyConn(user, db, ip, pass string) *RDBMS {
	opt := user + `:` + pass + `@` + ip + `/` + db
	conn := sqlx.MustConnect(`mysql`, opt)
	name := `my::` + opt
	return &RDBMS{
		Name:    name,
		Adapter: conn,
	}
}

// create new sqlite database
func NewSqConn(filename, password string) *RDBMS {
	conn := sqlx.MustConnect(`sqlite3`, `file:`+filename)
	db := &RDBMS{
		Name:    `sq::` + filename,
		Adapter: conn,
	}
	conn.MustExec(`PRAGMA key = ` + Z(password))
	conn.MustExec(`PRAGMA cipher_page_size = 4096`)
	return db
}

// query any number of columns, returns array of slice (to be exported directly to json, not for processing)
func (db *RDBMS) QArray(query string, params ...interface{}) A.X {
	rows := db.QAll(query)
	res := A.X{}
	defer rows.Close()
	for rows.Next() {
		res = append(res, rows.ScanSlice())
	}
	return res
}

// query any number of columns, returns first line of line
func (db *RDBMS) QFirstMap(query string, params ...interface{}) M.SX {
	rows := db.QAll(query)
	defer rows.Close()
	for rows.Next() {
		return rows.ScanMap()
	}
	return M.SX{}
}

// query any number of columns, returns first line of line
func (db *RDBMS) QFirstArray(query string, params ...interface{}) A.X {
	rows := db.QAll(query)
	defer rows.Close()
	for rows.Next() {
		return rows.ScanSlice()
	}
	return A.X{}
}

// query any number of columns, returns array of slice (to be exported directly to json, not for processing)
func (db *RDBMS) QMapArray(query string, params ...interface{}) A.MSX {
	rows := db.QAll(query)
	res := A.MSX{}
	defer rows.Close()
	for rows.Next() {
		res = append(res, rows.ScanMap())
	}
	return res
}

// query to tsv file
func (db *RDBMS) QTsv(header, query string, params ...interface{}) bytes.Buffer {
	res := bytes.Buffer{}
	res.WriteString(header)
	rows := db.QAll(query)
	defer rows.Close()
	for rows.Next() {
		row := rows.ScanSlice()
		for k := range row {
			res.WriteString(X.ToS(row[k]))
			res.WriteRune('\t')
		}
		res.WriteRune('\n')
	}
	return res
}

// query any number of columns, returns map of string, array (to be exported directly to json, not for processing)
// the key_idx will be converted to string and taken as key
func (db *RDBMS) QStrIdxArrMap(key_idx int64, query string, params ...interface{}) M.SAX {
	rows := db.QAll(query)
	res := M.SAX{}
	defer rows.Close()
	for rows.Next() {
		row := rows.ScanSlice()
		key_str := X.ToS(row[key_idx])
		res[key_str] = row
	}
	return res
}

// query any number of columns, returns map of string, array (to be exported directly to json, not for processing)
// the first index will be converted to string and taken as key
func (db *RDBMS) QStrShiftArrMap(query string, params ...interface{}) M.SAX {
	rows := db.QAll(query)
	res := M.SAX{}
	defer rows.Close()
	for rows.Next() {
		row := rows.ScanSlice()
		key_str := X.ToS(row[0])
		res[key_str] = row[1:]
	}
	return res
}

// query any number of columns, returns map of string, map (to be exported directly to json, not for processing)
// the key_idx will be converted to string and taken as key
func (db *RDBMS) QStrMapMap(key_idx string, query string, params ...interface{}) M.SX {
	rows := db.QAll(query)
	res := M.SX{}
	defer rows.Close()
	for rows.Next() {
		row := rows.ScanMap()
		key_str := X.ToS(row[key_idx])
		res[key_str] = row
	}
	return res
}

// query 2 colums of integer-integer as map
// SELECT id, COUNT(*)
func (db *RDBMS) QIntIntMap(query string, params ...interface{}) M.II {
	res := M.II{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := int64(0)
		val := key
		rows.Scan(&key, &val)
		res[key] = val
	}
	return res
}

// query 2 colums of integer-string as map
// SELECT id, puis_dosenid
func (db *RDBMS) QIntStrMap(query string, params ...interface{}) M.IS {
	res := M.IS{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := int64(0)
		val := ``
		rows.Scan(&key, &val)
		res[key] = val
	}
	return res
}

// query 1 colums of string as map
// SELECT data->>'puis_dosenid'
func (db *RDBMS) QStrBoolMap(query string, params ...interface{}) M.SB {
	res := M.SB{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	key := ``
	for rows.Next() {
		rows.Scan(&key)
		res[key] = true
	}
	return res
}

// query single column int64, return with true value
func (db *RDBMS) QIntBoolMap(query string, params ...interface{}) M.IB {
	res := M.IB{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	key := int64(0)
	for rows.Next() {
		rows.Scan(&key)
		res[key] = true
	}
	return res
}

// query 2 colums of string-string as map
func (db *RDBMS) QStrStrMap(query string, params ...interface{}) M.SS {
	res := M.SS{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := ``
		val := ``
		rows.Scan(&key, &val)
		res[key] = val
	}
	return res
}

// query 1+N columns of string-[]any as map
func (db *RDBMS) QStrArrMap(query string, params ...interface{}) M.SAX {
	res := M.SAX{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		row := rows.ScanSlice()
		res[X.ToS(row[0])] = row[1:]
	}
	return res
}

// query 2 colums of string-integer as map
func (db *RDBMS) QStrIntMap(query string, params ...interface{}) M.SI {
	res := M.SI{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		key := ``
		val := int64(0)
		rows.Scan(&key, &val)
		res[key] = val
	}
	return res
}

// query 1 colums of integer as map
func (db *RDBMS) QIntCountMap(query string, params ...interface{}) M.II {
	res := M.II{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := int64(0)
		rows.Scan(&val)
		res[val] += 1
	}
	return res
}

// query 1 colums of string as map
// result equal to: SELECT name, COUNT(*) FROM tabel1 GROUP BY 1
// map[string]int
func (db *RDBMS) QStrCountMap(query string, params ...interface{}) M.SI {
	res := M.SI{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := ``
		rows.Scan(&val)
		res[val] += 1
	}
	return res
}

// query 1 colums of integer
func (db *RDBMS) QIntArr(query string, params ...interface{}) []int64 {
	res := []int64{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := int64(0)
		rows.Scan(&val)
		res = append(res, val)
	}
	return res
}

// query one cell [1,2,3,...] and return array of int64
func (db *RDBMS) QJsonIntArr(query string, params ...interface{}) []int64 {
	str := db.QStr(query, params...)
	ax := S.JsonToArr(str)
	ai := []int64{}
	for _, v := range ax {
		ai = append(ai, X.ToI(v))
	}
	return ai
}

// query 1 colums of string
func (db *RDBMS) QStrArr(query string, params ...interface{}) []string {
	res := []string{}
	rows := db.QAll(query, params...)
	defer rows.Close()
	for rows.Next() {
		val := ``
		rows.Scan(&val)
		res = append(res, val)
	}
	return res
}

// do query all, also calls DoPrepare, don't forget to close the rows
func (db *RDBMS) QAll(query string, params ...interface{}) (rows Rows) {
	var start time.Time
	if DEBUG {
		start = time.Now()
	}
	var err error
	rs, err := db.Adapter.Queryx(query, params...)
	L.PanicIf(err, `failed to QAll: %s %# v`, query, params)
	rows = Rows{rs, query, params}
	if DEBUG {
		L.LogTrack(start, query)
	}
	return
}

// execute a select single value query, convert to string
func (db *RDBMS) QStr(query string, params ...interface{}) (dest string) {
	err := db.Adapter.Get(&dest, query, params...)
	L.PanicIf(err, `failed to QStr: %s %# v`, query, params)
	return
}

// check if unique exists
func (db *RDBMS) QId(table, key string) (id int64) {
	db.DoTransaction(func(tx *Tx) string {
		id = tx.QId(table, key)
		return ``
	})
	return
}

// check if id exists
func (db *RDBMS) QExists(table string, id int64) (ex bool) {
	db.DoTransaction(func(tx *Tx) string {
		ex = tx.QExists(table, id)
		return ``
	})
	return
}

// get unique_id from id
func (db *RDBMS) QUniq(table string, key int64) (uniq string) {
	db.DoTransaction(func(tx *Tx) string {
		uniq = tx.QUniq(table, key)
		return ``
	})
	return
}

// execute a select pair query, convert to int64 and string
func (db *RDBMS) QIntStr(query string, params ...interface{}) (i int64, s string) {
	db.DoTransaction(func(tx *Tx) string {
		i, s = tx.QIntStr(query, params...)
		return ``
	})
	return
}

// execute a select pair query, convert to string and int64
func (db *RDBMS) QStrInt(query string, params ...interface{}) (s string, i int64) {
	db.DoTransaction(func(tx *Tx) string {
		s, i = tx.QStrInt(query, params...)
		return ``
	})
	return
}

// execute a select pair query, convert to string and int64
func (db *RDBMS) QStrFloat(query string, params ...interface{}) (s string, f float64) {
	db.DoTransaction(func(tx *Tx) string {
		s, f = tx.QStrFloat(query, params...)
		return ``
	})
	return
}

// execute a select pair query, convert to string and string
func (db *RDBMS) QStrStr(query string, params ...interface{}) (s string, ss string) {
	db.DoTransaction(func(tx *Tx) string {
		s, ss = tx.QStrStr(query, params...)
		return ``
	})
	return
}

// query single column string, return with true value

// execute a select single value query, convert to int64
func (db *RDBMS) QBool(query string, params ...interface{}) (dest bool) {
	err := db.Adapter.Get(&dest, query, params...)
	L.PanicIf(err, `failed to QBool: %s %# v`, query, params)
	return
}

// execute a select single value query, convert to int64
func (db *RDBMS) QInt(query string, params ...interface{}) (dest int64) {
	err := db.Adapter.Get(&dest, query, params...)
	L.PanicIf(err, `failed to QInt: %s %# v`, query, params)
	return
}

// query count from a table
func (db *RDBMS) QCount(query string) (dest int64) {
	query = `
SELECT COUNT(*)
FROM (
	` + query + `
) count_sq0`
	err := db.Adapter.Get(&dest, query)
	L.PanicIf(err, `failed to QCount: %s`, query)
	return
}

func (db *RDBMS) QFloat(query string, params ...interface{}) (dest float64) {
	err := db.Adapter.Get(&dest, query, params...)
	L.PanicIf(err, `failed to QFloat: %s %# v`, query, params)
	return
}

// fetch a row to as Base struct
func (db *RDBMS) QBase(table string, id int64) (base Base) {
	db.DoTransaction(func(tx *Tx) string {
		base = tx.QBase(table, id)
		base.Connection = db
		return ``
	})
	return
}

// fetch a row to Base struct by unique_id
func (db *RDBMS) QBaseUniq(table, uid string) (base Base) {
	db.DoTransaction(func(tx *Tx) string {
		base = tx.QBaseUniq(table, uid)
		base.Connection = db
		return ``
	})
	return
}

// generate insert command and execute it
func (db *RDBMS) DoInsert(actor int64, table string, kvparams M.SX) (id int64) {
	db.DoTransaction(func(tx *Tx) string {
		id = tx.DoInsert(actor, table, kvparams)
		return ``
	})
	return
}

// generate insert or update command and execute it
func (db *RDBMS) DoUpsert(actor int64, table string, kvparams M.SX) (id int64) {
	id = 0
	db.DoTransaction(func(tx *Tx) string {
		id = tx.DoUpsert(actor, table, kvparams)
		return ``
	})
	return
}

// generate update command and execute it
func (db *RDBMS) DoUpdate(actor int64, table string, id int64, kvparams M.SX) (ra int64) {
	db.DoTransaction(func(tx *Tx) string {
		ra = tx.DoUpdate(actor, table, id, kvparams)
		return ``
	})
	return
}

// delete base table
func (db *RDBMS) DoDelete(actor int64, table string, id int64) (ok bool) {
	db.DoTransaction(func(tx *Tx) string {
		ok = tx.DoDelete(actor, table, id)
		return ``
	})
	return
}

// delete or restore
func (db *RDBMS) DoWipeUnwipe(a string, actor int64, table string, id int64) bool {
	if a == `save` || a == `` {
		return false
	}
	switch a {
	case `restore`:
		return db.DoRestore(actor, table, id)
	case `delete`:
		return db.DoDelete(actor, table, id)
	default:
		if S.StartWith(a, `restore_`) {
			return db.DoRestore(actor, table, id)
		} else if S.StartWith(a, `delete_`) {
			return db.DoDelete(actor, table, id)
		}
	}
	return false
}

// restore base table
func (db *RDBMS) DoRestore(actor int64, table string, id int64) (ok bool) {
	db.DoTransaction(func(tx *Tx) string {
		ok = tx.DoRestore(actor, table, id)
		return ``
	})
	return
}

// execute delete (is_deleted = true)
func (db *RDBMS) DoDelete2(actor int64, table string, id int64, lambda func(base Record) string, ajax M.SX) bool {
	base := db.QBase(table, id)
	err_msg := lambda(&base)
	if err_msg == `` {
		return base.Delete(actor)
	}
	ajax.Error(err_msg)
	return false
}

// execute delete (is_deleted = false)
func (db *RDBMS) DoRestore2(actor int64, table string, id int64, lambda func(base Record) string, ajax M.SX) bool {
	base := db.QBase(table, id)
	err_msg := lambda(&base)
	if err_msg == `` {
		return base.Restore(actor)
	}
	ajax.Error(err_msg)
	return false
}

// begin, commit transaction and rollback automatically when there are error
func (db *RDBMS) DoTransaction(lambda func(tx *Tx) string) {
	tx := db.Adapter.MustBegin()
	ok := ``
	defer func() {
		str := recover()
		if str != nil {
			// rollback when error
			err := tx.Rollback()
			L.PanicIf(errors.New(`transaction error`), `failed to end transaction %# v / %# v`, str, err)
			return
		}
		if ok == `` {
			// commit when empty string or nil
			err := tx.Commit()
			L.PanicIf(err, `failed to commit transaction`)
			return
		}
		L.Describe(ok)
		tx.Rollback() // rollback when there is a string
	}()
	ok = lambda(&Tx{tx})
}

// fetch all as json, after modified time only
// returns rows:{},columns{}
func (db *RDBMS) JsonAll(table, modified, where string) bytes.Buffer {
	query := `
		SELECT id
			, unique_id
			, modified_at
			, is_deleted
			, deleted_by
			, data
		FROM ` + table + ` x1 `
	if S.IsTime(modified) {
		query += `
		WHERE modified_at > ` + Z(modified)
		if where != `` {
			query += `
			AND ` + where
		}
	} else if where != `` {
		query += `
		WHERE ` + where
	}
	//	L.Describe(query)
	rows := db.QAll(query)

	res := bytes.Buffer{}
	columns := []interface{}{``, `id`, `unique_id`, `modified_at`, `is_deleted`, `deleted_by`}
	indexes := M.SI{}
	for idx, key := range columns {
		indexes[X.ToS(key)] = int64(idx)
	}
	start := int64(len(columns))
	res.WriteString(`rows:{`)
	first := true
	var str string
	defer rows.Close()
	for rows.Next() {
		if !first {
			res.WriteString(`,`)
		}
		first = false
		var id int64
		var unique_id sql.NullString
		var modified_at time.Time
		var is_deleted bool
		var deleted_by sql.NullInt64
		var data string
		rows.Scan(
			&id,
			&unique_id,
			&modified_at,
			&is_deleted,
			&deleted_by,
			&data,
		)
		id_str := I.ToS(id)
		str = id_str + `:{1:` + id_str
		str += `,2:'` + unique_id.String
		str += `',3:'` + T.ToS(modified_at)
		str += `',4:` + B.ToS(is_deleted)
		str += `,5:` + I.ToS(deleted_by.Int64)
		res.WriteString(str)
		jsonkv := S.JsonToMap(data)
		for key, val := range jsonkv {
			idx := indexes[key]
			if idx < 1 {
				indexes[key] = start
				columns = append(columns, key)
				idx = start
				start += 1
			}
			str = `,` + I.ToS(idx) + `:`
			str += K.ToJson5(val)
			res.WriteString(str)
		}
		res.WriteRune('}')
	}

	res.WriteString(`},columns:`)
	res.WriteString(A.ToJson(columns))
	return res
}

func (db *RDBMS) ViewExists(viewname string) bool {
	query := `SELECT COALESCE((SELECT COUNT(*) FROM information_schema.views WHERE table_name = ` + Z(viewname) + `),0)`
	return db.QInt(query) > 0
}

// require postgre 9.5+ for IF NOT EXISTS
func (db *RDBMS) MaterializedView(viewname, query string) string {
	start := time.Now()
	rquery := `CREATE MATERIALIZED VIEW IF NOT EXISTS ` + ZZ(viewname) + ` AS ` + query
	db.DoTransaction(func(tx *Tx) string {
		tx.DoExec(`DROP MATERIALIZED VIEW IF EXISTS ` + ZZ(viewname) + ` CASCADE`)
		tx.DoExec(rquery)
		return ``
	})
	L.LogTrack(start, viewname)
	return viewname
}

// 2015-12-04 Kiz: replacement for JsonLine
// lambda should return empty string if it's correct row
func (db *RDBMS) JsonRow(table string, id int64, lambda func(rec Record) string) M.SX {
	base := db.QBase(table, id)
	err_msg := lambda(&base)
	if err_msg == `` {
		ajax := base.XData
		ajax.Set(`unique_id`, base.UniqueId.String)
		ajax.Set(`is_deleted`, base.IsDeleted)
		return ajax
	} else {
		ajax := M.SX{}
		ajax.Error(err_msg)
		return ajax
	}
}

///////////////
// CACHED QUERY

// cached query module, all queries will be cached for TTL seconds per ram_key, expired per ram bucket
// bucket = the bucket of keys, all keys will be removed when it's expired
// ram_key = the key of the query

var CACHE_INV cmap.CMap  // table invalidate date
var CACHE_TTL cmap.CMap  // cache invalidate time
var CACHE_BORN cmap.CMap // cache born time
var CACHE cmap.CMap      // cache real storage

const TTL = 4

func init() {
	DEBUG = true
	CACHE_INV = cmap.New()
	CACHE_BORN = cmap.New()
	CACHE_TTL = cmap.New()
	CACHE = cmap.New()
}

func RamDel_ByQuery(ram_key, query string) {
	CACHE.Delete(ram_key)
}

func RamGlobalEvict_ByAjax_ByBucket(ajax M.SX, bucket string) {
	if !ajax.HasError() {
		CACHE_INV.Set(bucket, T.UnixNano())
	}
}

func RamExpired_ByBucket_ByKey(bucket, ram_key string) bool {
	global := CACHE_INV.Get(bucket)
	if global != nil {
		born := CACHE_BORN.Get(ram_key)
		if born != nil {
			// when new data exists, invalidate all cache
			if born.(int64) <= global.(int64) {
				CACHE.Delete(ram_key)
				if DEBUG {
					L.Print(`RamExpired_ByBucket_ByKey: ` + bucket + ` ` + ram_key)
				}
				return true
			}
		}
	}
	local := CACHE_TTL.Get(ram_key)
	if local != nil {
		now := T.UnixNano()
		if local.(int64) <= now {
			CACHE.Delete(ram_key)
			if DEBUG {
				L.Print(`RamExpired_ByBucket_ByKey: ` + bucket + ` ` + ram_key)
			}
			return true
		}
	}
	return false
}

func RamSet_ByBucket_ByRamKey_ByQuery(bucket, ram_key, query string, val interface{}, sec int64) {
	if DEBUG {
		L.Print(`RamSet_ByBucket_ByRamKey_ByQuery: ` + bucket + ` ` + ram_key + fmt.Sprintf("\n%# v", val))
	}
	CACHE_BORN.Set(ram_key, T.UnixNano())
	CACHE.Set(ram_key, val)
	dur := time.Second * time.Duration(sec)
	CACHE_TTL.Set(ram_key, T.UnixNanoAfter(dur))
	go (func() {
		time.Sleep(dur)
		RamExpired_ByBucket_ByKey(bucket, ram_key)
	})()
}

// returns false when cache expired
func RamGet_ByRamKey_ByQuery(ram_key, query string) interface{} {
	res := CACHE.Get(ram_key)
	if res == nil {
		if DEBUG {
			L.Print(`RamGet_ByRamKey_ByQuery: (miss) ` + ram_key)
		}
	}
	return res
}

// M.SX
// only set when not yet being set, preventing double write and double delete
func RamSetMSX(bucket, ram_key, query string, val M.SX, sec int64) M.SX {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, ram_key, query, val, sec)
	return val
}

func RamGetMSX(ram_key, query string) (M.SX, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return M.SX{}, false
	}
	v, ok := val.(M.SX)
	return v, ok
}

// A.MSX
// only set when not yet being set, preventing double write and double delete
func RamSetAMSX(bucket, key, query string, val A.MSX, sec int64) A.MSX {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetAMSX(ram_key, query string) (A.MSX, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return A.MSX{}, false
	}
	v, ok := val.(A.MSX)
	return v, ok
}

// []STRING
// only set when not yet being set, preventing double write and double delete
func RamSetAS(bucket, key, query string, val []string, sec int64) []string {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetAS(ram_key, query string) ([]string, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return []string{}, false
	}
	v, ok := val.([]string)
	return v, ok
}

// []STRING
// only set when not yet being set, preventing double write and double delete
func RamSetAI(bucket, key, query string, val []int64, sec int64) []int64 {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetAI(ram_key, query string) ([]int64, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return []int64{}, false
	}
	v, ok := val.([]int64)
	return v, ok
}

// MAP[INT64]STRING
// only set when not yet being set, preventing double write and double delete
func RamSetMIS(bucket, key, query string, val M.IS, sec int64) M.IS {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetMIS(ram_key, query string) (M.IS, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return M.IS{}, false
	}
	v, ok := val.(M.IS)
	return v, ok
}

// FLOAT64
// only set when not yet being set, preventing double write and double delete
func RamSetFloat(bucket, key, query string, val float64, sec int64) float64 {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetFloat(ram_key, query string) (float64, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return 0, false
	}
	v, ok := val.(float64)
	return v, ok
}

// BOOL
// only set when not yet being set, preventing double write and double delete
func RamSetBool(bucket, key, query string, val bool, sec int64) bool {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetBool(ram_key, query string) (bool, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return false, false
	}
	v, ok := val.(bool)
	return v, ok
}

// INT64
// only set when not yet being set, preventing double write and double delete
func RamSetInt(bucket, key, query string, val int64, sec int64) int64 {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetInt(ram_key, query string) (int64, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return 0, false
	}
	v, ok := val.(int64)
	return v, ok
}

// STRING
// only set when not yet being set, preventing double write and double delete
func RamSetStr(bucket, key, query string, val string, sec int64) string {
	RamSet_ByBucket_ByRamKey_ByQuery(bucket, key, query, val, sec)
	return val
}

func RamGetStr(ram_key, query string) (string, bool) {
	val := RamGet_ByRamKey_ByQuery(ram_key, query)
	if val == nil {
		return ``, false
	}
	v, ok := val.(string)
	return v, ok
}

// query and cache AMSX for TTL seconds
func (conn *RDBMS) CQMapArray(bucket, ram_key, query string) A.MSX {
	if val, ok := RamGetAMSX(ram_key, query); ok {
		return val
	}
	val := conn.QMapArray(query)
	return RamSetAMSX(bucket, ram_key, query, val, TTL)
}

// query and cache M.SX for TTL seconds
func (conn *RDBMS) CQFirstMap(bucket, ram_key, query string) M.SX {
	if val, ok := RamGetMSX(ram_key, query); ok {
		return val
	}
	val := conn.QFirstMap(query)
	return RamSetMSX(bucket, ram_key, query, val, TTL)
}

// query and cache []string for TTL seconds
func (conn *RDBMS) CQStrArr(bucket, ram_key, query string) []string {
	if val, ok := RamGetAS(ram_key, query); ok {
		return val
	}
	val := conn.QStrArr(query)
	return RamSetAS(bucket, ram_key, query, val, TTL)
}

// query and cache bool for TTL seconds
func (conn *RDBMS) CQBool(bucket, ram_key, query string) bool {
	if val, ok := RamGetBool(ram_key, query); ok {
		return val
	}
	val := conn.QBool(query)
	return RamSetBool(bucket, ram_key, query, val, TTL)
}

// query and cache int64 for TTL seconds
func (conn *RDBMS) CQInt(bucket, ram_key, query string) int64 {
	if val, ok := RamGetInt(ram_key, query); ok {
		return val
	}
	val := conn.QInt(query)
	return RamSetInt(bucket, ram_key, query, val, TTL)
}

// Query and cache float64 for TTL seconds
func (conn *RDBMS) CQFloat(bucket, ram_key, query string) float64 {
	if val, ok := RamGetFloat(ram_key, query); ok {
		return val
	}
	val := conn.QFloat(query)
	return RamSetFloat(bucket, ram_key, query, val, TTL)
}

// query and cache string for TTL seconds
func (conn *RDBMS) CQStr(bucket, ram_key, query string) string {
	if val, ok := RamGetStr(ram_key, query); ok {
		return val
	}
	val := conn.QStr(query)
	return RamSetStr(bucket, ram_key, query, val, TTL)
}

// query and cache M.SX for TTL seconds
func (conn *RDBMS) CQStrMapMap(bucket, ram_key, index, query string) M.SX {
	if val, ok := RamGetMSX(ram_key, query); ok {
		return val
	}
	val := conn.QStrMapMap(index, query)
	return RamSetMSX(bucket, ram_key, query, val, TTL)
}

// query and cache []int64 for TTL seconds
func (conn *RDBMS) CQIntArray(bucket, ram_key, query string) []int64 {
	if val, ok := RamGetAI(ram_key, query); ok {
		return val
	}
	val := conn.QIntArr(query)
	return RamSetAI(bucket, ram_key, query, val, TTL)
}

// query and cache map[int64]string for TTL seconds
func (conn *RDBMS) CQIntStrMap(bucket, ram_key, query string) map[int64]string {
	if val, ok := RamGetMIS(ram_key, query); ok {
		return val
	}
	val := conn.QIntStrMap(query)
	return RamSetMIS(bucket, ram_key, query, val, TTL)
}
