package D

import (
	"database/sql"
	"github.com/lib/pq"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/J"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/X"
)

func rowsAffected(rs sql.Result) int64 {
	ra, err := rs.RowsAffected()
	L.PanicIf(err, `failed to get rows affected`, ra)
	return ra
}

var Z func(string) string
var ZZ func(string) string
var ZJ func(string) string
var ZI func(int64) string
var ZLIKE func(string) string
var ZS func(string) string

func init() {
	Z = S.Z
	ZZ = S.ZZ
	ZJ = S.ZJ
	ZI = S.ZI
	ZLIKE = S.ZLIKE
	ZS = S.ZS
}

type Record interface {
	Get_Str(string) string
	Get_Float(string) float64
	Get_Int(string) int64
	Get_Arr(string) []interface{}
	Get_Bool(string) bool
	Get_Id() int64
	Get_UniqueId() string
}

// base structure for all model, including schema
type Base struct {
	Id         int64 `db:"id"`
	Table      string
	Connection *RDBMS
	UniqueId   sql.NullString `db:"unique_id"`
	CreatedAt  pq.NullTime    `db:"created_at"`
	UpdatedAt  pq.NullTime    `db:"updated_at"`
	DeletedAt  pq.NullTime    `db:"deleted_at"`
	RestoredAt pq.NullTime    `db:"restored_at"`
	ModifiedAt pq.NullTime    `db:"modified_at"`
	CreatedBy  sql.NullInt64  `db:"created_by"`
	UpdatedBy  sql.NullInt64  `db:"updated_by"`
	DeletedBy  sql.NullInt64  `db:"deleted_by"`
	RestoredBy sql.NullInt64  `db:"restored_by"`
	IsDeleted  bool           `db:"is_deleted"`
	DataStr    string         `db:"data"` // json object
	XData      M.SX
}

func (b *Base) DataToMap() M.SX {
	if b.DataStr != `` {
		b.XData = J.ToMap(b.DataStr)
	}
	return b.XData
}

func (b *Base) MapToData() string {
	b.DataStr = M.ToJson(b.XData)
	return b.DataStr
}

// 2015-08-27 also update unique_id
func (b *Base) Save(actor int64) bool {
	records := M.SX{
		`id`:   b.Id,
		`data`: b.MapToData(),
	}
	if b.UniqueId.Valid {
		records[`unique_id`] = b.UniqueId.String
	}
	id := b.Connection.DoUpsert(actor, b.Table, records)
	if id == 0 {
		return false
	}
	b.Id = id
	return true
}

// 2016-05-25 delete row, TODO: add DeletedAt
func (b *Base) Delete(actor int64) bool {
	if b.Connection.DoDelete(actor, b.Table, b.Id) {
		b.IsDeleted = true
		return true
	}
	return false
}

// 2016-05-25 restore row, TODO: add RestoredAt
func (b *Base) Restore(actor int64) bool {
	if b.Connection.DoRestore(actor, b.Table, b.Id) {
		b.IsDeleted = false
		return true
	}
	return false
}

// get string from Data
func (b *Base) Get_Str(key string) string {
	return b.XData.GetStr(key)
}

// get boolean from Data
func (b *Base) Get_Bool(key string) bool {
	return b.XData.GetBool(key)
}

// get int64 from Data
func (b *Base) Get_Int(key string) int64 {
	return b.XData.GetInt(key)
}

// get []interface{} from Data
func (b *Base) Get_Arr(key string) []interface{} {
	return X.ToArr(b.XData[key])
}

// get float64 from Data
func (b *Base) Get_Float(key string) float64 {
	return b.XData.GetFloat(key)
}

// get id
func (b *Base) Get_Id() int64 {
	return b.Id
}

// get unique id
func (b *Base) Get_UniqueId() string {
	return b.UniqueId.String
}

// rename a base table
func (db *RDBMS) RenameBaseTable(oldname, newname string) {
	db.DoTransaction(func(tx *Tx) string {
		query := `ALTER TABLE ` + ZZ(oldname) + ` RENAME TO ` + ZZ(newname)
		tx.DoExec(query)
		oldtrig := oldname + `__trigger`
		newtrig := newname + `__trigger`
		query = `ALTER TRIGGER ` + ZZ(oldtrig) + ` ON ` + ZZ(newname) + ` RENAME TO ` + ZZ(newtrig)
		tx.DoExec(query)
		oldseq := oldname + `_id_seq`
		newseq := newname + `_id_seq`
		query = `ALTER SEQUENCE ` + ZZ(oldseq) + ` RENAME TO ` + ZZ(newseq)
		tx.DoExec(query)
		oldlog := `_log_` + oldname
		newlog := `_log_` + newname
		query = `ALTER TABLE ` + ZZ(oldlog) + ` RENAME TO ` + ZZ(newlog)
		tx.DoExec(query)
		oldseq = oldlog + `_id_seq`
		newseq = newlog + `_id_seq`
		query = `ALTER SEQUENCE ` + ZZ(oldseq) + ` RENAME TO ` + ZZ(newseq)
		tx.DoExec(query)
		return ``
	})
}

// init trigger

func (db *RDBMS) InitTrigger() {
	// auto update timestamp trigger
	query := `
CREATE OR REPLACE FUNCTION timestamp_changer() RETURNS trigger AS $$
DECLARE
	changed BOOLEAN  := FALSE;
	log_table TEXT := quote_ident('_log_' || TG_TABLE_NAME);
	info TEXT := '';
	mod_time TIMESTAMP := CURRENT_TIMESTAMP;
	actor BIGINT;
	query TEXT := '';
BEGIN
	IF (OLD.unique_id <> NEW.unique_id) THEN
		NEW.updated_at := mod_time;
		actor := NEW.updated_by;
		changed := TRUE;
		IF info <> '' THEN info := info || chr(10); END IF;
		info := info || 'unique' || E'\t' || OLD.unique_id || E'\t' || NEW.unique_id;
	END IF;
	IF (OLD.is_deleted = TRUE) AND (NEW.is_deleted = FALSE) THEN
		NEW.restored_at := mod_time;
		actor := NEW.restored_by;
		IF info <> '' THEN info := info || chr(10); END IF;
		info := info || 'restore';
		changed := TRUE;
	END IF;
	IF (OLD.is_deleted = FALSE) AND (NEW.is_deleted = TRUE) THEN
		NEW.deleted_at := mod_time;
		actor := NEW.deleted_by;
		IF info <> '' THEN info := info || chr(10); END IF;
		info := info || 'delete';
		changed := TRUE;
	END IF;
	IF (OLD.data <> NEW.data) THEN
		NEW.updated_at := mod_time;
		IF info <> '' THEN info := info || chr(10); END IF;
		info := info || 'update';
		query := 'INSERT INTO ' || log_table || '( record_id, user_id, date, info, data_before, data_after )' || ' VALUES(' || OLD.id || ',' || NEW.updated_by || ',' || quote_literal(mod_time) || ',' || quote_literal(info) || ',' || quote_literal(NEW.data) || ',' || quote_literal(OLD.data) || ')';
		EXECUTE query;
		changed := TRUE;
	ELSEIF changed THEN
		query := 'INSERT INTO ' || log_table || '( record_id, user_id, date, info )' || ' VALUES(' || OLD.id || ',' || actor || ',' || quote_literal(mod_time) || ',' || quote_literal(info) || ')';
		EXECUTE query;
	END IF;
	IF changed THEN NEW.modified_at := mod_time; END IF;
	RETURN NEW;
END; $$ LANGUAGE plpgsql;`
	db.DoTransaction(func(tx *Tx) string {
		tx.DoExec(query)
		return ``
	})
}

// create a base table
func (db *RDBMS) CreateBaseTable(name string) {
	db.DoTransaction(func(tx *Tx) string {
		query := `
CREATE TABLE IF NOT EXISTS ` + name + ` (
	id BIGSERIAL PRIMARY KEY,
	unique_id VARCHAR(120) UNIQUE,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP WITH TIME ZONE,
	deleted_at TIMESTAMP WITH TIME ZONE,
	restored_at TIMESTAMP WITH TIME ZONE,
	modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	created_by BIGINT,
	updated_by BIGINT,
	deleted_by BIGINT,
	restored_by BIGINT,
	is_deleted BOOLEAN DEFAULT FALSE,
	data JSONB
);`
		is_deleted__index := name + `__is_deleted__index`
		modified_at__index := name + `__modified_at__index`
		unique_patern__index := name + `__unique__patern__index`
		query_count_index := `SELECT COUNT(*) FROM pg_indexes WHERE indexname = `
		ra, _ := tx.DoExec(query).RowsAffected()
		if ra > 0 {
			query = query_count_index + Z(is_deleted__index)
			if tx.QInt(query) == 0 {
				query = `CREATE INDEX ` + name + `__is_deleted__index ON ` + name + `(is_deleted);`
				tx.DoExec(query)
			}
			query = query_count_index + Z(modified_at__index)
			if tx.QInt(query) == 0 {
				query = `CREATE INDEX ` + name + `__modified_at__index ON ` + name + `(modified_at);`
				tx.DoExec(query)
			}
			query = query_count_index + Z(unique_patern__index)
			if tx.QInt(query) == 0 {
				query = `CREATE INDEX ` + name + `__unique__pattern ON ` + name + ` (unique_id varchar_pattern_ops);`
				tx.DoExec(query)
			}
			query = query_count_index + Z(name)
			if tx.QInt(query) == 0 {
				query = `CREATE INDEX ON ` + name + ` USING GIN(data)`
				tx.DoExec(query)
			}
		}
		trig_name := name + `__timestamp_changer`
		query = `SELECT COUNT(*) FROM pg_trigger WHERE tgname = ` + Z(trig_name)
		if tx.QInt(query) == 0 {
			query = `DROP TRIGGER IF EXISTS ` + trig_name + ` ON ` + name
			tx.DoExec(query)
			query = `CREATE TRIGGER ` + trig_name + ` BEFORE UPDATE ON ` + name + ` FOR EACH ROW EXECUTE PROCEDURE timestamp_changer();`
		}
		tx.DoExec(query)
		// logs
		if name == `users` { // TODO: tambahkan record ke access_log ketika login, renew session, logout
			query = `
CREATE TABLE IF NOT EXISTS access_logs (
	id BIGSERIAL PRIMARY KEY,
	user_id BIGINT REFERENCES users(id),
	log_count BIGINT,
	session VARCHAR(256),
	ip_address VARCHAR(256),
	logs TEXT,
	CONSTRAINT unique__user_id__logs UNIQUE(user_id,session)
);`
			tx.DoExec(query)
		}
		query = `
CREATE TABLE IF NOT EXISTS _log_` + name + ` (
	id BIGSERIAL PRIMARY KEY,
	record_id BIGINT REFERENCES ` + name + `(id) ON UPDATE CASCADE,
	user_id BIGINT REFERENCES users(id),
	date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	info TEXT,
	data_before JSONB NULL,
	data_after JSONB NULL
);`
		tx.DoExec(query)
		idx_name := `_log_` + name + `__record_id__idx`
		query = query_count_index + Z(idx_name)
		if tx.QInt(query) == 0 {
			query = `CREATE INDEX	` + idx_name + ` ON 	_log_` + name + `	(record_id);`
			tx.DoExec(query)
		}

		idx_name = `_log_` + name + `__date__idx`
		query = query_count_index + Z(idx_name)
		if tx.QInt(query) == 0 {
			query = `CREATE INDEX	` + idx_name + ` ON 	_log_` + name + `	(date);`
			tx.DoExec(query)
		}
		idx_name = `_log_` + name + `__user_id__idx`
		query = query_count_index + Z(idx_name)
		if tx.QInt(query) == 0 {
			query = `CREATE INDEX	` + idx_name + ` ON 	_log_` + name + `	(user_id);`
			tx.DoExec(query)
		}
		return ``
	})

}

// reset sequence, to be called after TRUNCATE TABLE tablename
func (db *RDBMS) FixSerialSequence(table string) {
	db.DoTransaction(func(tx *Tx) string {
		next := tx.QInt(`SELECT COALESCE(MAX(id)+1,1) FROM ` + table)
		tx.DoExec(`ALTER SEQUENCE ` + table + `_id_seq RESTART WITH ` + I.ToS(next))
		return ``
	})
}
