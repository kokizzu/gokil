package D

import (
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/T"
	"gitlab.com/kokizzu/gokil/W"
	"gitlab.com/kokizzu/gokil/X"
	"time"
)

// primary table model
type Row struct {
	Row      M.SX
	Posts    *W.Posts
	Ajax     *M.SX
	ReqModel *W.RequestModel
	Table    string
	Id       int64
	Tx       *Tx
	DbActor  int64
	Log      string
	UniqueId string // set when you want to update it
}

// convert Row to JSON string
func (mp *Row) ToJson() string {
	return M.ToJson(mp.Row)
}

// fetch model to be edited
func NewRow(tx *Tx, table string, rm *W.RequestModel) *Row {
	data := tx.DataJsonMap(table, rm.Id)
	return &Row{data, rm.Posts, &rm.Ajax, rm, table, rm.Id, tx, rm.DbActor, ``, ``}
}

// fetch model to be edited from unique
func NewRowUniq(tx *Tx, table string, unique_id string, rm *W.RequestModel) *Row {
	data, id := tx.DataJsonMapUniq(table, unique_id)
	new_uniq := unique_id
	if id == 0 {
		new_uniq = ``
	}
	res := &Row{data, rm.Posts, &rm.Ajax, rm, table, id, tx, rm.DbActor, ``, new_uniq}
	if id == 0 {
		res.Set_UniqueId(unique_id)
	}
	return res
}

// insert row
func (mp *Row) InsertRow() int64 {
	if mp.Ajax.HasError() {
		// ignore saving
		mp.Ajax.Info(`no record inserted..`)
		mp.Ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	new_id := mp.Tx.DoInsert(mp.DbActor, mp.Table, params)
	label := mp.Table + `'s row ID:` + I.ToS(new_id)
	if new_id < 1 {
		mp.Ajax.Error(`Failed saving ` + label)
		L.Describe(mp.DbActor, mp.Row, mp.Id, mp.Log, mp.Table, mp.UniqueId)
		mp.Ajax.Set(`id`, new_id)
		return new_id
	}
	mp.Id = new_id
	mp.Ajax.Info(`Created new ` + label + " with: \n" + mp.Log)
	mp.Ajax.Set(`id`, new_id)
	return new_id
}

// update row
func (mp *Row) UpdateRow() int64 {
	if mp.Ajax.HasError() {
		// ignore saving
		mp.Ajax.Info(`no record updated..`)
		mp.Ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	label := mp.Table + `'s row ID:` + I.ToS(mp.Id)
	if mp.Log == `` {
		mp.Ajax.Info(`No changes detected ` + label)
		//L.Describe(mp)
		mp.Ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	new_id := mp.Tx.DoUpdate(mp.DbActor, mp.Table, mp.Id, params)
	if new_id < 1 {
		mp.Ajax.Error(`Failed saving ` + label)
		L.Describe(mp.Table, mp.Id, mp.UniqueId, mp.Log, mp.Row)
		mp.Ajax.Set(`id`, new_id)
		return new_id
	}
	mp.Ajax.Info(`Updated ` + label + " with: \n" + mp.Log)
	mp.Ajax.Set(`id`, new_id)
	return new_id
}

// insert or update row, if uniq ada
func (mp *Row) UpsertRow() int64 {
	if mp.Ajax.HasError() {
		// ignore saving
		mp.Ajax.Info(`no record upserted..`)
		mp.Ajax.Set(`id`, mp.Id)
		return mp.Id
	}
	new_rec := mp.Id == 0
	label := mp.Table + `'s row ID:`
	if !new_rec {
		label += I.ToS(mp.Id)
		if mp.Log == `` {
			mp.Ajax.Info(`No changes detected ` + label)
			//L.Describe(mp)
			mp.Ajax.Set(`id`, mp.Id)
			return mp.Id
		}
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.Id > 0 {
		params[`id`] = mp.Id
	}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	new_id := mp.Tx.DoUpsert(mp.DbActor, mp.Table, params)
	if new_rec {
		label += I.ToS(new_id)
	}
	if new_id < 1 {
		mp.Ajax.Error(`Failed saving ` + label)
		L.Describe(mp)
		mp.Ajax.Set(`id`, new_id)
		return new_id
	} else {
		mp.Id = new_id
	}
	if new_rec && mp.Log == `` {
		mp.Ajax.Info(`Saved new ` + label + ` with empty data`)
		mp.Ajax.Set(`id`, new_id)
		return new_id
	}
	if new_rec {
		mp.Ajax.Info(`Created new ` + label + " with: \n<br/>" + mp.Log)
	} else {
		mp.Ajax.Info(`Updated ` + label + " with: \n<br/>" + mp.Log)
	}
	mp.Ajax.Set(`id`, new_id)
	return new_id
}

// insert or update row, insert if not exists even when uinque_id exists (error)
func (mp *Row) IndateRow() int64 {
	if mp.Ajax.HasError() {
		// ignore saving
		mp.Ajax.Info(`no record upserted..`)
		return mp.Id
	}
	new_rec := mp.Id == 0
	label := mp.Table + `'s row ID:`
	if !new_rec {
		label += I.ToS(mp.Id)
		if mp.Log == `` {
			mp.Ajax.Info(`No changes detected ` + label)
			//L.Describe(mp)
			mp.Ajax.Set(`id`, mp.Id)
			return mp.Id
		}
	}
	data_str := mp.ToJson()
	params := M.SX{`data`: data_str}
	if mp.Id > 0 {
		params[`id`] = mp.Id
	}
	if mp.UniqueId != `` {
		params[`unique_id`] = mp.UniqueId
	}
	new_id := int64(0)
	if mp.Id == 0 {
		new_id = mp.Tx.DoForcedInsert(mp.DbActor, mp.Table, params)
	} else {
		new_id = mp.Tx.DoUpsert(mp.DbActor, mp.Table, params)
	}
	if new_rec {
		label += I.ToS(new_id)
	}
	if new_id < 1 {
		mp.Ajax.Error(`Failed saving ` + label)
		L.Describe(mp)
		mp.Ajax.Set(`id`, new_id)
		return new_id
	} else {
		mp.Id = new_id
	}
	if new_rec && mp.Log == `` {
		mp.Ajax.Info(`Saved new ` + label + ` with empty data`)
		mp.Ajax.Set(`id`, new_id)
		return new_id
	}
	if new_rec {
		mp.Ajax.Info(`Created new ` + label + " with: \n<br/>" + mp.Log)
	} else {
		mp.Ajax.Info(`Updated ` + label + " with: \n<br/>" + mp.Log)
	}
	mp.Ajax.Set(`id`, new_id)
	return new_id
}

// log the changes
func (mp *Row) LogIt(key string, val interface{}) {
	key_label := ZZ(key)
	newv := X.ToS(val)
	new_label := ZZ(newv)
	if mp.Id == 0 {
		mp.Log += key_label + ` = ` + new_label + "\n<br/>"
	} else {
		oldv := X.ToS(mp.Row[key])
		if oldv != newv {
			mp.Log += key_label + `  from ` + ZZ(oldv) + ` to ` + new_label + "\n<br/>"
		}
	}
}

// set unique id
func (mp *Row) Set_UniqueId(val string) {
	if val != `` {
		key_label := ZZ(`unique_id`)
		new_label := ZZ(val)
		if val != mp.UniqueId {
			mp.Log += key_label + ` = ` + new_label + "\n<br/>"
			mp.UniqueId = S.Trim(val)
		}
	}
	// TODO: unset unique id?
}

// undelete
func (mp *Row) Restore() {
	if mp.Id > 0 {
		if mp.Tx.DoRestore(mp.DbActor, mp.Table, mp.Id) {
			mp.Log += "record restored\n<br/>"
		}
	}
}

// delete
func (mp *Row) Delete() {
	if mp.Id > 0 {
		if mp.Tx.DoDelete(mp.DbActor, mp.Table, mp.Id) {
			mp.Log += "record deleted\n<br/>"
		}
	}
}

// delete or restore
func (mp *Row) WipeUnwipe(a string) {
	mp.Tx.DoWipeUnwipe(a, mp.DbActor, mp.Table, mp.Id)
}

// set string
func (mp *Row) SetStr(key string) string {
	val := mp.Posts.GetStr(key)
	if val != `` {
		val = S.Trim(val)
		val = S.XSS(val)
		mp.LogIt(key, val)
		mp.Row[key] = val
	}
	return X.ToS(mp.Row[key])
}

// set string strip prefix and suffix from and letters
func (mp *Row) SetStrPhone(key string) string {
	val := mp.Posts.GetStr(key)
	val = S.PhoneOf(val)
	if val != `` {
		mp.LogIt(key, val)
		mp.Row[key] = val
	}
	return X.ToS(mp.Row[key])
}

// set international phone, format: +xx xxxxxx
func (mp *Row) SetIntlPhone(key string) string {
	val := mp.Posts.GetStr(key)
	val = S.Trim(val)
	if val != `` {
		part := S.Split(val, ` `)
		trim := S.PhoneOf(val)
		if val[0] != '+' || len(part) != 2 || len(trim)+1 != len(val) {
			mp.Ajax.Error(`Invalid international phone format (+xx xxxxxx): ` + val)
			return ``
		}
		mp.LogIt(key, val)
		mp.Row[key] = val
	}
	return X.ToS(mp.Row[key])
}

// get string from Row
func (mp *Row) Get_Str(key string) string {
	return X.ToS(mp.Row[key])
}

// get boolean from Row
func (mp *Row) Get_Bool(key string) bool {
	return X.ToBool(mp.Row[key])
}

// get int64 from Row
func (mp *Row) Get_Int(key string) int64 {
	return X.ToI(mp.Row[key])
}

// get []interface{} from Row
func (mp *Row) Get_Arr(key string) []interface{} {
	return X.ToArr(mp.Row[key])
}

// get float64 from Row
func (mp *Row) Get_Float(key string) float64 {
	return X.ToF(mp.Row[key])
}

// get id
func (mp *Row) Get_Id() int64 {
	return mp.Id
}

// get unique id
func (mp *Row) Get_UniqueId() string {
	return mp.UniqueId
}

// set date from Posts to Row
// unset when string is whitespace
func (mp *Row) SetUnsetDate(key string) string {
	val := mp.Posts.GetStr(key)
	return mp.SetUnsetValDate(key, val)
}

func (mp *Row) SetUnsetValDate(key string, val string) string {
	//	L.Describe(key, val)
	if val != `` {
		val = S.Trim(val)
		if val == `` {
			mp.Unset(key)
			return ``
		}
		val = S.XSS(val)
		val = S.Replace(val, `/`, `-`)
		d := S.Split(val, `-`)
		if len(d) != 3 {
			mp.Ajax.Error(`invalid date format for ` + key + ` parameter, value: ` + Z(val))
			return ``
		}
		d1 := S.ToI(d[0])
		d2 := S.ToI(d[1])
		d3 := S.ToI(d[2])
		if d1 > 1900 && d2 <= 12 && d3 <= 31 && d2 > 0 && d3 > 0 {
			// YYYY-MM-DD
			val = S.PadLeft(I.ToS(d1), `0`, 4) + `-` + S.PadLeft(I.ToS(d2), `0`, 2) + `-` + S.PadLeft(I.ToS(d3), `0`, 2)
		} else if d1 <= 31 && d2 <= 12 && d3 > 1900 && d1 > 0 && d2 > 0 {
			// DD-MM-YYYY
			val = S.PadLeft(I.ToS(d3), `0`, 4) + `-` + S.PadLeft(I.ToS(d2), `0`, 2) + `-` + S.PadLeft(I.ToS(d1), `0`, 2)
		} else {
			mp.Ajax.Error(`invalid date format for '` + key + `': ` + val)
			return ``
		}
		_, err := time.Parse(`2006-01-02`, val)
		if err != nil {
			mp.Ajax.Error(`invalid date for '` + key + `': ` + val)
			return ``
		}
		mp.LogIt(key, val)
		mp.Row[key] = val
	}
	return X.ToS(mp.Row[key])
}

// set time from Posts to Row
// unset when string is whitespace
func (mp *Row) SetUnsetTime(key string) string {
	val := mp.Posts.GetStr(key)
	return mp.SetUnsetValTime(key, val)
}

//
func (mp *Row) SetUnsetValTime(key string, val string) string {
	//	L.Describe(key, val)
	if val != `` {
		val = S.Trim(val)
		if val == `` {
			mp.Unset(key)
			return ``
		}
		val = S.Replace(val, `.`, `:`)
		hh_mm := S.Split(val, `:`)
		if len(hh_mm) < 2 {
			mp.Ajax.Error(`invalid format for '` + key + `': ` + val + `, time format must a HH:MM`)
			return ``
		}
		// check hours
		hh := S.ToI(hh_mm[0])
		if hh < 0 || hh > 23 {
			mp.Ajax.Error(`invalid hour for '` + key + `': ` + val)
			return ``
		}
		// check minutes
		mm := S.ToI(hh_mm[1])
		if mm < 0 || mm > 59 {
			mp.Ajax.Error(`invalid minute for '` + key + `': ` + val)
			return ``
		}
		val = S.PadLeft(I.ToS(hh), `0`, 2) + `:` + S.PadLeft(I.ToS(mm), `0`, 2)
		mp.LogIt(key, val)
		mp.Row[key] = val
	}
	return X.ToS(mp.Row[key])
}

// set dates from Posts to Row
func (mp *Row) SetDates(key string) {
	// TODO: validate this
	val := mp.Posts.GetStr(key)
	if val != `` {
		val = S.Trim(val)
		val = S.XSS(val)
		mp.LogIt(key, val)
		mp.Row[key] = val
	}
}

// set int64 from Posts to Row
func (mp *Row) SetInt(key string) int64 {
	val := mp.Posts.GetStr(key)
	if val != `` {
		mp.LogIt(key, val)
		val := mp.Posts.GetInt(key)
		mp.Row[key] = val
	}
	return X.ToI(mp.Row[key])
}

// set float64 from Posts to Row
func (mp *Row) SetFloat(key string) float64 {
	val := mp.Posts.GetStr(key)
	if val != `` {
		mp.LogIt(key, val)
		val := mp.Posts.GetFloat(key)
		mp.Row[key] = val
	}
	return X.ToF(mp.Row[key])
}

// set bool from Posts to Row
func (mp *Row) SetBool(key string) bool {
	val := mp.Posts.GetStr(key)
	if val != `` {
		mp.LogIt(key, val)
		mp.Row[key] = (val == `true`)
	}
	return X.ToBool(mp.Row[key])
}

// unset Row key
func (mp *Row) Unset(key string) {
	oldv, exists := mp.Row[key]
	if exists {
		mp.Log += ZZ(key) + ` removed, previously ` + ZZ(X.ToS(oldv)) + "\n<br/>"
		delete(mp.Row, key)
	}
}

// set unset int, returns 0 when unsetted
func (mp *Row) SetUnsetIntVal(key string, val int64) int64 {
	if val <= 0 {
		mp.Unset(key)
		return 0
	}
	mp.SetVal(key, val)
	return val
}

// set user password, skip logging
func (mp *Row) Set_UserPassword(pass string) {
	if pass != `` {
		mp.Log += ZZ(`password`) + ` changed` + "\n<br/>"
		mp.Row[`password`] = S.HashPassword(pass)
		mp.Row[`last_reset_password`] = T.DateTimeStr()
	}
}

// check password
func (mp *Row) Check_UserPassword(pass string) bool {
	return mp.Row.GetStr(`password`) == S.HashPassword(pass)
}

// set Row value
func (mp *Row) SetVal(key string, val interface{}) interface{} {
	switch v := val.(type) {
	case string:
		v = S.Trim(v)
		val = S.XSS(v)
	}
	mp.LogIt(key, val)
	mp.Row[key] = val
	return val
}

// set Row value with check
func (mp *Row) SetType1(val string) string {
	// error check
	if mp.Id > 0 {
		old_type := mp.Row.GetStr(`type`)
		if !(old_type == `` || old_type == val) {
			return mp.Ajax.Error(`Invalid record type, trying to overwrite a ` + Z(old_type) + ` record to ` + Z(val) + `, this should not be happened, please contact Site Administrator`)
		}
	}
	mp.SetVal(`type`, val)
	return ``
}

// check type should be inside certain types
func (mp *Row) SetType2(val string, types ...string) string {
	if mp.Id > 0 {
		old_type := mp.Row.GetStr(`type`)
		exists := old_type == ``
		for _, allowed := range types {
			if exists {
				break
			}
			exists = exists || old_type == allowed
		}
		if !exists {
			return mp.Ajax.Error(`Invalid record type, trying to overwrite a ` + Z(old_type) + ` record to ` + Z(val) + `, this should not be happened, please contact Site Administrator`)
		}
	}
	mp.SetVal(`type`, val)
	return ``
}

func (mp *Row) IsChanged() bool {
	return mp.Log != ``
}

// set val without XSS filtering
func (mp *Row) SetValNoXSS(key string, val interface{}) interface{} {
	mp.LogIt(key, val)
	mp.Row[key] = val
	return val
}

// set Row value if ok
func (mp *Row) SetValIf(ok bool, key string, val interface{}) {
	if ok {
		mp.SetVal(key, val)
	}
}

// set Row value from string
func (mp *Row) SetValStr(key, val string) {
	if val != `` {
		val = S.Trim(val)
		val = S.XSS(val)
		mp.LogIt(key, val)
		mp.Row[key] = val
	}
}

// set Row by current date
func (mp *Row) SetValNow(key string) string {
	val := T.NowStr()
	mp.LogIt(key, val)
	mp.Row[key] = val
	return val
}

func (mp *Row) SetValNow2(key string) string {
	val := T.NowStr2()
	mp.LogIt(key, val)
	mp.Row[key] = val
	return val
}

// set Row by current date epoch as float
func (mp *Row) SetValEpoch(key string) float64 {
	val := T.Epoch()
	mp.LogIt(key, val)
	mp.Row[key] = val
	return float64(val)
}

// set Row by current date epoch as float
func (mp *Row) SetValEpochOnce(key string) float64 {
	old_val := X.ToF(mp.Row[key])
	if old_val > 0 {
		return old_val
	}
	val := T.Epoch()
	mp.LogIt(key, val)
	mp.Row[key] = val
	return float64(val)
}

// set Row office_mail, gmail, yahoo and email
func (mp *Row) Set_UserEmails(emails string) (ok bool) {
	return mp.Set_UserEmails_ByTable(emails, `users`)
}

func (mp *Row) Set_UserEmails_ByTable(emails string, table string) (ok bool) {
	ok = true
	if emails != `` {
		emails = S.XSS(emails)
		emails = S.ToLower(emails)
		orig := M.SS{}
		orig[`office_mail`] = ``
		orig[`gmail`] = ``
		orig[`yahoo`] = ``
		orig[`email`] = ``
		mails := S.Split(emails, `,`)
		for _, mail := range mails {
			orig_mail := S.Trim(mail)
			mail = S.ValidateEmail(orig_mail)
			if mail == `` {
				L.Describe(`invalid e-mail`, orig_mail)
				continue
			}
			if S.EndWith(mail, OFFICE_MAIL_SUFFIX) {
				orig[`office_mail`] = mail
			} else if S.Contains(mail, `@gmail.`) {
				orig[`gmail`] = mail
			} else if S.Contains(mail, `@yahoo.`) ||
				S.Contains(mail, `@ymail.`) ||
				S.Contains(mail, `@rocketmail.`) {
				orig[`yahoo`] = mail
			} else {
				orig[`email`] = mail
			}
		}
		for _, key := range []string{`office_mail`, `gmail`, `yahoo`, `email`} {
			val := orig[key]
			mp.SetVal(key, val)
			if val == `` {
				continue
			}
			lkey, rkey, id_str := Z(key), Z(val), ZI(mp.Id)
			query := `SELECT COALESCE((SELECT id FROM ` + ZZ(table) + ` WHERE data->>` + lkey + ` = ` + rkey + ` AND id <> ` + id_str + ` AND is_deleted = false LIMIT 1),0)`
			dup_id := mp.Tx.QInt(query)
			if dup_id == 0 {
				continue
			}
			msg := `The ` + lkey + ` is being used by another user account: ` + rkey + `, if you think this should not be happened, please send a bug report to the WebMaster.` // ` = ` + ZI(dup_id) + ` <> ` + id_str
			L.Describe(msg)
			mp.Ajax.Error(msg)
			ok = false
		}
	}
	return
}
