// PUKIS' JSON Helper
package J

import (
	"encoding/json"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
)

/* Converting JSON string to map
func main() {
   m := `{"test":123,"bla":[1,2,3,4]}`
   L.Describe (J.ToMap(m))//M.SX{
                    //"test": float64(123),
                    //"bla":  []interface {}{
                    // float64(1),
                    //float64(2),
                    //float64(3),
                    //float64(4),
                    // },
                    //}
}
*/

// from json string
func ToMap(str string) (any M.SX) {
	any = M.SX{}
	err := json.Unmarshal([]byte(str), &any)
	L.PanicIf(err, `unable to parse JSON string`, str)
	return
}
