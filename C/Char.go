// PUKIS' Character Helper
package C

/* Checking whether the character is a digit or not
func main() {
   L.Describe(C.IsDigit('9'))
}
*/
func IsDigit(ch byte) bool {
	return ch >= '0' && ch <= '9'
}

/* Checking whether the character is a letter/underscore or not
func main() {
   L.Describe(C.IsIdentStart('-')) // output bool(false)
   L.Describe(C.IsIdentStart('_')) // bool(true)
}
*/
func IsIdentStart(ch byte) bool {
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == '_'
}

/* Checking whether the character is an alphanumeric (letter/underscore/numeral) or not
func main() {
   L.Describe(C.IsIdent('_')) // output bool(true)
   L.Describe(C.IsIdent('9')) // output bool(true)
}
*/
func IsIdent(ch byte) bool {
	return IsDigit(ch) || IsIdentStart(ch)
}

/* Checking whether the character is alphanumeric/comma/full-stop/dash
func main() {
   L.Describe(C.IsValidFilename(' ')) // output bool(true)
}
*/
func IsValidFilename(ch byte) bool {
	return ch == ' ' || IsIdent(ch) || ch == ',' || ch == '.' || ch == '-'
}
