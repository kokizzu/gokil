// PUKIS' Integer Helper
package I

import (
	"encoding/base64"
	"strconv"
	"strings"
	"time"
)

/* Return int64 number (second arg), if the condition (first arg) is true,
   Return 0, if the condition (first arg) is false
func main() {
   L.Describe(I.If(true,3))  // output int64(3)
   L.Describe(I.If(false,4)) // output int64(0)
}
*/
func If(b bool, yes int64) int64 {
	if b {
		return yes
	}
	return 0
}

/* Return int64 number (second arg), if the condition (first arg) is true,
   Return int64 number (second arg), if the condition (first arg) is false
func main() {
   L.Describe(I.IfElse(true,3,4))  // output int64(3)
   L.Describe(I.IfElse(false,3,4)) // output int64(4)
}
*/
func IfElse(b bool, yes, no int64) int64 {
	if b {
		return yes
	}
	return no
}

/*
func main() {
   L.Describe(I.ToS(56))    // output "56"
   L.Describe(I.ToStr(-56)) // output "56"
   L.Describe(I.UToS(56))   // output "56"
}
*/
// int64 to string
func ToS(num int64) string {
	return strconv.FormatInt(num, 10)
}

func UToS(num uint64) string {
	return strconv.FormatUint(num, 10)
}

// int to string
func ToStr(num int) string {
	return strconv.Itoa(num)
}

// int to date YYYY-MM-DD string
func ToDateStr(num int64) string {
	t := time.Unix(num/1000, 0)
	return t.Format(`2006-01-02`)
}

// int min
func Min(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

// int max
func Max(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

// int min
func Min2(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// int max
func Max2(a, b int) int {
	if a > b {
		return a
	}
	return b
}

/* Adding ordinal number suffix such as st, nd, rd, and th.
   Return as a string.
func main() {
   L.Describe(I.ToEnglishNumerals(241)) // output "241st"
   L.Describe(I.ToEnglishNumerals(242)) // output "242nd"
   L.Describe(I.ToEnglishNumerals(244)) // output "244th"
}
*/
func ToEnglishNumerals(num int64) string {
	if num < 0 {
		return ``
	}
	prefix := ToS(num)
	n2 := num % 100
	num %= 10
	if n2 == 11 || n2 == 12 || n2 == 13 {
		prefix += `th`
	} else if num == 1 {
		prefix += `st`
	} else if num == 2 {
		prefix += `nd`
	} else if num == 3 {
		prefix += `rd`
	} else {
		prefix += `th`
	}
	return prefix
}

/* It converts int64 (first arg) to string, then
   adding string "0" in front of the converted string until
   the length (number of character) of the converted string
   reach a certain integer number (second arg).

/*
func main() {
   L.Describe(I.ToZeroPad(123,5)) // output "00123"
}
*/
func ToZeroPad(num int64, length int) string {
	str := ToS(num)
	slen := len(str)
	if slen >= length {
		return str
	}
	return strings.Repeat(`0`, length-slen) + str
}

var romanFig = []int64{100000, 10000, 1000, 100, 10, 1}

var romanI, romanV map[int64]rune

func init() { // M == ↀ
	romanI = map[int64]rune{1: 'I', 10: 'X', 100: 'C', 1000: 'M', 10000: 'ↂ', 100000: 'ↈ'}
	romanV = map[int64]rune{1: 'V', 10: 'L', 100: 'D', 1000: 'ↁ', 10000: 'ↇ'}
}

/* Convert int64 to roman number in string type
func main() {
   L.Describe(I.ToRoman(16)) // output "XVI"
}
*/
func ToRoman(num int64) string {
	res := []rune{}
	x := ' '
	for _, z := range romanFig {
		digit := num / z
		i, v := romanI[z], romanV[z]
		switch digit {
		case 1:
			res = append(res, i)
		case 2:
			res = append(res, i, i)
		case 3:
			res = append(res, i, i, i)
		case 4:
			res = append(res, i, v)
		case 5:
			res = append(res, v)
		case 6:
			res = append(res, v, i)
		case 7:
			res = append(res, v, i, i)
		case 8:
			res = append(res, v, i, i, i)
		case 9:
			res = append(res, i, x)
		}
		num -= digit * z
		x = i
	}
	return string(res)
}

/* Convert int64 to base64 number in string type, for unsigned number.
func main() {
   L.Describe(I.UToBase64(16)) // output "MTY="
}
*/
// encode integer to base64
func UToBase64(val uint64) string {
	buf := []byte(UToS(val))
	return base64.StdEncoding.EncodeToString(buf)
}

/* Convert int64 to base64 number in string type, for signed number.
func main() {
   L.Describe(I.ToBase64(-16)) // output"LTE2"
}
*/
func ToBase64(val int64) string {
	buf := []byte(ToS(val))
	return base64.StdEncoding.EncodeToString(buf)
}
