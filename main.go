package main

import (
	"os"

	_ "gitlab.com/kokizzu/gokil/A" // array
	_ "gitlab.com/kokizzu/gokil/B" // bool
	_ "gitlab.com/kokizzu/gokil/C" // character
	_ "gitlab.com/kokizzu/gokil/D" // database
	_ "gitlab.com/kokizzu/gokil/F" // float
	_ "gitlab.com/kokizzu/gokil/I" // integer
	_ "gitlab.com/kokizzu/gokil/J" // json
	_ "gitlab.com/kokizzu/gokil/K" // others
	_ "gitlab.com/kokizzu/gokil/L" // logs
	_ "gitlab.com/kokizzu/gokil/M" // map
	_ "gitlab.com/kokizzu/gokil/S" // string
	_ "gitlab.com/kokizzu/gokil/T" // time
	_ "gitlab.com/kokizzu/gokil/W" // web
	_ "gitlab.com/kokizzu/gokil/X" // anything
	_ "gitlab.com/kokizzu/gokil/Z" // z-template engine
	// `github.com/kokizzu/gin`
	// `github.com/kokizzu/colorgo`
	// `github.com/pwaller/goupx`
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = `gokil`
	app.Usage = `GO Keen for Improvement Library`
	app.Commands = []cli.Command{
		{
			Name:    `init`,
			Aliases: []string{`i`},
			Usage:   `initialize project`,
			Action: func(c *cli.Context) {
				os.Mkdir(`ajax`, 0750)
				os.Mkdir(`model`, 0750)
				os.Mkdir(`page`, 0750)
				os.MkdirAll(`public/js`, 0750)
				os.MkdirAll(`public/css`, 0750)
				os.Mkdir(`views`, 0750)
			},
		},
	}

	app.Run(os.Args)
}
