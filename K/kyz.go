// PUKIS' Escaping and Json5 Helper
package K

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"github.com/nfnt/resize"
	"github.com/yosuke-furukawa/json5/encoding/json5"
	"gitlab.com/kokizzu/gokil/A"
	"gitlab.com/kokizzu/gokil/B"
	"gitlab.com/kokizzu/gokil/C"
	"gitlab.com/kokizzu/gokil/F"
	"gitlab.com/kokizzu/gokil/I"
	"gitlab.com/kokizzu/gokil/L"
	"gitlab.com/kokizzu/gokil/M"
	"gitlab.com/kokizzu/gokil/S"
	"gitlab.com/kokizzu/gokil/T"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"reflect"
	"runtime"
	"strings"
	"time"
)

var NUM_CPU float64
var CPU_STAT2, CPU_STAT4, CPU_STAT5, LAST_STAT7 int64
var CPU_PERCENT, RAM_PERCENT float64
var LAST_CPU_CALL, LAST_RAM_CALL int64

func init() {
	gob.Register(M.SB{})
	gob.Register(M.SX{})
	gob.Register(M.IB{})
	gob.Register(M.IX{})
	gob.Register(M.SI{})
	NUM_CPU = float64(runtime.NumCPU())
	PercentCPU()
}

/*
func main() {
    m:=M.SX{`satu`:1}
    L.Describe(K.ToGOB64(m)) // output "Ev+BBAEBAlNYAf+CAAEMARAAABH/ggABBHNhdHUDaW50BAIAAg=="
}
*/
// go binary encoder
func ToGOB64(m M.SX) string {
	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	err := e.Encode(m)
	L.PanicIf(err, `failed K.ToGOB64`, m, b.Bytes())
	return b.String()
}

/*
func main() {
    L.Describe(K.FromGOB64("Ev+BBAEBAlNYAf+CAAEMARAAABH/ggABBHNhdHUDaW50BAIAAg=="))// output "Ev+BBAEBAlNYAf+CAAEMARAAABH/ggABA3RlcwNpbnQEAwD/9g=="
    // output
    // M.SX{
    //     "satu": int(1),
    // }
}
*/
// go binary decoder
func FromGOB64(str string) M.SX {
	m := M.SX{}
	b := bytes.Buffer{}
	b.WriteString(str)
	d := gob.NewDecoder(&b)
	err := d.Decode(&m)
	L.PanicIf(err, `failed K.FromGOB64`)
	return m
}

func json5fromMIB(orig map[int64]bool) string {
	b := bytes.Buffer{}
	b.WriteByte('{')
	first := true
	for k, v := range orig {
		if !first {
			b.WriteByte(',')
		} else {
			first = false
		}
		b.WriteString(I.ToS(k))
		b.WriteByte(':')
		b.WriteString(ToJson5(v))
	}
	b.WriteByte('}')
	return b.String()
}

func json5fromMIX(orig map[int64]interface{}) string {
	b := bytes.Buffer{}
	b.WriteByte('{')
	first := true
	for k, v := range orig {
		if !first {
			b.WriteByte(',')
		} else {
			first = false
		}
		b.WriteString(I.ToS(k))
		b.WriteByte(':')
		b.WriteString(ToJson5(v))
	}
	b.WriteByte('}')
	return b.String()
}

func json5fromMIAX(orig map[int64][]interface{}) string {
	b := bytes.Buffer{}
	b.WriteByte('{')
	first := true
	for k, v := range orig {
		if !first {
			b.WriteByte(',')
		} else {
			first = false
		}
		b.WriteString(I.ToS(k))
		b.WriteByte(':')
		b.WriteString(ToJson5(v))
	}
	b.WriteByte('}')
	return b.String()
}

func json5fromMSAX(orig map[string][]interface{}) string {
	b := bytes.Buffer{}
	b.WriteByte('{')
	first := true
	for k, v := range orig {
		if !first {
			b.WriteByte(',')
		} else {
			first = false
		}
		b.WriteString(S.ZZ(k))
		b.WriteByte(':')
		b.WriteString(ToJson5(v))
	}
	b.WriteByte('}')
	return b.String()
}

func json5fromMSI(orig map[string]int64) string {
	b := bytes.Buffer{}
	b.WriteByte('{')
	first := true
	for k, v := range orig {
		if !first {
			b.WriteByte(',')
		} else {
			first = false
		}
		quote := true
		if len(k) > 0 {
			ch := k[0]
			if C.IsDigit(ch) && ch != '0' {
				for _, ch := range k[1:] {
					// find non digit
					if !C.IsDigit(uint8(ch)) {
						quote = true
						break
					}
				}
			} else if C.IsIdentStart(k[0]) {
				for _, ch := range k[1:] {
					// find non identifier character
					if !C.IsIdent(uint8(ch)) {
						quote = true
						break
					}
				}
			} else {
				quote = true
			}
		}
		if quote {
			k = S.Q(k)
		}
		b.WriteString(k)
		b.WriteByte(':')
		b.WriteString(I.ToS(v))
	}
	b.WriteByte('}')
	return b.String()
}

func ToJson5(any interface{}) string {
	// bug when using map[int64]interface{}
	if any == nil {
		return `''`
	}
	switch orig := any.(type) {
	case bytes.Buffer: // return as is
		return orig.String()
	case string:
		return S.ZJ(orig)
	case []byte:
		return S.ZJ(string(orig))
	case int, int64, int32:
		return I.ToS(any.(int64))
	case float32, float64:
		return F.ToS(any.(float64))
	case bool:
		return B.ToS(orig)
	case M.IB:
		return json5fromMIB(orig)
	case map[int64]bool:
		return json5fromMIB(orig)
	case M.IX:
		return json5fromMIX(orig)
	case map[int64]interface{}:
		return json5fromMIX(orig)
	case M.IAX:
		return json5fromMIAX(orig)
	case map[int64][]interface{}:
		return json5fromMIAX(orig)
	case M.SAX:
		return json5fromMSAX(orig)
	case map[string][]interface{}:
		return json5fromMSAX(orig)
	case M.SX:
		return orig.ToJson()
	case map[string]interface{}:
		return M.ToJson(orig)
	//   return any.(M.SX).ToJson()
	case M.SI:
		return json5fromMSI(orig)
	case map[string]int64:
		return json5fromMSI(orig)
	case A.X:
		return A.ToJson(orig)
	case []interface{}:
		return A.ToJson(orig)
	default:
		str, err := json.Marshal(any)
		L.PanicIf(err, `K.ToJson5 failed`, any)
		return string(str)
	}
	// TODO: add more types (M/A) here, do not EVER TRY to use reflection in this case
}

/*
func main() {
    m:= []interface {}{true,`1`,23,`[abc]`}
    L.Print(K.ToJsonHTML(m)) // output [<br/> &nbsp; true,<br/> &nbsp; "1",<br/> &nbsp; 23,<br/> &nbsp; "[abc]"<br/>]
}
*/
// convert to beautiful json html
func ToJsonHTML(any interface{}) string {
	res, err := json.MarshalIndent(any, ``, ` &nbsp; `)
	L.PanicIf(err, `K.ToJsonHTML failed`, any)
	return S.Replace(string(res), "\n", `<br/>`)
}

/*
func main() {
    m:= []interface {}{true,`1`,23,`[abc]`}
    L.Print(K.ToJsonPretty(m))
    // output
    // [
    //   true,
    //   "1",
    //   23,
    //  "[abc]"
    // ]
}
*/
// convert to beautiful json text
func ToJsonPretty(any interface{}) string {
	res, err := json.MarshalIndent(any, ``, `  `)
	L.PanicIf(err, `K.ToJsonPretty failed`, any)
	return string(res)
}

// convert to standard json text
func ToJson(any interface{}) string {
	res, err := json.Marshal(any)
	L.PanicIf(err, `K.ToJson failed`, any)
	return string(res)
}

// encode LogKey
/*    0..9   -> 0..9
      10..35 -> a..z
      36..61 -> A..Z
      62     -> _
      63     -> -
*/
/*
func main() {
   L.Print(K.EncodeLogKey(10)) // output a
   L.Print(K.EncodeLogKey(1))  // output 1
}
*/
func EncodeLogKey(id int64) string {
	str := make([]byte, 0, 12)
	if id == 0 {
		return `0`
	}
	for id > 0 {
		var ch rune
		mod := rune(id % 64)
		if mod < 10 {
			ch = '0' + mod
		} else if mod < 10+26 {
			ch = 'a' + (mod - 10)
		} else if mod < 10+26+26 {
			ch = 'A' + (mod - 10 - 26)
		} else if mod%2 == 0 {
			ch = '_'
		} else {
			ch = '-'
		}
		if ch == ':' {
			L.Describe(mod, ':')
		}
		str = append(str, byte(ch))
		id /= 64
	}
	return string(str)
}

/*
func main() {
   L.Print(K.RandLogKey(11)) // output iRf-74ywBR4fpNfvZwCyx7t8nBCtsjgk53MnjXJb17w3hbKN95oxqp3ozm7dooQFv5u0w5VBPOCxkadNyMGQ8g1wugaVwQvFNpKuDogWh4q24QRxPrcvO74
}
*/
func RandLogKey(n int64) string {
	res := ``
	for z := int64(0); z < n; z++ {
		res += EncodeLogKey(rand.Int63())
	}
	return res
}

/*
TODO: bug?
*/
// decode LogKey
func DecodeLogKey(logKey string) (int64, error) {
	res := int64(0)
	for _, ch := range logKey {
		res *= 64
		mod := int64(ch)
		if ch >= '0' && ch <= '9' {
			res += (mod - '0')
		} else if ch >= 'a' && ch <= 'z' {
			res += 10 + (mod - 'a')
		} else if ch >= 'A' && ch <= 'Z' {
			res += 10 + 26 + (mod - 'A')
		} else if ch == '_' {
			res += 10 + 26 + 26
		} else if ch == '-' {
			res += 10 + 26 + 26 + 1
		} else {
			return res, fmt.Errorf(`Invalid logKey character: '%c' (%d)`, ch, ch)
		}
	}
	return res, nil
}

/* Validate JSON
func main() {
    json := `{"test":123,1:"satu"}`
   L.Print(K.ValidateJson(json))
    // output
    // {
    //    "1": "satu",
    //    "test": 123
    // }
}
*/
// validate JSON
func ValidateJson(str string) string {
	var res map[string]interface{}
	err := json5.Unmarshal([]byte(str), &res)
	L.PanicIf(err, `decoding JSON failed: %s`, str)
	buf, err := json5.MarshalIndent(res, ``, `    `)
	L.PanicIf(err, `encoding JSON failed: %# v`, res)
	return string(buf)
}

// execute command and return output
func RunCmd(cmd string, args ...string) (output []byte) {
	var err error
	fullcmd := `RunCmd: ` + cmd + ` `
	if len(args) > 0 {
		fullcmd += `'` + A.StrJoin(args, `' '`) + `' `
	}
	L.Describe(fullcmd)
	output, err = exec.Command(cmd, args...).CombinedOutput()
	if err != nil {
		out_str := string(output)
		L.Describe(fullcmd, err)
		output = []byte(err.Error())
		fmt.Println(out_str)
	}
	return
}

// run cmd and pipe to stdout
func PipeRunCmd(cmd string, args ...string) error {
	fullcmd := `RunCmd: ` + cmd + ` `
	if len(args) > 0 {
		fullcmd += `'` + A.StrJoin(args, `' '`) + `' `
	}
	L.Describe(fullcmd)
	exe := exec.Command(cmd, args...)
	exe.Stdout = os.Stdout
	exe.Stderr = os.Stderr
	return exe.Run()
}

// generate report using Ruby, the ruby script should use
func RunRuby(ruby_script string, json M.SX) (link string) {
	tmp := RandLogKey(3)
	fname := `/tmp/` + tmp + `.json`
	err := ioutil.WriteFile(fname, []byte(json.ToJson()), 0644)
	L.PanicIf(err, `failed to generate temporary file`, fname)
	last := ``
	T.Track(func() {
		out := RunCmd(`ruby/`+ruby_script+`.rb`, fname)
		out_str := string(out)
		lines := S.Split(out_str, "\n")
		last = lines[len(lines)-1]
		L.Describe(fname)
		fmt.Println(out_str)
	})
	return last
}

// generate report using PHP, the ruby script should use
func RunPhp(ruby_script string, json M.SX) (link string) {
	tmp := RandLogKey(3)
	fname := `/tmp/` + tmp + `.json`
	err := ioutil.WriteFile(fname, []byte(json.ToJson()), 0644)
	L.PanicIf(err, `failed to generate temporary file`, fname)
	last := ``
	T.Track(func() {
		out := RunCmd(`php/`+ruby_script+`.php`, fname)
		out_str := string(out)
		lines := S.Split(out_str, "\n")
		last = lines[len(lines)-1]
		L.Describe(fname)
		fmt.Println(out_str)
	})
	return last
}

// convertImageToJPEG converts from PNG to JPEG.
func ResizeImage(w io.Writer, r io.Reader, src_is_png bool) error {
	return ResizeImage_WithMaxSize(w, r, src_is_png, 480) // PU
}

// convertImageToJPEG converts from PNG to JPEG.
func ResizeImage_WithMaxSize(w io.Writer, r io.Reader, src_is_png bool, max_wh uint) error {
	var img image.Image
	var err error
	if src_is_png {
		img, err = png.Decode(r)
	} else {
		img, _, err = image.Decode(r)
	}
	if err != nil {
		return err
	}
	// resize
	rect := img.Bounds()
	point := rect.Size()
	nw, nh := uint(point.X), uint(point.Y)
	if nw > nh {
		if nw > max_wh {
			nh = nh * max_wh / nw
			nw = max_wh
		}
	} else {
		if nh > max_wh {
			nw = nw * max_wh / nh
			nh = max_wh
		}
	}
	img = resize.Resize(nw, nh, img, resize.Lanczos3)
	// encode to jpg
	return jpeg.Encode(w, img, &jpeg.Options{Quality: 100})
}

func FunctionName(fun interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(fun).Pointer()).Name()
}
func FunctionAddr(fun interface{}) uintptr {
	return runtime.FuncForPC(reflect.ValueOf(fun).Pointer()).Entry()
}

func PercentCPU() float64 {
	last_cpu_call := time.Now().Unix()
	if last_cpu_call <= LAST_CPU_CALL {
		return CPU_PERCENT
	}
	LAST_CPU_CALL = last_cpu_call
	fs, err := os.Open("/proc/stat") // usage
	if err != nil {
		return -1
	}
	defer fs.Close()
	scanner := bufio.NewScanner(fs)
	if !scanner.Scan() {
		return -1
	}
	cpu_stat := strings.Fields(scanner.Text())
	if len(cpu_stat) < 7 {
		return -1
	}
	l7 := S.ToI(cpu_stat[7])
	if l7 > LAST_STAT7 {
		s13 := S.ToI(cpu_stat[2-1])
		s15 := S.ToI(cpu_stat[4-1])
		s16 := S.ToI(cpu_stat[5-1])
		percent := float64(s13-CPU_STAT2+s15-CPU_STAT4) * 100 / float64(s13-CPU_STAT2+s15-CPU_STAT4+s16-CPU_STAT5) / float64(l7-LAST_STAT7)
		CPU_PERCENT = percent
		LAST_STAT7 = l7
		CPU_STAT2 = s13
		CPU_STAT4 = s15
		CPU_STAT5 = s16
	}
	return CPU_PERCENT
}

func PercentRAM() float64 {
	last_ram_call := time.Now().Unix()
	if last_ram_call <= LAST_RAM_CALL {
		return RAM_PERCENT
	}
	LAST_RAM_CALL = last_ram_call
	fs, err := os.Open(`/proc/meminfo`) // usage
	if err != nil {
		return -1
	}
	defer fs.Close()
	scanner := bufio.NewScanner(fs)
	var ram [3]float64
	for x := 0; x < 3; x++ {
		if !scanner.Scan() {
			return -1
		}
		cols := strings.Fields(scanner.Text())
		ram[x] = S.ToF(cols[1])
	}
	RAM_PERCENT = (1 - ram[2]/ram[0]) * 100
	return RAM_PERCENT
}
