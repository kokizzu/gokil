// PUKIS' Boolean Helper
package B

/* It convert boolean type to string type, writing "true" or "false"
func main() {
   fmt.Println(B.ToS(true))  // output "true"
   fmt.Println(B.ToS(false)) // output "false"
}
*/
func ToS(b bool) string {
	if b {
		return `true`
	}
	return `false`
}
