// PUKIS' Logging Helper
package L

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/fatih/color"
	"github.com/kr/pretty"
	"github.com/op/go-logging"
	"gitlab.com/kokizzu/gokil/I"
	"net/http"
	"os"
	"os/user"
	"runtime"
	"runtime/debug"
	"strings"
	"time"
)

const DEBUG = false
const BR = "\n"
const BR2 = "\n\n"

var ENABLE_TELEGRAM_LOG bool
var FILE_PATH string
var GO_PATH string
var GO_ROOT string
var BgRed, BgGreen (func(format string, a ...interface{}) string)
var LOG *logging.Logger
var TIMETRACK_MIN_DURATION float64

type TelegramMessageBody struct {
	ChatID int64  `json:"chat_id"`
	Text   string `json:"text"`
}

const TELEGRAM_ID = `bot1881113574:AAHRhsRxjez_0eoTvSaP8T2D2gzES0xHBJg`

// initialize logger
func init() {
	_, file, _, _ := runtime.Caller(1)
	FILE_PATH = file[:4+strings.Index(file, `/src/`)]
	GO_PATH = os.Getenv(`GOPATH`)
	GO_ROOT = os.Getenv(`GOROOT`)
	spew.Config.Indent = `  `
	LOG = logging.MustGetLogger(`[KYZ]`)
	backend := logging.NewLogBackend(os.Stderr, ``, 0)
	format := logging.MustStringFormatter(
		`%{color}%{time:2006-01-02 15:04:05.000} %{shortfunc} ▶%{color:reset} %{message}`,
	)
	formatter := logging.NewBackendFormatter(backend, format)
	logging.SetBackend(formatter)
	BgGreen = color.New(color.BgGreen).SprintfFunc()
	BgRed = color.New(color.BgRed).SprintfFunc()
	TIMETRACK_MIN_DURATION = 100
	ENABLE_TELEGRAM_LOG = false
}

func StackTrace(start int) string {
	str := ``
	for {
		pc, file, line, ok := runtime.Caller(start)
		name := runtime.FuncForPC(pc).Name()
		if !ok || strings.HasPrefix(name, `main.`) {
			break
		}
		start += 1
		if strings.HasPrefix(name, `runtime.`) || strings.HasPrefix(name, `gitlab.com/kokizzu/gokil/L.`) {
			continue
		}
		str += "\n\t" + file[len(FILE_PATH):] + `:` + I.ToStr(line) + `  ` + name
	}
	return str
}

func CensoredStackTrace(start int) string {
	return string(CensorString(StackTrace(start + 1)))
}

/*
func main(){
    L.PanicIf(errors.New(`SATU`),`DUA`)
	// output go run cobaLog.go
	// 2016-03-18 13:41:24.617 PanicIf ▶ /gitlab.com/kokizzu/gokil/cobaLog.go:31:  main.main: DUA
	// 2016-03-18 13:41:24.618 PanicIf ▶ &errors.errorString{s:"SATU"}
	// StackTrace:
	//  panic: SATU
	//  goroutine 1 [running]:
	//  gitlab.com/kokizzu/gokil/L.PanicIf(0x7fe910f377c0, 0xc20800a9e0, 0x56df90, 0x3, 0x0, 0x0, 0x0)
	//   /home/bintang/go/src/gitlab.com/kokizzu/gokil/L/Log.go:82 +0x595
	//  main.main()
	//   /home/bintang/go/src/gitlab.com/kokizzu/gokil/cobaLog.go:31 +0xf7
	// }
*/
// print error message and exit program
func PanicIf(err error, msg string, args ...interface{}) {
	if err == nil {
		return
	}
	pc, file, line, _ := runtime.Caller(1)
	strf := file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `
	str := color.MagentaString(strf)
	strf2 := ` ` + runtime.FuncForPC(pc).Name() + `: `
	str += color.YellowString(strf2)
	LOG.Criticalf(str+msg, args...)
	stt := StackTrace(3)
	res := pretty.Formatter(err)
	param := ``
	if len(args) > 0 {
		param = args[0].(string)
	}
	LOG.Criticalf("%# v\n    StackTrace: %s", res, stt)
	if ENABLE_TELEGRAM_LOG {
		err_tele := err.Error() + " \n " + msg + " \n " + stt + " \n " + param
		err_telegram := LogTelegram(err_tele)
		if err_telegram != nil {
			fmt.Print(`Failed send telegeram. `, err_telegram)
		}
	}
	panic(fmt.Errorf(err.Error()+BR2+fmt.Sprintf("%# v"+BR2+"    StackTrace: %s", res, stt)+BR2+strf+strf2+BR2+msg, args...))
}

func LogTelegram(msg string) error {
	user, err := user.Current()
	if err != nil {
		return err
	}
	username := user.Username
	if username != `web` { // only run on server
		return nil
	}
	reqBody := &TelegramMessageBody{
		// -226478574 : chat group softdev
		// -1001361356076: channel PUIS2
		ChatID: -1001361356076,
		Text:   msg,
	}
	reqBytes, err := json.Marshal(&reqBody)
	if err != nil {
		return err
	}
	uri := `https://api.telegram.org/` + TELEGRAM_ID + `/sendMessage`

	res, err := http.Post(uri, "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}
	return nil
}

/*
func main(){
    L.Describe(L.CensorString(`error`)) // output []uint8{0x65, 0x72, 0x72, 0x6f, 0x72}
}
*/
// remove unnecessary source code information
func CensorString(haystack string) []byte {
	//	re := regexp.MustCompile(`(?m)^.*` + GO_ROOT + `.*$[\r\n]+`)
	//	haystack = re.ReplaceAllString(haystack, ``)
	buf := []byte(haystack)
	buf = bytes.Replace(buf, []byte(`github.com/`), nil, -1)
	buf = bytes.Replace(buf, []byte(FILE_PATH), nil, -1)
	buf = bytes.Replace(buf, []byte(GO_PATH), nil, -1)
	//buf = bytes.Replace(buf, []byte(`.go:`), []byte(`.c:`), -1)
	buf = bytes.Replace(buf, []byte(`goexit`), []byte(`exit`), -1)
	return buf
}

/*
func main(){
    L.Describe (L.DebugStack())
    // output
    // []uint8{0x2f, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d,
    // 0x2f, 0x6b, 0x6f, 0x6b, 0x69, 0x7a, 0x7a, 0x75, 0x2f, 0x67, 0x6f, 0x6b, 0x69, 0x6c, 0x2f, 0x4c, 0x2f, 0x4c,
    // 0x6f, 0x67, 0x2e, 0x67, 0x6f, 0x3a, 0x39, 0x36, 0x20, 0x28....dst
}
*/
// print cleaned up debug stack
func DebugStack() []byte {
	stack := debug.Stack()
	return CensorString(string(stack))
}

/*
func main(){
    L.IsError(errors.New(`SATU`),`DUA`)
    L.Describe()//output /gitlab.com/kokizzu/gokil/cobaLog.go:42:  main.main: DUA
                //2016-03-18 13:35:05.301 IsError ▶ &errors.errorString{s:"SATU"}
                //2016-03-18 13:35:05.301 IsError ▶ &errors.errorString{s:"SATU"}
                StackTrace:
                2016-03-18 13:34:41.007 Describe ▶ /gitlab.com/kokizzu/gokil/cobaLog.go:43:  main.main
                }
*/
// print warning message
func IsError(err error, msg string, args ...interface{}) bool {
	if err == nil {
		return false
	}
	pc, file, line, _ := runtime.Caller(1)
	str := color.MagentaString(file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `)
	str += color.YellowString(` ` + runtime.FuncForPC(pc).Name() + `: `)
	LOG.Errorf(str+msg, args...)
	res := pretty.Formatter(err)
	LOG.Errorf("%# v\n", res)
	str = StackTrace(3)
	res = pretty.Formatter(err)
	LOG.Criticalf("%# v\n    StackTrace: %s", res, str)
	return true
}

/*
func main(){
    L.CheckIf(true,`DUA`)  // output /gitlab.com/kokizzu/gokil/cobaLog.go:69:  main.main: DUA
                           // 2016-03-18 13:47:05.039 Describe ▶ /gitlab.com/kokizzu/gokil/cobaLog.go:70:  main.main
    L.Describe()
    L.CheckIf(false,`DUA`) // output 2016-03-18 13:49:32.951 Describe ▶ /gitlab.com/kokizzu/gokil/cobaLog.go:73:  main.main
    L.Describe()
}
*/
// print warning message
func CheckIf(has_error bool, msg string, args ...interface{}) bool {
	if !has_error {
		return has_error
	}
	pc, file, line, _ := runtime.Caller(1)
	str := color.MagentaString(file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `)
	str += color.YellowString(` ` + runtime.FuncForPC(pc).Name() + `: `)
	LOG.Errorf(str+msg, args...)
	return has_error
}

/*
func main(){
    L.CheckIf2(true,`DUA`)  // output  runtime.main: DUA
                            // 2016-03-18 14:03:01.233 Describe ▶ /gitlab.com/kokizzu/gokil/cobaLog.go:79:  main.main
    L.Describe()
    L.CheckIf2(false,`DUA`) // output 2016-03-18 14:03:01.233 Describe ▶ /gitlab.com/kokizzu/gokil/cobaLog.go:81:  main.main
    L.Describe()
}
*/
func CheckIf2(has_error bool, msg string, args ...interface{}) bool {
	if !has_error {
		return has_error
	}
	pc, file, line, _ := runtime.Caller(2)
	str := color.MagentaString(file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `)
	str += color.YellowString(` ` + runtime.FuncForPC(pc).Name() + `: `)
	LOG.Errorf(str+msg, args...)
	return has_error
}

/*
	L.Describe(L.FunctionName()) // output "/gitlab.com/kokizzu/gokil/cobaLog.go:22: main.main"
*/
// get function name
func FunctionName() string {
	pc, file, line, _ := runtime.Caller(1)
	return file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: ` + runtime.FuncForPC(pc).Name()
}

/*
func main(){
    L.PrintFunctionName(`b`,`j`) // output b /gitlab.com/kokizzu/gokil/cobaLog.go:42: main.main j
    L.Describe()
}
*/
func PrintFunctionName(prefix, suffix string) {
	pc, file, line, _ := runtime.Caller(1)
	str := prefix + ` ` + file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: ` + runtime.FuncForPC(pc).Name() + ` ` + suffix
	fmt.Println(str)
}

// trace a function call
func Trace() {
	if !DEBUG {
		return
	}
	pc, file, line, _ := runtime.Caller(1)
	str := ` [TRACE] ` + file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: ` + runtime.FuncForPC(pc).Name() + ` `
	fmt.Println(str)
}

// describe anything
func Describe(args ...interface{}) {
	pc, file, line, _ := runtime.Caller(1)
	prefix := ``
	if len(file) >= len(FILE_PATH) {
		prefix = file[len(FILE_PATH):]
	}
	str := color.CyanString(prefix + `:` + I.ToStr(line) + `: `)
	str += color.YellowString(` ` + runtime.FuncForPC(pc).Name() + "\n")
	for _, arg := range args {
		//res, _ := json.MarshalIndent(variable, `   `, `  `)
		res := pretty.Formatter(arg)
		str += fmt.Sprintf("\t%# v\n", res)
	}
	LOG.Debug(strings.Replace(str, `%`, `%%`, -1))
}

// describe anything as HTMl []byte
func HtmlDescribe(args ...interface{}) []byte {
	buf := bytes.NewBuffer(nil)
	pc, file, line, _ := runtime.Caller(1)
	str := `<b>` + file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `
	buf.WriteString(str)
	str = runtime.FuncForPC(pc).Name() + ` </b><pre><br/>`
	buf.WriteString(str)
	for _, arg := range args {
		//res, _ := json.MarshalIndent(variable, `   `, `  `)
		res := pretty.Formatter(arg)
		str = fmt.Sprintf(`%# v`, res)
		buf.WriteString(str)
	}
	buf.WriteString(`</pre>`)
	return buf.Bytes()
}

// ddescribe anything as text []byte
func TextDescribe(args ...interface{}) []byte {
	buf := bytes.NewBuffer(nil)
	pc, file, line, _ := runtime.Caller(1)
	str := `` + file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `
	buf.WriteString(str)
	str = runtime.FuncForPC(pc).Name() + "\n"
	buf.WriteString(str)
	for _, arg := range args {
		//res, _ := json.MarshalIndent(variable, `   `, `  `)
		res := pretty.Formatter(arg)
		str = fmt.Sprintf("\t%# v", res)
		buf.WriteString(str)
	}
	return buf.Bytes()
}

// explain a variable
func Explain(args ...interface{}) {
	pc, file, line, _ := runtime.Caller(1)
	str := color.CyanString(file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `)
	str += runtime.FuncForPC(pc).Name() + "\n"
	for _, arg := range args {
		res := spew.Sdump(arg)
		str += "\t" + res
	}
	LOG.Warning(strings.Replace(str, `%`, `%%`, -1))
}

// explain as html []byte
func HtmlExplain(args ...interface{}) []byte {
	buf := bytes.NewBuffer(nil)
	pc, file, line, _ := runtime.Caller(1)
	str := fmt.Sprintf(`<b>%s:%d: `, file[len(FILE_PATH):], line)
	buf.WriteString(str)
	str = fmt.Sprintf(` %s </b><pre><br/>`, runtime.FuncForPC(pc).Name())
	buf.WriteString(str)
	for _, arg := range args {
		res := spew.Sdump(arg)
		buf.WriteString(res)
	}
	buf.WriteString(`</pre>`)
	return buf.Bytes()
}

// explain as text []byte
func TextExplain(args ...interface{}) []byte {
	buf := bytes.NewBuffer(nil)
	pc, file, line, _ := runtime.Caller(1)
	str := fmt.Sprintf(`%s:%d: `, file[len(FILE_PATH):], line)
	buf.WriteString(str)
	str = fmt.Sprintf(` %s `, runtime.FuncForPC(pc).Name())
	buf.WriteString(str)
	for _, arg := range args {
		res := spew.Sdump(arg)
		buf.WriteString(res)
	}
	return buf.Bytes()
}

// return elapsed time in ms, show 1st level, returns in ms
func TimeTrack(start time.Time, name string) float64 {
	_, file, line, _ := runtime.Caller(1)
	prefix := color.YellowString(file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `)
	elapsed := float64(time.Since(start).Nanoseconds()) / 1000000.0
	if elapsed < TIMETRACK_MIN_DURATION {
		return elapsed
	}
	suffix := color.GreenString(`%.2f ms`, elapsed)
	LOG.Noticef(prefix+"%s "+suffix, name)
	return elapsed
}

// return elapsed time in ms, show 3nd level, returns in ms
func LogTrack(start time.Time, name string) float64 {
	_, file, line, _ := runtime.Caller(3)
	prefix := color.CyanString(file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `)
	elapsed := float64(time.Since(start).Nanoseconds()) / 1000000.0
	if elapsed < TIMETRACK_MIN_DURATION {
		return elapsed
	}
	suffix := color.CyanString(`%.2f ms`, elapsed)
	LOG.Noticef(prefix+"%s "+suffix, name)
	return elapsed
}

func Print(any ...interface{}) {
	_, file, line, _ := runtime.Caller(1)
	str := color.CyanString(file[len(FILE_PATH):] + `:` + I.ToStr(line) + `: `)
	LOG.Info(strings.Replace(str, `%`, `%%`, -1))
	fmt.Println(any...)
}
